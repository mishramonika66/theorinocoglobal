<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class ChangePassword extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('email');
        $this->load->library('session');
        $this->load->model('User_Model');

        $this->load->database();
        $config = Array('mailtype' => 'html', 'charset' => 'iso-8859-1');
        $this->load->library('email', $config);
    }

    public function index() {
        //Silence
    }

    public function doChange($Token = NULL) {
        $User = $this->db->get_where('users', ['reset_password_token' => $Token])->row();
        if ($User != NULL) {
            $this->load->view('management/Reset_password', ['token' => $Token, 'user' => $User]);
        }
    }

    public function save() {


        $data = [];
        $this->form_validation->set_rules('new_password', 'New_Password', 'trim|required|numeric|min_length[8]');
        $this->form_validation->set_rules('cpassword', 'confirm_password', 'trim|required|numeric|min_length[8]');

        if ($this->form_validation->run() == FALSE) {
            echo validation_errors('<p class="text-danger text-center"><i class="fa fa-info-circle"></i> ', '</p>');
        } else {

            $new_password = $this->input->post('new_password');
            $confirm_password = $this->input->post('cpassword');
            $tbl = "users";

            if ($new_password == $confirm_password) {
//                $data = array('password' => $this->bcrypt->hash_password($new_password));
                $data =  array('password' => md5($new_password));
                $updated_id = $this->User_Model->update_record_by_condition($tbl, $data, array('reset_password_token' => $this->input->post('hash')));
                if ($updated_id > 0) {
                    echo '<p class="text-success text-center"><i class="fa fa-info-circle"></i> 
                     Your Password has been reset successfully, You can login into the app with the updated password.
                      </p>';
                    exit;
                } else {
                    echo '<p class="text-danger text-center"><i class="fa fa-info-circle"></i> 
                   Nothing to change.
                      </p>';
                    exit;
                }
            } else {
                echo '<p class="text-danger text-center"><i class="fa fa-info-circle"></i> 
                    new password and confirm password should be same.
                      </p>';
                exit;
            }
        }
    }

}
