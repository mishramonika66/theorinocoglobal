<?php
class Auth_model extends CI_Model {
	
	
	/* ====================================== 
	Auth_model() constructor for Auth class, it will load all database
	dependencies
	====================================== */
	  function __construct()
	  {
	      parent::__construct();
	      $this->load->database();
	  }
	  
	  
	  /* ====================================== 
	login() Takes username and password from Auth contoller and perform
	authentication on it, returns user row if user is authencticated.
	====================================== */
	  
	  function login( $username, $loginpassword)
	  {
	         $user_row =  $this->db->get_where("users", array(
	  		
	  		      'email' => addslashes($username), 
	  			  'password'  => md5(addslashes($loginpassword))  
	  		
	  		))->result();
	  	    
	  		return $user_row;
	   }

/* End of file auth.php */
}
?>