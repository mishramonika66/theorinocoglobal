<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_Model extends CI_Model
{
  
  public function __construct()
  {
    parent::__construct();
	$this->load->database();
  }
  
  //Get Slider | Home
  function getSliderRows($id='')
  {
    if(!empty($id))
	{
	  $Whr =array('id'=>$id,'display_status'=>1,'type'=>0);
	  $query = $this->db->select('slider_image')->get_where('sliders',$Whr);
	  return $query->row_array();
	}
	else
	{	  
	  $Whr = array('display_status'=>1,'type'=>0);
	  $this->db->limit(5);
	  $query = $this->db->select('slider_image')->get_where('sliders',$Whr);
	  
	  return $query->result();
	}  
  }
  
  //Get All Deals | Home
  public function getDealsRows($id='')
  {
    if(!empty($id))
	{
	  $Whr =array('id'=>$id,'display_status'=>1);
	  $query = $this->db->select('id,name')->get_where('deals',$Whr);
	  return $query->row_array();
	}
	else
	{
	  $Whr = array('display_status'=>1);
	  $this->db->limit(10);
	  $query = $this->db->select('id,name')->get_where('deals',$Whr);
	  return $query->result();
	}	  
  }
 
  //Get Individual Product According to Deal Id | Home
  public function getProductOnDeals($dealId,$type='')
  {   
	$WhrCnd = "";
	if($type=='dailydeal')
	{
	  $WhrCnd = " AND DATE(Pro.deal_end_date)='".date('Y-m-d')."'";
	}
	$deals =$this->db->query("SELECT pro.id,pro.product_name,pro.price,pro.discount,pro.discount_type,pro.price_after_discount,pro.cover_image,pro.deal_end_date from product_deals as deal LEFT JOIN products as pro ON deal.product_id=pro.id WHERE deal.deal_id=".$dealId." ".$WhrCnd." ORDER BY pro.id desc LIMIT 5")->result();	
	return $deals;
  }
 
  //Get Individual Product & Sub Category According to Deal Id | Home
  public function getProductWithCategoryOnDeals($dealId)
  { 
	$deals =$this->db->query("SELECT pro.id,pro.product_name,pro.price,pro.discount,pro.discount_type,pro.price_after_discount,pro.cover_image,cat.name as category_name,cat.id as category_id from product_deals as deal LEFT JOIN products as pro ON deal.product_id=pro.id LEFT JOIN category as cat ON pro.sub_category_id=cat.id WHERE deal.deal_id=".$dealId." AND pro.sub_category_id != '' ORDER BY pro.id desc LIMIT 5")->result();	
	return $deals;
  }

  public function getProductRows($Id='')
  {
    if(!empty($id))
	{
	  $Whr =array('id'=>$id,'display_status'=>1);
	  $query = $this->db->select('id,product_name,price,discount_type,discount,price_after_discount,cover_image,deal_end_date')->get_where('products',$Whr);
	  return $query->row_array();
	}
	else
	{
	  $Whr = array('display_status'=>1,'is_featured'=>1);
	  $this->db->limit(6);
	  $this->db->order_by("id", "desc");
	  $query = $this->db->select('id,product_name,price,discount_type,discount,price_after_discount,cover_image,deal_end_date')->get_where('products',$Whr);
	  return $query->result();
	}
  }




}

?>

