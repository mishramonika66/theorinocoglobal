<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller 
{
  public function __construct()
  {    
    parent::__construct();        
    $this->load->helper('url');  
	$this->load->helper('email');
	$this->load->library('session');
	$this->load->database();
	$config = Array('mailtype'=>'html','charset'=>'iso-8859-1');
	$this->load->library('email',$config);
	if(empty($this->session->userdata('ID'))){
      redirect(admin_base_url.'auth/login');   
    }
  }
  
  function index()
  {
    $data['Users'] = $this->db->query('SELECT * FROM users WHERE role="user" ORDER BY ID DESC')->result();

	$this->load->view('management/backend_header');
	$this->load->view('management/users/listing_user',$data);
	$this->load->view('management/backend_footer'); 
  }

  function add()
  {  
    $data = array();
	if(isset($_POST['add_user']) && $_POST['add_user']=='add')
	{
	  $user_name     = addslashes($_POST['user_name']); 
	  $phone_number  = addslashes($_POST['phone_number']); 
      $user_email    = addslashes($_POST['user_email']);
	  $user_password = addslashes($_POST['user_password']); 
      $user_type     = addslashes($_POST['user_type']);     

	  if(empty($user_name))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter username.</strong></div>';
	  }
	  else if(empty($phone_number) || !is_numeric($phone_number))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter phone number.</strong></div>';
	  }
	  else if(empty($user_email) || !valid_email($user_email))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter user email.</strong></div>';
	  }
	  else if(empty($user_password))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter password.</strong></div>';
	  }
	  else if(empty($user_type))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please select user type .</strong></div>';
	  }
	  else
	  {
	    $CntQry = mysqli_query($this->db->conn_id,"SELECT ID FROM ".DB_PREFIX."members WHERE email='".$user_email."'");

	    $UserExistence = mysqli_num_rows($CntQry);	

		if( $UserExistence > 0 )
		{
		  $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Eamil already exist,please try with differnt email.</strong></div>';
		}
		else
		{
		  $AddUser   = array('username'=>$user_name,
			                 'email'=>$user_email,
			                 'password'=>md5($user_password),
			                 'dec_password'=>base64_encode(base64_encode($user_password)),
			                 'phone_number'=>$phone_number,
			                 'active'=>1,		               
			                 'role'=>'staff',
			                 'added_date'=>date('Y-m-d H:i:s'));

		  if( !empty($_POST['permission']) )
		  {
			 $permission = implode(",",$_POST['permission']);
		     $AddUser['access_permission'] = $permission;
		  }
		  else
		  {
			 $AddUser['access_permission'] = '';
		  }

		  $this->db->insert(''.DB_PREFIX.'members',$AddUser);

		  //Email process
		  if(isset($_POST['is_email_sent']))
		  {
		    $SiteRecords   = get_site_option();
		    $Noreply       = $SiteRecords->noreply_email; 
	        $no_reply_name = $SiteRecords->noreply_name;			  
		    $best_regards  = $SiteRecords->best_regards;
		    include(APPPATH.'views/email-templates/staff/sent_message_to_staff.php');	
		  }
		  redirect(admin_base_url.'users?action=added');
		}	   
	  }
	}

	$this->load->view('management/backend_header');
	$this->load->view('management/users/add_user',$data);
	$this->load->view('management/backend_footer');
  }

  public function edit()
  {
    $data = array();
	$Id   = $this->uri->segment(4);
	if( !empty($Id) && is_numeric($Id) )
	{	  
	  $data['User'] = $this->db->query('SELECT * FROM '.DB_PREFIX.'members WHERE ID='.$Id.'')->result();

	  if(isset($_POST['edit_user']) && $_POST['edit_user']=='edit')
	  {	    
		  $user_name     = addslashes($_POST['user_name']); 
		  $phone_number  = addslashes($_POST['phone_number']); 
		  $user_email    = addslashes($_POST['user_email']);
		  $user_password = addslashes($_POST['user_password']); 
		  $user_type     = addslashes($_POST['user_type']);     

		  if(empty($user_name))
		  {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter username.</strong></div>';
		  }
		  else if(empty($phone_number) || !is_numeric($phone_number))
		  {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter phone number.</strong></div>';
		  }
		  else if(empty($user_email) || !valid_email($user_email))
		  {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter user email.</strong></div>';
		  }
		  else if(empty($user_type))
		  {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please select user type .</strong></div>';
		  }
		  else
		  {
			$CntQry = mysqli_query($this->db->conn_id,"SELECT ID FROM ".DB_PREFIX."members WHERE email='".$user_email."' AND ID!=".$Id."");

			$UserExistence = mysqli_num_rows($CntQry);	

			if( $UserExistence > 0 )
			{
			  $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Eamil already exist,please try with differnt email.</strong></div>';
			}
			else
			{
			  $EditUser   = array('username'=>$user_name,
								 'email'=>$user_email,
								 'phone_number'=>$phone_number,
								 'active'=>$_POST['user_status'],		               
								 'role'=>'staff');
			  if(!empty($user_password))
			  {
			    $EditUser['password'] = md5($user_password);
				$EditUser['dec_password'] = base64_encode(base64_encode($user_password));
			  }

			  if( !empty($_POST['permission']) )
			  {
			    $permission = implode(",",$_POST['permission']);
				$EditUser['access_permission'] = $permission;
			  }
			  else
			  {
			    $EditUser['access_permission'] = '';
			  }

			  $this->db->where('ID',$Id);
			  $this->db->update(''.DB_PREFIX.'members',$EditUser);
			  redirect(admin_base_url.'users?action=updated');
			}	   
		  }		  
	  }	
	}
	else
	{
	  redirect(admin_base_url.'location');
	}

	$this->load->view('management/backend_header');
	$this->load->view('management/users/edit_user',$data);
	$this->load->view('management/backend_footer');  
  }  

  function deleted()
  {
    $Id   = $this->uri->segment(4);
	if( !empty($Id) && is_numeric($Id) )
	{
	  $this->db->where('ID',$Id);
	  $this->db->delete(''.DB_PREFIX.'members');			 
	  redirect(admin_base_url.'users/?action=deleted');
	}
	else
	{
	  redirect(admin_base_url.'users');
	}
  }
}