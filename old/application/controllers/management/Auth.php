<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
    $this->load->library('session');
    $this->load->database();  
  }
  
  public function index()
  {    
	redirect(admin_base_url.'auth/login');
	exit();
  }

  public function login()
  {       
   if($this->session->userdata('ID') != '' and $this->session->userdata('ip')  == $_SERVER['REMOTE_ADDR'])
   {
	 redirect(admin_base_url.'dashboard'); 
   }
   else
   {
	 $this->load->view('management/login');		
   }  
  }
  
  public function logout()
  {
	 $this->session->sess_destroy();
	 redirect(admin_base_url.'auth/login?action=logout');
  } 
  
  public function verify()
  {
    foreach($_POST as $key => $value) {  $this->$key = $value; }
	if(empty($this->username) or empty($this->loginpassword))
	{
      redirect(admin_base_url.'auth/login?error=required');	
	  exit();
	}
	else
	{	
	  $this->load->model('management/auth_model','login_model_obj');
	  $it_will_be_row_if_valid = $this->login_model_obj->login($this->username,$this->loginpassword);  
	 
	  if(!empty( $it_will_be_row_if_valid))
	  {
		if("admin" != $it_will_be_row_if_valid[0]->role)
		{
		  redirect(admin_base_url.'auth/login?error=not_allowed'); 
		  exit;
		}
		elseif( 1  != $it_will_be_row_if_valid[0]->status)
		{
		  redirect(admin_base_url.'auth/login?error=not_allowed'); 
		  exit;
		}
		else
		{
		  $this->session->set_userdata(array(
						   'session_id' => session_id(),
						   'ID'  =>   $it_will_be_row_if_valid[0]->id,
						   'login_time' => date("Y-m-d H:i:s"),
			               'role'=>$it_will_be_row_if_valid[0]->role,
						   'ip' => $_SERVER['REMOTE_ADDR']));
		  redirect(admin_base_url.'dashboard');
		}
	   }
	   else
	   {
		 redirect(admin_base_url.'auth/login?error=failed');
		 exit;
	   }
	}  
  }
}
?>