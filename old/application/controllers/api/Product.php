<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require(APPPATH.'/libraries/Format.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Product extends REST_Controller
{
	public function __construct()
	{
	  parent::__construct();
	  $this->load->helper(['jwt','authorization']); 
	  $this->load->model('Product_Model');
	  $this->load->helper(array('email','url'));	  
	}
	
	private function verify_request()
    {
	  $headers = $this->input->request_headers();   
      if(isset($headers['Authorization']) && !empty($headers['Authorization']))
	  {
		$token = $headers['Authorization'];
        try{
            $data = AUTHORIZATION::validateToken($token);		
            if($data===false)
			{			  
              $status = parent::HTTP_UNAUTHORIZED;
              $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
              $this->response($response, $status);
              exit();			
			}
			else
		    {			  
               $token_db = ValidateJwtToken($token);
			   if($token_db==false){
			    $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                $this->response($response, $status);
			   }else{
			     return $token_db;
			   }			   
			}		
		}catch(Exception $e){
          $status = parent::HTTP_UNAUTHORIZED;
          $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
          $this->response($response, $status);
		}
	  }
	  else
	  {
		$status = parent::HTTP_UNAUTHORIZED;
        $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
        $this->response($response, $status);
	  } 	
	}

	/*
	*HOME PAGE 1 | START HERE
	*/
	public function home1_get()
	{
	  $dealsItems = array();

	  $sliders = $this->Product_Model->getSliderRows();
	  $deals = $this->Product_Model->getDealsRows();
	  $dailyDealItems = $this->Product_Model->getProductOnDeals(15,'dailydeal');
	  $superDealItems = $this->Product_Model->getProductWithCategoryOnDeals(14);     
	  $moreProducts   = $this->Product_Model->getProductRows();
	  if(!empty($deals) && !empty($deals[0]))
	  {
		$dealsItems = $this->Product_Model->getProductOnDeals($deals[0]->id);		
	  }  	
     
	  $Response['result']['status'] = true;
	  $Response['result']['sliders'] = $sliders;
	  $Response['result']['deals'] = $deals;
	  $Response['result']['dealItems'] = $dealsItems;
	  $Response['result']['dailydealItems'] = $dailyDealItems;
	  $Response['result']['superdealItems'] = $superDealItems;
	  $Response['result']['moreProducts'] = $moreProducts;
	  $this->response($Response,REST_Controller::HTTP_OK);	
	}

	
}
