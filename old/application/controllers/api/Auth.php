<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require(APPPATH.'/libraries/Format.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Auth extends REST_Controller
{
	public function __construct()
	{
	  parent::__construct();
	  $this->load->helper(['jwt','authorization']); 
	  $this->load->model('Auth_Model');
	  $this->load->helper(array('email','url'));	  
	}
	
	private function verify_request()
    {
	  $headers = $this->input->request_headers();   
      if(isset($headers['Authorization']) && !empty($headers['Authorization']))
	  {
		$token = $headers['Authorization'];
        try{
            $data = AUTHORIZATION::validateToken($token);		
            if($data===false)
			{			  
              $status = parent::HTTP_UNAUTHORIZED;
              $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
              $this->response($response, $status);
              exit();			
			}
			else
		    {			  
               $token_db = ValidateJwtToken($token);
			   if($token_db==false){
			    $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                $this->response($response, $status);
			   }else{
			     return $token_db;
			   }			   
			}		
		}catch(Exception $e){
          $status = parent::HTTP_UNAUTHORIZED;
          $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
          $this->response($response, $status);
		}
	  }
	  else
	  {
		$status = parent::HTTP_UNAUTHORIZED;
        $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
        $this->response($response, $status);
	  } 	
	}

	public function splashes_get($id=0)
	{
	  $splashes = $this->Auth_Model->getSplashRows($id); 
	  if(!empty($splashes))
	  {
	     $Response['status'] = true;
		 $Response['result'] = $splashes;
		 $this->response($Response,REST_Controller::HTTP_OK);
	  }
	  else
	  {
	    $this->response(['status'=>FALSE,'message'=>'screen not found.'],REST_Controller::HTTP_NOT_FOUND);
	  }
	}

	public function register_post()
	{
	  $first_name  = $this->post('first_name');
	  $last_name   = $this->post('last_name');
	  $email       = $this->post('email');
      $password    = $this->post('password');
	  $country_code= $this->post('country_code');
	  $mobile      = $this->post('mobile');
	  $gender      = $this->post('gender');
	  $device_type = $this->post('device_type');
	  $device_token= $this->post('device_token');
	  
	  if(empty($first_name))
	  {	
		$Response['status'] = false;
		$Response['message'] = 'Please enter name.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);		
	  }
	  else if(empty($email))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Please enter email.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(!valid_email($email))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Invalid email.'; 
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(empty($password))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Please enter password.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(empty($country_code))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Please enter country code.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(empty($mobile))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Please enter contact number.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(empty($gender))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Please select gender.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(empty($device_type))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Invalid device.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(empty($device_token))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Invalid device token.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else
	  {
		$CntQry    = mysqli_query($this->db->conn_id,"SELECT id FROM users WHERE email='".$email."'");
		$Existence = mysqli_num_rows($CntQry);
		if($Existence > 0)
		{
		  $Response['status'] = FALSE;
		  $Response['message'] = 'Email already exist,please try with differnt email.';
		  return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
		}
		else
		{
		   $AddUser = array('first_name'=>$first_name,
			                'last_name'=>$last_name,
			                'email'=>$email,
			                'password'=>md5($password),
			                'country_code'=>$country_code,
			                'mobile'=>$mobile,
			                'status'=>1,
			                'gender'=>$gender,
			                'device_type'=>$device_type,
			                'device_token'=>$device_token,
			                'created_date'=>date('Y-m-d H:i:s'));
			$this->db->insert('users',$AddUser);
			$Response['status'] = true;
			$Response['message'] = 'Register successfully.';
			return $this->response($Response,REST_Controller::HTTP_OK);
		}				
	  }	  
	}

	public function login_post()
	{
	  $email    = $this->post('email');
      $password = $this->post('password');
	  $device_type = $this->post('device_type');
	  $device_token= $this->post('device_token');

	  if(empty($email))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Please enter email.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(!valid_email($email))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Invalid email.'; 
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(empty($password))
	  {
	    $Response['status'] = FALSE;
		$Response['message'] = 'Please enter password.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(empty($device_type))
	  {
	    $Response['status'] = FALSE;
		$Response['message']= 'Invalid device.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else if(empty($device_token))
	  {
	    $Response['status'] = FALSE;
		$Response['message']= 'Invalid device token.';
		return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
	  }
	  else
	  {
	    $CntQry = mysqli_query($this->db->conn_id,"SELECT id FROM users WHERE email='".$email."' AND password='".md5($password)."' AND status=1");
		$Existence = mysqli_num_rows($CntQry);
		if($Existence > 0)
		{
		  $UserRec = mysqli_fetch_object($CntQry);	
		  if(!empty($UserRec->id))
		  {
		    $token = AUTHORIZATION::generateToken(['id'=>$UserRec->id]);
			
			$UpdToken = array('jwt_token'=>$token);
			$this->db->where('id',$UserRec->id);
			$this->db->update('users',$UpdToken);

			$response = ['status' => TRUE, 'token' => $token];
			return $this->response($response, REST_Controller::HTTP_OK);
		  }
		  else
		  {
		    $Response['status'] = FALSE;
		    $Response['message']= 'Invalid email or password.';
		    return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
		  }
		}
		else
		{
		  $Response['status'] = FALSE;
		  $Response['message']= 'Invalid email or password.';
		  return $this->response($Response,REST_Controller::HTTP_BAD_REQUEST);
		}	  
	  }	
	}	

	public function get_me_data_get()
	{
	  $verifydata = $this->verify_request();
	  if(!empty($verifydata) && !empty($verifydata->id) && is_numeric($verifydata->id))
	  {
        $response = ['status' => TRUE, 'result' => $verifydata];
        $this->response($response, parent::HTTP_OK);
	  }
	  else
	  {
        $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
        $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
	  }	  
	}

	public function logout_get()
	{
	  $verifydata = $this->verify_request();
	  if(!empty($verifydata) && !empty($verifydata->id) && is_numeric($verifydata->id))
	  {
		$this->db->where('id',$verifydata->id);
		$this->db->update('users',array('jwt_token'=>NULL));
		
		$response = ['status'=>TRUE,'message'=>'Logout successfully.'];
        $this->response($response, REST_Controller::HTTP_OK);
	  }
	  else
	  {
        $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
        $this->response($response, REST_Controller::HTTP_BAD_REQUEST);
	  }	  
	}
}
