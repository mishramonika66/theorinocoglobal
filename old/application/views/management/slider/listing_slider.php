<?php
$errmsg = "";
if(isset($_GET['action']) && $_GET['action']=='deleted')
{
  $errmsg = '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Deleted successfully.</strong></div>';
}
else if(isset($_GET['action']) && $_GET['action']=='added' && empty($errmsg))
{
  $errmsg = '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Added successfully.</strong></div>';
}
if(isset($_GET['action']) && $_GET['action']=='updated' && empty($errmsg))
{
  $errmsg = '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Saved successfully.</strong></div>';
}
?>
<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">
			      Manage Sliders
			   </h4>
            </div>
            <div>
               <a href="<?php echo admin_base_url.'slider/add'; ?>" class="btn btn-primary btn-icon-text">
               <i class="ti-plus"></i> Create New Image
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
			 <?php if(!empty($errmsg)) { echo $errmsg; } ?>
               <div class="table-responsive pt-3">
                  <table class="table table-bordered dataTables" id="dtBasicExample">
                     <thead>
                        <tr class="text-center">
                           <th>#</th>
						   <th>Image</th>
						   <th>Status</th>
						   <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>	
                        <?php if(!empty($sliders)){ ?>
						<?php $Counter=1; ?>
						<?php foreach($sliders as $slider){ ?>
						<tr class="text-center">
                           <td><?=$Counter?></td>
                           <td>
						    <?php if(!empty($slider->slider_image) && file_exists($slider->slider_image)){ ?>
						     <img src="<?php echo base_url().$slider->slider_image; ?>">
						    <?php } ?>
						   </td>  			   
						   <td><b>
						   <?php if($slider->display_status==1){
						    echo 'Active';
						   }else{
						    echo 'Inactive';
						   }?>
						   </b></td>					   
						   <td>						  
                              <a href="<?php echo admin_base_url.'slider/edit/'.$slider->id; ?>" class="btn btn-success btn-xs"><i class="ti-pencil"></i></a>
							  <a href="<?php echo admin_base_url.'slider/deleted/'.$slider->id; ?>" class="btn btn-danger btn-xs"><i class="ti-trash"></i></a>
						   </td>
                        </tr>
						<?php $Counter++; } ?>
						<?php } ?>						
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>