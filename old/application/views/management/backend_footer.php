<footer class="footer">
   <div class="d-sm-flex justify-content-center justify-content-sm-between">
      <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright &copy; 2019 <!--SensibleParenting.--> All rights reserved.</span>
      <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"></span>
   </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="<?php echo base_url(); ?>assets/back/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!--<script src="<?php echo base_url(); ?>assets/back/js/Chart.min.js"></script>-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?php echo base_url(); ?>assets/back/js/off-canvas.js"></script>
<script src="<?php echo base_url(); ?>assets/back/js/hoverable-collapse.js"></script>
<script src="<?php echo base_url(); ?>assets/back/js/template.js"></script>
<script src="<?php echo base_url(); ?>assets/back/js/todolist.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/back/js/chart.js"></script>-->
<!-- endinject -->
<!-- Custom js for this page-->
<script src="<?php echo base_url(); ?>assets/back/js/dashboard.js"></script>
<!-- End custom js for this page-->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"/></script>
<script src="<?php echo base_url(); ?>assets/back/js/bootstrap-datepicker.min.js"></script>
<script>
var BaseUrl = "<?php echo admin_base_url; ?>";
$(document).ready( function(){
    $('.dataTables').DataTable();
});
$(".alert-danger,.alert-success, .alert-info").fadeTo(2000, 600).slideUp(600, function(){
    $(".alert-danger,.alert-success").slideUp(600);
});
//sidebar-icon-only
</script>
<script src="<?php echo base_url(); ?>assets/back/js/orinocoglobal.js"></script>
</body>
</html>