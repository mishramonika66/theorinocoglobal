<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_Model extends CI_Model
{
  
  public function __construct()
  {
    parent::__construct();
	$this->load->database();
  }
  
  function getSplashRows($id='')
  {
    if(!empty($id))
	{
	  $query = $this->db->select('image,heading')->get_where('splashes',array('id'=>$id,'status'=>1));
	  return $query->row_array();
	}
	else
	{	  
	  $query = $this->db->select('image,heading')->get_where('splashes',array('status'=>1));
	  return $query->result();
	}  
  }
}

?>

