<style>
.del-img {
	display: inline-block;
	width: 10%;
	position: relative;
	margin:30px;

}
.del-pro-img-icon i{
    position: absolute;
    right: 0px;
    top: -12px;
    font-size: 20px;
    left: 96px;
}
.del-pro-img img {
    width: 100px;
    border: 1px solid #ccc;
 }
</style>
<div id="page-wrapper">
   <div class="row">
      <div class="col-lg-12">
         &nbsp;
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
   <div class="row">
      <div class="col-lg-12">
         <div class="panel panel-default">
            <div class="panel-heading">
			   <span><b>Update Product</b></span>
               <span class="pull-right"><a href="<?php echo base_url(); ?>administrator/products/listing"><b>Back</b></a></span>
            </div>
            <div class="panel-body">
               <div class="row">
                  <div class="col-lg-12">
                     <form action="" name="siteoption_form" id="siteoption_form" method="post" enctype="multipart/form-data">
                        <?php if(isset($Errormessage) && $Errormessage !='') {echo $Errormessage; }?>

                        <div class="form-group col-sm-6">
                           <label>Product Code</label>
                           <input class="form-control" type="text" name="product_code" id="product_code" value="<?=$ProductRec[0]['product_code']?>">   
                        </div>

						<div class="form-group col-sm-6">
                           <label>Product Name</label>
                           <input class="form-control" type="text" name="product_name" id="product_name" value="<?=$ProductRec[0]['product_name']?>">   
                        </div>

						<div class="form-group col-sm-6">
                           <label>Brand Name</label>
                           <input class="form-control" type="text" name="product_brand" id="product_brand" value="<?=$ProductRec[0]['product_brand']?>">   
                        </div>

                        <div class="form-group col-sm-6">
                           <label>Product Modal</label>
                           <input class="form-control" type="text" name="product_model" id="product_model" value="<?=$ProductRec[0]['product_model']?>">
                        </div>

						<div class="form-group col-sm-12">
                           <label>Product Description</label>
                           <textarea class="form-control" type="text" name="product_description" id="product_description" cols="8"><?=stripslashes($ProductRec[0]['product_description'])?></textarea>   
                        </div>

						 <div class="form-group col-sm-12">
                           <label>Product Images</label>
                           <input class="form-control" type="file" name="product_image[]" multiple>  
						    <?php
                            $productImg = SU_ProductImage($_GET['pro_id']);
							if(!empty($productImg)){
							foreach($productImg as $ProImage){								
							if(!empty($ProImage['image']) && file_exists('uploads/products/'.$ProImage['image'])){ ?>
							    <div class="del-pro-img del-img" id="del-pro-img-<?=$ProImage['image_id']?>">
								<img src="<?php echo base_url().'uploads/products/'.$ProImage['image']; ?>"><span class="del-pro-img del-pro-img-icon" data-imgnm="<?=$ProImage['image']?>" data-imgid="<?=$ProImage['image_id']?>" class="del-pro-img"><a href=""><i class="fa fa-times-circle"></i></a></span>
								<input type="hidden" name="old_image" value="<?php echo stripslashes($ProImage['image']); ?>"/>
								</div>
                             <?php } } } ?>
                        </div>

						<div class="form-group col-sm-12">
						  <label>Youtube Video</label>
						   <input type="text" class="form-control" type="text" name="product_youtube_link" id="product_youtube_link" value="<?=$ProductRec[0]['product_youtube_link']?>">
						</div>	

						<div class="form-group col-sm-6">
						  <label>Outright Price</label>
                           <input class="form-control" type="text" name="product_outright_price" id="product_outright_price" value="<?=$ProductRec[0]['product_outright_price']?>"> 
						</div>

						<div class="form-group col-sm-6">
						  <label>Rental Price</label>
                           <input class="form-control" type="text" name="product_rental_price" id="product_rental_price" value="<?=$ProductRec[0]['product_rental_price']?>"> 
						</div>
            <div class="form-group col-sm-6">
              <label>HSN</label>
                        <input class="form-control" type="text" name="hsn" id="hsn" value="<?=$ProductRec[0]['hsn']?>"> 
            </div>
            <div class="form-group col-sm-6">
              <label>GST</label>
                        <input class="form-control" type="text" name="gst" id="gst" value="<?=$ProductRec[0]['gst']?>"> 
            </div>		

						<div class="form-group col-sm-6">
						  <label>Rental Tenure</label>
                          <select name="rental_tenure" id="rental_tenure" class="form-control">
						  <option value="0" <?php if($ProductRec[0]['rental_tenure']==0){ echo 'selected'; } ?>>Select Tenure</option>
						   <option value="3" <?php if($ProductRec[0]['rental_tenure']==3){ echo 'selected'; } ?>>03</option>
						   <option value="5" <?php if($ProductRec[0]['rental_tenure']==5){ echo 'selected'; } ?>>05</option>
						   <option value="10" <?php if($ProductRec[0]['rental_tenure']==10){ echo 'selected'; } ?>>10</option>
						  </select>
						</div>

						<div class="form-group col-sm-6">
						  <label>Select Category</label>
                           <select name="product_category" id="product_category" class="form-control">
                           <option value="">Select Category</option>
						   <?php if(!empty($AllCategory)){ 
						   foreach($AllCategory as $Category){  ?>						   
						   <option value="<?=$Category['category_id']?>" <?php if($ProductRec[0]['product_category']==$Category['category_id']){ echo 'selected'; } ?>><?=$Category['category_name']?></option>	
						   <?php } } ?>

						  </select> 
						</div>

						<div class="form-group col-sm-6">
						  <label>Display Value</label>
                           <input class="form-control" type="text" name="display_order" id="display_order" value="<?=$ProductRec[0]['display_order']?>"> 
						</div>

						<div class="form-group col-sm-6">
						  <label>Page Status</label>
                            <div class="radio">
                              <label>
                                <input type="radio" name="display_status" id="display_status1" value="1" <?php if($ProductRec[0]['display_status']==1){ echo 'checked'; } ?>>Display
                              </label>
							  <label>
                                <input type="radio" name="display_status" id="display_status0" value="0" <?php if($ProductRec[0]['display_status']==0){ echo 'checked'; } ?>>Hide
                            </label>
                          </div>
						</div>

                        <input type="submit" class="btn btn-default pull-right" name="update_Product" value="Update Product">
                     </form>
                  </div>
               </div>
               <!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
         </div>
         <!-- /.panel -->
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- /.row -->
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<script>
$(document).ready(function(){
	$('.del-pro-img').click(function(){
	  var Imgid = $(this).data('imgid');
	  var Imgnm = $(this).data('imgnm');

	  if(Imgid != '' && Imgid > 0 && isNaN(Imgid)==false)
	  {
	    jQuery.ajax({
				type: 'POST',						
				url: '<?=base_url(); ?>administrator/products/deleteImg/',						
				data: {'Imgid' : Imgid, 'Imgnm':Imgnm },
				success: function(response){
					if(response=='success'){
					   $('#del-pro-img-'+Imgid).hide();
					}				
				}						
	     });  
	  }	
	});
});

</script>