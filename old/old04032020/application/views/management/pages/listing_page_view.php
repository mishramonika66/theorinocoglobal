<?php
$errmsg = "";
if(isset($_GET['action']) && $_GET['action']=='deleted')
{
  $errmsg = '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Deleted successfully.</strong></div>';
}
?>
<!-- Everything goes inside this --> 
<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">MANAGE PAGES</h4>
            </div>
            <div>
               <a href="<?php echo admin_base_url.'pages/add'; ?>" class="btn btn-primary btn-icon-text">
               <i class="ti-plus"></i>  
               ADD PAGE
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
			 <?php if(!empty($errmsg)) { echo $errmsg; } ?>
               <div class="table-responsive pt-3">
                  <table class="table table-bordered dataTables" id="dtBasicExample">
                     <thead>
                        <tr class="text-center">
                           <th>#</th>
                           <th>Pproduct Name</th>                        
                           <th>Created At</th>
						   <th>Action</th>
                        </tr>
                     </thead>
                     <tbody>
					   <?php if(!empty($PagesRecord)){ ?>
					   <?php $Counter=1; ?>
					   <?php foreach($PagesRecord as $Record){ ?>
                        <tr class="text-center">
                           <td><?=$Counter?></td>
                           <td><?=stripslashes($Record->page_title)?></td>                           
                           <td><?=date("d-m-Y",strtotime($Record->added_date))?></td>
						   <td>						  
                              <a href="<?php echo admin_base_url.'pages/edit/'.$Record->ID; ?>" class="btn btn-success btn-xs"><i class="ti-pencil"></i></a>
							  <a href="<?php echo admin_base_url.'pages/deleted/'.$Record->ID; ?>" class="btn btn-danger btn-xs"><i class="ti-trash"></i></a>
						   </td>
                        </tr>  
						<?php $Counter++; ?>
						<?php } ?>
						<?php } ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Everything goes inside this -->