<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">Create New Member</h4>
            </div>
            <div>
               <a href="<?php echo admin_base_url.'users'; ?>" class="btn btn-primary btn-icon-text">
               <i class="ti-angle-left btn-icon-append"></i>  
               BACK
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <?php if(!empty($errmsg)) { echo $errmsg; } ?>
               <form class="forms-sample" method="post">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="user_name"><b>Name <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter name" value="<?php if(!empty($_POST['user_name'])){ echo $_POST['user_name']; } ?>">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="phone_number"><b>Phone Number <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Enter phone number" value="<?php if(!empty($_POST['phone_number'])){ echo $_POST['phone_number']; } ?>">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="user_email"><b>Email <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="user_email" name="user_email" placeholder="Enter email" value="<?php if(!empty($_POST['user_email'])){ echo $_POST['user_email']; } ?>">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="user_password"><b>Password <font color="red">*</font></b></label>
                           <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Enter password" value="<?php if(!empty($_POST['user_password'])){ echo $_POST['user_password']; } ?>">
                        </div>
                     </div>
                  </div>

				  <div class="row">
				   <!--
				   <div class="col-sm-2">
                        <div class="form-group">                           
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="all" name="permission[]" class="form-check-input" <?php if(!empty($permission) && in_array('all',$permission) ){ echo 'checked'; } ?>>All
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>
					 -->

                     <div class="col-sm-2">
                        <div class="form-group">
                          <label for="user_password"><b>Access Permission <font color="red">*</font></b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="staff" name="permission[]" class="form-check-input">Manage Staff
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>
                     
					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="location" name="permission[]" class="form-check-input">Manage Location
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>

					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="vendors" name="permission[]" class="form-check-input">Manage Vendors
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>

					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="items" name="permission[]" class="form-check-input">Manage Items
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>

					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="orders" name="permission[]" class="form-check-input">Manage Orders
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>

					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="reports" name="permission[]" class="form-check-input" >View Reports
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>
				  </div>




                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="user_type"><b>Role <font color="red">*</font></b></label>
                           <select class="custom-select" id="user_type" name="user_type">
                              <option value="staff" selected>Staff members</option>
                           </select>
                        </div>
                     </div>

					 <div class="col-sm-6">
                        <div class="form-group">
                           <label for="is_email_sent"><b>Is Email Sent<font color="red">*</font></b></label> 
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="yes" name="is_email_sent" checked class="form-check-input" >Yes, Login information sent.
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
                        </div>
                     </div>
                     
                  </div>
				  <div class="col-sm-12">
                        <button type="submit" name="add_user" value="add" class="btn btn-primary btn-lg mr-2">Add</button> 
                     </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>