<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Expample extends REST_Controller {

	public function __construct()
	{
	  parent::__construct();

	  $this->load->model('UserExample');
	
	}

	public function index_get()
	{
	  /*
	  $users = $this->UserExample->getRows($id);

	   if(!empty($users)){
	      $this->response($users,REST_Controller::HTTP_OK);
	   }else{
	     $this->response(['status'=>FALSE,'message'=>'no user were found'],REST_Controller::HTTP_NOT_FOUND);
	   }
	   */
	}

	public function user_get($id=0)
	{
	   $users = $this->UserExample->getRows($id);

	   if(!empty($users)){
	      $this->response($users,REST_Controller::HTTP_OK);
	   }else{
	     $this->response(['status'=>FALSE,'message'=>'no user were found'],REST_Controller::HTTP_NOT_FOUND);
	   }
	}

	public function user_post()
	{
	  
	   
	   
	   $userData = array();
	   $userData['first_name'] = $this->post('first_name');
	   $userData['last_name'] = $this->post('last_name');
	   $userData['phone'] = $this->post('phone');
	   $userData['email'] = $this->post('email');

	   if(!empty($userData['first_name']) && !empty($userData['email'])){
	       $insert = $this->UserExample->insert($userData);
		   if($insert){
			   $this->response(['status'=>TRUE,'message'=>'Added'],REST_Controller::HTTP_OK);
		   }else{
			  $this->response(['status'=>FALSE,'message'=>'Error'],REST_Controller::HTTP_BAD_REQUEST);
		   }
	   
	   }else{
	       $this->response(['arr'=>$userData['first_name']],REST_Controller::HTTP_BAD_REQUEST);
	   }

	   
	}

	public function user_put()
	{
	  $userData = array();
	  $id = $this->put('id');
	  $userData['first_name'] = $this->put('first_name');
	  $userData['last_name'] = $this->put('past_name');
	  $userData['email'] = $this->put('email');
	  $userData['phone'] = $this->put('phone');

	  if(!empty($id) && !empty($userData['first_name']) && !empty($userData['email'])){
	      
		  $update = $this->UserExample->update($userData,$id);
		  if($update){
		     $this->response(['status'=>TRUE,'message'=>'Saved'],REST_Controller::HTTP_OK);
		  }else{
		     $this->response('Error',REST_controller::HTTP_BAD_REQUEST);
		  }
	   
	  }else{
	         $this->response(['status'=>FALSE,'message'=>'Incomplete'],REST_Controller::HTTP_BAD_REQUEST);	  
	  }

 	}

	public function user_delete($id)
	{
	   if($id){
	     
		 $delete = $this->UserExample->delete($id);
		 if($delete){
		   $this->response(['status'=>TRUE,'message'=>'Deleted'],REST_Controller::HTTP_OK);
		 }
		 else{
		   $this->response(['status'=>FALSE,'message'=>'Error'],REST_Controller::HTTP_BAD_REQUEST);
		 }
	   
	   }
	}
	
}
