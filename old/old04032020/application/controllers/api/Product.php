<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require(APPPATH . '/libraries/Format.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Product extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']);
        $this->load->model('Product_Model');
        $this->load->helper(array('email', 'url'));
    }

    private function verify_request() {
        $headers = $this->input->request_headers();
        if (isset($headers['Authorization']) && !empty($headers['Authorization'])) {
            $token = $headers['Authorization'];
            try {
                $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                    $this->response($response, $status);
                    exit();
                } else {
                    $token_db = ValidateJwtToken($token);
                    if ($token_db == false) {
                        $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                        $this->response($response, $status);
                    } else {
                        return $token_db;
                    }
                }
            } catch (Exception $e) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                $this->response($response, $status);
            }
        } else {
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
            $this->response($response, $status);
        }
    }

    /*
     * HOME API| START HERE
     */

    public function home_get() {
        $dealsItems = array();
        $sliders = $this->Product_Model->getSliderRows();
        $deals = $this->Product_Model->getDealsRows();
        $dailyDealItems = $this->Product_Model->getProductOnDeals(15, 'dailydeal');
        $superDealItems = $this->Product_Model->getProductWithCategoryOnDeals(14);
        $moreProducts = $this->Product_Model->getProductRows();
        $FeaturedCategories = $this->Product_Model->getCategoriesHome();

        if (!empty($deals) && !empty($deals[0])) {
            $dealsItems = $this->Product_Model->getProductOnDeals($deals[0]->id);
        }

        $Response['result']['status'] = true;
        $Response['result']['sliders'] = $sliders;
        $Response['result']['deals'] = $deals;
        $Response['result']['dealItems'] = $dealsItems;
        $Response['result']['dailydealItems'] = $dailyDealItems;
        $Response['result']['superdealItems'] = $superDealItems;
        $Response['result']['moreProducts'] = $moreProducts;
        if (!empty($FeaturedCategories)) {
            $Response['result']['featuredCategories'] = $FeaturedCategories;
        }
        $this->response($Response, REST_Controller::HTTP_OK);
    }

    /*
     * CATEGORIES API| START HERE
     */

    public function categories_get() {
        $categories = $this->Product_Model->getCategoriesRows();
        if (!empty($categories)) {
            $Response['result']['status'] = true;
            $Response['result']['categories'] = $categories;
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
        $Response['status'] = false;
        $Response['message'] = 'categories not found.';
        return $this->response($Response, REST_Controller::HTTP_BAD_REQUEST);
    }

    /*
     * DEALS API| START HERE
     */

    public function deals_get() {
        $deals = $this->Product_Model->getDealsRows();
        if (!empty($deals)) {
            $Response['result']['status'] = true;
            $Response['result']['deals'] = $deals;
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
        $Response['status'] = false;
        $Response['message'] = 'deals not found.';
        return $this->response($Response, REST_Controller::HTTP_BAD_REQUEST);
    }

    /*
     * PRODUCT LIST BY CATEGORY PAGE API| START HERE
     */

    public function category_products_get() {
        if (isset($_GET['category_id']) && !empty($_GET['category_id']) || is_numeric($_GET['category_id'])) {
            $products = $this->Product_Model->getProductByCategory($_GET['category_id']);
            if (!empty($products)) {
                $Response['result']['status'] = true;
                $Response['result']['products'] = $products;
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
        $Response['status'] = false;
        $Response['message'] = 'Product not found.';
        return $this->response($Response, REST_Controller::HTTP_BAD_REQUEST);
    }

    /*
     * SINGLE PRODUCT DETAIL BY PRODUCT ID API| START HERE
     */

    public function product_detail_get() {
        if (isset($_GET['product_id']) && !empty($_GET['product_id']) && is_numeric($_GET['product_id'])) {
            $product = $this->Product_Model->getProductRows($_GET['product_id']);
            if (!empty($product)) {
                $sizes = array();
                $colors = array();
                $relatedProducts = array();

                $ratings = $this->Product_Model->getProductRatings($_GET['product_id']);
                $sliders = $this->Product_Model->getProductSlider($_GET['product_id']);

                if (!empty($product['sizes'])) {
                    $sizes = $this->Product_Model->getAttributeInProducts($product['sizes'], 2);
                }
                if (!empty($product['color'])) {
                    $colors = $this->Product_Model->getAttributeInProducts($product['color'], 1);
                }

                if (!empty($product['subsub_category_id'])) {
                    $CatId = $product['subsub_category_id'];
                } else if (!empty($product['sub_category_id'])) {
                    $CatId = $product['sub_category_id'];
                } else if (!empty($product['category_id'])) {
                    $CatId = $product['category_id'];
                }

                if (!empty($CatId)) {
                    $relatedProducts = $this->Product_Model->getProductByCategory($CatId, 'related', $_GET['product_id']);
                }

                $Response['result']['status'] = true;
                $Response['result']['product'] = $product;
                $Response['result']['product']['return_policy'] = 'Free return With in 15 days for orinoco items and 7 days for other eligible items.';
                $Response['result']['product']['avg_rating'] = 3;
                $Response['result']['product']['pro_sizes'] = $sizes;
                $Response['result']['product']['pro_colors'] = $colors;
                $Response['result']['product']['pro_ratings'] = $ratings;
                $Response['result']['product']['pro_related'] = $relatedProducts;
                $Response['result']['product']['pro_sliders'] = $sliders;
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }

        $Response['status'] = false;
        $Response['message'] = 'Product not found.';
        return $this->response($Response, REST_Controller::HTTP_BAD_REQUEST);
    }

    /*
     * DEALS PRODUCT PAGE BY DEAL ID API| START HERE
     */

    public function product_deals_get() {
        if (isset($_GET['deal_id']) && !empty($_GET['deal_id']) && !is_numeric($_GET['deal_id'])) {
            
        }

        $Response['status'] = false;
        $Response['message'] = 'Product not found.';
        return $this->response($Response, REST_Controller::HTTP_BAD_REQUEST);
    }

}
