<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CI_Controller
{
  public function __construct()
  {         
    parent::__construct();		
    $this->load->helper('url');
	$this->load->helper('email');	
	$this->load->library('session');    
	$this->load->library("pagination");	
	$this->load->database();	
	if(empty($this->session->userdata('ID'))){
      redirect(admin_base_url.'auth/login');   
    }      	
   }

   public function index()
   {
     $data = array();

     $data['products']=$this->db->query("SELECT pro.id,pro.product_name,pro.cover_image,pro.is_featured,pro.display_status,cat.name FROM products as pro LEFT JOIN category as cat ON pro.category_id=cat.id WHERE pro.display_status !=2 ORDER BY pro.id DESC")->result();

	 $this->load->view('management/backend_header');
	 $this->load->view('management/product/listing_product',$data);
	 $this->load->view('management/backend_footer');
   }
   
   public function add()
   {
     $data['parentcat'] = $this->db->query('SELECT * FROM category WHERE type=1 AND display_status=1 AND parent_id=0 ORDER BY id DESC')->result();

	 $data['sizes'] = $this->db->query('SELECT id,attribute_name FROM attributes WHERE type=2 AND display_status=1 ORDER BY id DESC')->result();

	 $data['colors'] = $this->db->query('SELECT id,attribute_name FROM attributes WHERE type=1 AND display_status=1 ORDER BY id DESC')->result();

	 $data['deals'] = $this->db->query('SELECT id,name FROM deals WHERE type=2 AND display_status=1 ORDER BY id DESC')->result();

	 if(isset($_POST['add_product']) && $_POST['add_product'] != '')
	 {
	   $pro_name = addslashes($_POST['pro_name']);
	   $pro_qty = addslashes($_POST['pro_qty']);
	   $pro_cat = (isset($_POST['pro_cat'])) ? $_POST['pro_cat'] : 0;
	   $subcat_id = (isset($_POST['subcat_id'])) ? $_POST['subcat_id'] : 0;
	   $subsubcat_id = (isset($_POST['subsubcat_id'])) ? $_POST['subsubcat_id'] : 0;
	   $pro_price = addslashes($_POST['pro_price']);
	   $pro_discount = addslashes($_POST['pro_discount']);
	   $discount_type = addslashes($_POST['discount_type']);	   
	   $pro_desc = addslashes($_POST['pro_desc']);
	   $pro_spec = addslashes($_POST['pro_spec']);
	   $in_featured = (isset($_POST['in_featured'])) ? $_POST['in_featured'] : 0;
	   $display_status = (isset($_POST['display_status'])) ? $_POST['display_status'] : 0;
       
	   $sizes = '';
	   $color = '';
	   if(isset($_POST['sizes']) && !empty($_POST['sizes']))
	   {
	     $sizes = implode(',',$_POST['sizes']);
	   }
	   if(isset($_POST['color']) && !empty($_POST['color']))
	   {
	      $color = implode(',',$_POST['color']);
	   }

	   $image_name = addslashes($_FILES["cover_img"]["name"]);
	   $templocation = $_FILES["cover_img"]["tmp_name"];
	   $ext= strtolower(substr($image_name, strrpos($image_name, '.') + 1));
	   $store = "uploads/product-img/";
       if(!file_exists($store)){ @mkdir($store, 0777); }

	   if(empty($pro_name))
	   {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter product name.</strong></div>';
	   }
	   else if(empty($pro_qty))
	   {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter product quantity.</strong></div>';
	   }
	   else if(empty($templocation))
	   {
	     $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please upload image.</strong></div>';
	   }
	   else
	   {
		 $priceafterdiscount = $pro_price;
	     if( isset($_POST['discount_type']) && $_POST['discount_type']==1 )
		 {
		   $percentageprice = ($pro_price * $pro_discount)/100;
           $priceafterdiscount = $pro_price - $percentageprice;		   
		 }
		 
		 $product_slug = UniqueSlug($pro_name,'products','product_slug');
		 $Add = array('product_name'=>$pro_name,
			          'product_slug'=>$product_slug,
			          'quantity'=>$pro_qty,
			          'category_id'=>$pro_cat,
			          'sub_category_id'=>$subcat_id,
			          'subsub_category_id'=>$subsubcat_id,
			          'price'=>$pro_price,
			          'discount_type'=>$discount_type,
			          'discount'=>$pro_discount,
			          'price_after_discount'=>$priceafterdiscount,
			          'sizes'=> $sizes,
			          'color'=>$color,
			          'description'=>$pro_desc,
			          'specification'=>$pro_spec,
			          'is_featured'=>$in_featured ,
			          'display_status'=>$display_status ,
			          'added_date'=>date('Y-m-d H:i:s'));

		 if($ext !="jpg" && $ext !="png" && $ext !="jpeg" && $ext !="gif" && $ext !='svg'){}
		 else
		 {
		   $newfilename = file_new_name_when_exist($store,$image_name);	
		   if(move_uploaded_file($templocation, $store.$newfilename))
		   {
             $Add['cover_image'] = $store.$newfilename;
		   }
		 }

		 if(isset($_POST['deals']) && !empty($_POST['deals']))
		 {
		   $Add['deal_ids'] = implode(',',$_POST['deals']);
		 }

		 $this->db->insert('products',$Add);

		 $ProductId = $this->db->insert_id();
		 if($ProductId)
		 {
		   //=====Multiple image upload start======
		   if(!empty($_FILES['product_image']['name']))
		   {
		      $filesCount = count($_FILES['product_image']['name']);
			  for($i = 0; $i < $filesCount; $i++)
			  {			   
                $_FILES['file']['name']     = $_FILES['product_image']['name'][$i];
                $_FILES['file']['type']     = $_FILES['product_image']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['product_image']['tmp_name'][$i];
                $_FILES['file']['error']    = $_FILES['product_image']['error'][$i];
                $_FILES['file']['size']     = $_FILES['product_image']['size'][$i]; 
                $uploadPath                 = 'uploads/product-img/';
                $config['upload_path']      = $uploadPath;
                $config['allowed_types']    = 'jpg|jpeg|png|gif'; 
                $this->load->library('upload', $config);
                $this->upload->initialize($config);                
               
                if($this->upload->do_upload('file')){
                    // Uploaded file data
                    $fileData = $this->upload->data();
                    $uploadData[$i]['file_name'] = $fileData['file_name'];
                    $uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
                }

				if(!empty($uploadData))
				{				 
				  $AddImages = array('product_id'=>$ProductId,
					                'image'=>$store.$uploadData[$i]['file_name']);
		          $this->db->insert("product_images",$AddImages);
				}		  
			  }
		   }
		   //=====Multiple image upload end======

		  //=====Add deals in product start======
		  if(isset($_POST['deals']) && !empty($_POST['deals']))
		  {
		    foreach($_POST['deals'] as $deals)
	        {
	          $Adddeal = array('deal_id'=>$deals,
                               'category_id'=>$pro_cat,
				               'sub_category_id'=>$subcat_id,
			                   'subsub_category_id'=>$subsubcat_id,
				               'product_id'=>$ProductId);

			  $this->db->insert("product_deals",$Adddeal);
	        }		  
		  }
		  //=====Add deals in product end======

		 }

		 redirect(admin_base_url.'products/?action=added');
	   } 
	 }

     $this->load->view('management/backend_header');
	 $this->load->view('management/product/add_product',$data);
	 $this->load->view('management/backend_footer');
   }

   public function edit()
   {      
     $Id = $this->uri->segment(4);
	 if(!empty($Id) && is_numeric($Id) )
	 {
	     $data['product']=$this->db->query("SELECT * FROM products WHERE id=".$Id."")->result();

		  $data['images']=$this->db->query("SELECT id,image FROM product_images WHERE product_id=".$Id."")->result_array();
		 
		 $data['parentcat'] = $this->db->query('SELECT * FROM category WHERE type=1 AND display_status=1 AND parent_id=0 ORDER BY id DESC')->result();

		 $data['sizes'] = $this->db->query('SELECT id,attribute_name FROM attributes WHERE type=2 AND display_status=1 ORDER BY id DESC')->result();

		 $data['colors'] = $this->db->query('SELECT id,attribute_name FROM attributes WHERE type=1 AND display_status=1 ORDER BY id DESC')->result();

		 $data['deals'] = $this->db->query('SELECT id,name FROM deals WHERE type=2 AND display_status=1 ORDER BY id DESC')->result();

		 if(isset($_POST['add_product']) && $_POST['add_product'] != '')
		 {
		   $pro_name = addslashes($_POST['pro_name']);
		   $pro_qty = addslashes($_POST['pro_qty']);
		   $pro_cat = (isset($_POST['pro_cat'])) ? $_POST['pro_cat'] : 0;
		   $subcat_id = (isset($_POST['subcat_id'])) ? $_POST['subcat_id'] : 0;
		   $subsubcat_id = (isset($_POST['subsubcat_id'])) ? $_POST['subsubcat_id'] : 0;
		   $pro_price = addslashes($_POST['pro_price']);
		   $pro_discount = addslashes($_POST['pro_discount']);
		   $discount_type = addslashes($_POST['discount_type']);	   
		   $pro_desc = addslashes($_POST['pro_desc']);
		   $pro_spec = addslashes($_POST['pro_spec']);
		   $in_featured = (isset($_POST['in_featured'])) ? $_POST['in_featured'] : 0;
		   $display_status = (isset($_POST['display_status'])) ? $_POST['display_status'] : 0;
		   
		   $sizes = '';
		   $color = '';
		   if(isset($_POST['sizes']) && !empty($_POST['sizes']))
		   {
			 $sizes = implode(',',$_POST['sizes']);
		   }
		   if(isset($_POST['color']) && !empty($_POST['color']))
		   {
			  $color = implode(',',$_POST['color']);
		   }
		
		   $image_name = addslashes($_FILES["cover_img"]["name"]);
		   $templocation = $_FILES["cover_img"]["tmp_name"];
		   $ext= strtolower(substr($image_name, strrpos($image_name, '.') + 1));
		   $store = "uploads/product-img/";
		   if(!file_exists($store)){ @mkdir($store, 0777); }

		   if(empty($pro_name))
		   {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter product name.</strong></div>';
		   }
		   else if(empty($pro_qty))
		   {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter product quantity.</strong></div>';
		   }		   
		   else
		   {
			 $product_slug = UniqueSlug($pro_name,'products','product_slug');
			 $Add = array('product_name'=>$pro_name,
						  'product_slug'=>$product_slug,
						  'quantity'=>$pro_qty,
						  'category_id'=>$pro_cat,
						  'sub_category_id'=>$subcat_id,
						  'subsub_category_id'=>$subsubcat_id,
						  'price'=>$pro_price,
						  'discount_type'=>$discount_type,
						  'price_after_discount'=>$pro_discount,
						  'sizes'=> $sizes,
						  'color'=>$color,
						  'description'=>$pro_desc,
						  'specification'=>$pro_spec,
						  'is_featured'=>$in_featured ,
						  'display_status'=>$display_status ,
						  'added_date'=>date('Y-m-d H:i:s'));

			 if(!empty($templocation))
			 {
			   if($ext !="jpg" && $ext !="png" && $ext !="jpeg" && $ext !="gif" && $ext !='svg'){}
			   else
			   {
			     $newfilename = file_new_name_when_exist($store,$image_name);	
			     if(move_uploaded_file($templocation, $store.$newfilename))
			     {
				   $Add['cover_image'] = $store.$newfilename;
			     }
			   }			 
			 }			 

             $Add['deal_ids'] = '';
			 if(isset($_POST['deals']) && !empty($_POST['deals']))
			 {
			   $Add['deal_ids'] = implode(',',$_POST['deals']);
			 }
			

			   $this->db->where('id',$Id);
			   $this->db->update('products',$Add);
			 
			   //=====Multiple image upload start======
			   if(!empty($_FILES['product_image']['name']))
			   {
				  $filesCount = count($_FILES['product_image']['name']);
				  for($i = 0; $i < $filesCount; $i++)
				  {			   
					$_FILES['file']['name']     = $_FILES['product_image']['name'][$i];
					$_FILES['file']['type']     = $_FILES['product_image']['type'][$i];
					$_FILES['file']['tmp_name'] = $_FILES['product_image']['tmp_name'][$i];
					$_FILES['file']['error']    = $_FILES['product_image']['error'][$i];
					$_FILES['file']['size']     = $_FILES['product_image']['size'][$i]; 
					$uploadPath                 = 'uploads/product-img/';
					$config['upload_path']      = $uploadPath;
					$config['allowed_types']    = 'jpg|jpeg|png|gif'; 
					$this->load->library('upload', $config);
					$this->upload->initialize($config);                
				   
					if($this->upload->do_upload('file')){
						// Uploaded file data
						$fileData = $this->upload->data();
						$uploadData[$i]['file_name'] = $fileData['file_name'];
						$uploadData[$i]['uploaded_on'] = date("Y-m-d H:i:s");
					}

					if(!empty($uploadData))
					{				 
					  $AddImages = array('product_id'=>$Id,
										'image'=>$store.$uploadData[$i]['file_name']);
					  $this->db->insert("product_images",$AddImages);
					}		  
				  }
			   }
			   //=====Multiple image upload start======

			   //=====Add deals in product start======
			   $this->db->where('product_id',$Id);
			   $this->db->delete('product_deals');
			   if(isset($_POST['deals']) && !empty($_POST['deals']))
			   {
				foreach($_POST['deals'] as $deals)
				{
				  $Adddeal = array('deal_id'=>$deals,
								   'category_id'=>$pro_cat,
								   'sub_category_id'=>$subcat_id,
								   'subsub_category_id'=>$subsubcat_id,
								   'product_id'=>$Id);

				  $this->db->insert("product_deals",$Adddeal);
				}		  
			   }
			   //=====Add deals in product end======			 

			 redirect(admin_base_url.'products/?action=updated');	   
		   } 
		}	 
	 }
	 else
	 {
	   redirect(admin_base_url.'products');
	 }

     $this->load->view('management/backend_header');
	 $this->load->view('management/product/edit_product',$data);
	 $this->load->view('management/backend_footer');

   }

   public function deleted()
   {
   
   }


   public function deleteImg()
   {
     if(isset($_POST['Imgid']) && !empty($_POST['Imgid']) && is_numeric($_POST['Imgid']))
	 {
	    $this->db->where('id',$_POST['Imgid']);
	    $this->db->delete('product_images');
        echo 'success';
        if(file_exists($_POST['Imgnm']))
	    {		 
		   unlink($_POST['Imgnm']);
		} 
	 }
	 else
	 {
	   echo 'error';
	 }  
   }

   public function subcategories()
   {
      $id   = $_POST['id'];
	  $html = '<option value="">Select category</option>';
	  $status = false;
	  if(!empty($id) && is_numeric($id))
	  {
	    $category = $this->db->query('SELECT * FROM category WHERE type=1 AND parent_id='.$id.' AND display_status=1 ORDER BY id DESC')->result();
		if(!empty($category))
		{		 
		  $status = true;
		  foreach($category as $cat)
		  {
		     $html .='<option value="'.$cat->id.'">'.$cat->name.'</option>';
		  }
		}
	  }
	  echo json_encode(array('status'=>$status,'html'=>$html));
   }

}