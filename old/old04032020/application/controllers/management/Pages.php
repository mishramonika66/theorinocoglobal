<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CI_Controller 
{
  public function __construct()
  {    
    parent::__construct();        
    $this->load->helper('url');  
	$this->load->helper('email');
	$this->load->library('session');
	$this->load->database();
	if(empty($this->session->userdata('ID')))
	{
      redirect(admin_base_url.'auth/login');   
    }
	CheckAdminRole($this->session->userdata('ID'));
  }

  public function index()
  {
    $data['PagesRecord'] = $this->db->query("SELECT ID,page_slug,page_title,page_description,added_date FROM ".DB_PREFIX."content_pages WHERE ID NOT IN (1,2)")->result(); 
	
	
	$this->load->view('management/backend_header');
	$this->load->view('management/pages/listing_page_view',$data);
	$this->load->view('management/backend_footer'); 
  }

  public function add()
  {
    $data = array();  
	if(isset($_POST['add_page']) && $_POST['add_page']=='add')
	{
	  $page_title       = addslashes($_POST['page_title']);
	  $page_description = addslashes($_POST['page_description']);
	  if(empty($page_title))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter title.</strong></div>';
	  }
	  else if(empty($page_description))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter description.</strong></div>';
	  }
	  else
	  {
	    $Slug    = UniqueSlug($page_title,''.DB_PREFIX.'content_pages','page_slug');
		$AddInfo = array('page_slug'=>$Slug,
			             'page_title'=>$page_title,
			             'page_description'=>$page_description,
			             'added_date'=>date('Y-m-d H:i:s'));

		$this->db->insert(''.DB_PREFIX.'content_pages',$AddInfo);
		redirect(admin_base_url.'pages/add?action=added');	  
	  }	
	}
	
	$this->load->view('management/backend_header');
	$this->load->view('management/pages/add_page_view',$data);
	$this->load->view('management/backend_footer'); 
  }

  public function edit()
  {
    $data = array();
	$Id   = $this->uri->segment(4);
	if(!empty($Id) && is_numeric($Id) && $Id > 2)
	{
	  $data['PageRecord'] = $this->db->query("SELECT page_title,page_description,page_image FROM ".DB_PREFIX."content_pages WHERE ID=".$Id."")->result();

	  if(isset($_POST['edit_page']) && $_POST['edit_page']=='edit')
	  {
		  $page_title       = addslashes($_POST['page_title']);
		  $page_description = addslashes($_POST['page_description']);
		  if(empty($page_title))
		  {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter title.</strong></div>';
		  }
		  else if(empty($page_description))
		  {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter description.</strong></div>';
		  }
		  else
		  {
			$Slug    = UniqueSlug($page_title,''.DB_PREFIX.'content_pages','page_slug','ID',$Id);
			
			$AddInfo = array('page_slug'=>$Slug,
							 'page_title'=>$page_title,
							 'page_description'=>$page_description,
							 'added_date'=>date('Y-m-d H:i:s'));
            
			$this->db->where('ID',$Id);
			$this->db->update(''.DB_PREFIX.'content_pages',$AddInfo);
			redirect(admin_base_url.'pages/edit/'.$Id.'?action=updated');	  
		  }	
	  }
	}
	else
	{
	  redirect(admin_base_url.'pages');
	}
	
	$this->load->view('management/backend_header');
	$this->load->view('management/pages/edit_page_view',$data);
	$this->load->view('management/backend_footer'); 
  }

  public function deleted()
  {
    $Id   = $this->uri->segment(4);
	if(!empty($Id) && is_numeric($Id) && $Id > 2)
	{
	  $this->db->delete(''.DB_PREFIX.'content_pages', array('ID'=>$Id));
	  redirect(admin_base_url.'pages?action=deleted');	
	}  
  }
  

  
}