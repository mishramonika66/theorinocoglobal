<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SubCategory extends CI_Controller 
{
  public function __construct()
  {    
    parent::__construct();        
    $this->load->helper('url');  
	$this->load->helper('email');
	$this->load->library('session');
	$this->load->database();	
	if(empty($this->session->userdata('ID'))){
      redirect(admin_base_url.'auth/login');   
    }	
  }
  
  function index()
  {
	if( !isset($_GET['cat_id']) || empty($_GET['cat_id']) || !is_numeric($_GET['cat_id']) )
	{
	  redirect(admin_base_url.'category');
	}	
	
	$data['category'] = $this->db->query('SELECT * FROM category WHERE display_status !=2 AND parent_id='.$_GET['cat_id'].' ORDER BY id DESC')->result();

	$this->load->view('management/backend_header');
	$this->load->view('management/category/subcategory/listing_subcategory',$data);
	$this->load->view('management/backend_footer'); 
  }

  function add()
  {  
    if( !isset($_GET['cat_id']) || empty($_GET['cat_id']) || !is_numeric($_GET['cat_id']) )
	{
	  redirect(admin_base_url.'category');
	}

	$data['parentcat'] = $this->db->query('SELECT * FROM category WHERE parent_id=0 AND display_status=1 ORDER BY id DESC')->result();
	
	if(isset($_POST['add_category']) && $_POST['add_category']=='add')
	{
	  $category_name = addslashes($_POST['category_name']); 
	  $parent_id  = addslashes($_POST['parent_id']); 
      $display_status = addslashes($_POST['display_status']);

	  $image_name = addslashes($_FILES["image"]["name"]);
	  $templocation = $_FILES["image"]["tmp_name"];
	  $ext= strtolower(substr($image_name, strrpos($image_name, '.') + 1));
	  $store = "uploads/website-imgs/";
      if(!file_exists($store)){ @mkdir($store, 0777); }

	  if(empty($category_name))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter category name.</strong></div>';
	  }
	  else if(empty($parent_id))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please select parent category.</strong></div>';
	  }	  
	  else
	  {
	       $Add = array('parent_id'=>$parent_id,
			            'type'=>1,
			            'name'=>$category_name,
			            'display_status'=>$display_status);		   

		   if(!empty($templocation))
		   {
		     if($ext != "jpg" && $ext != "png" && $ext != "jpeg" && $ext != "gif" && $ext != 'svg'){}
			 else
			 {
			    $newfilename = file_new_name_when_exist($store,$image_name);	
			    if(move_uploaded_file($templocation, $store.$newfilename))
			    {
				  $Add['image'] = $store.$newfilename;
			    }
			 }
		   }           
		  		   
	       $this->db->insert('category',$Add);
		   redirect(admin_base_url.'subcategory?cat_id='.$_GET['cat_id'].'&action=added');
	  }
	}

	$this->load->view('management/backend_header');
	$this->load->view('management/category/subcategory/add_subcategory',$data);
	$this->load->view('management/backend_footer');
  }

  public function edit()
  {
    if( !isset($_GET['cat_id']) || empty($_GET['cat_id']) || !is_numeric($_GET['cat_id']) )
	{
	  redirect(admin_base_url.'category');
	}

	$Id   = $this->uri->segment(4);
	$data['parentcat'] = $this->db->query('SELECT * FROM category WHERE parent_id=0 AND display_status=1 ORDER BY id DESC')->result();
	
	if( !empty($Id) && is_numeric($Id) )
	{	  
	  $data['category'] = $this->db->query('SELECT * FROM category WHERE id='.$Id.'')->result();

	  if(isset($_POST['edit_category']) && $_POST['edit_category']=='edit')
	  {	    
		  $category_name     = addslashes($_POST['category_name']); 
		  $parent_id  = addslashes($_POST['parent_id']); 
		  $display_status    = addslashes($_POST['display_status']);
		  
		  $image_name = addslashes($_FILES["image"]["name"]);
	      $templocation = $_FILES["image"]["tmp_name"];	 
	      $target_file = $target_dir.basename($image_name);
	      $ext= strtolower(substr($image_name, strrpos($image_name, '.') + 1));
	      $store = "uploads/website-imgs/";
          if(!file_exists($store)){ @mkdir($store, 0777); }

		  if(empty($category_name))
		  {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter category name.</strong></div>';
		  }
		  else
		  {
		   $upd = array('parent_id'=>$parent_id,
						'type'=>1,
						'name'=>$category_name,						
					    'display_status'=>$display_status);

		   if(!empty($templocation))
	       {
		     if($ext != "jpg" && $ext != "png" && $ext != "jpeg" && $ext != "gif" && $ext != 'svg'){}
			 else
			 {
			   $newfilename = file_new_name_when_exist($store,$image_name);	
			   if(move_uploaded_file($templocation, $store.$newfilename))
		       {
                 $upd['image'] = $store.$newfilename;
				 if(!empty($_POST['old_img']) && file_exists($_POST['old_img']))
				 {
				   unlink($_POST['old_img']);
				 }
		       }
			 }
		   }
		   $this->db->where('id',$Id);
		   $this->db->update('category',$upd);
           redirect(admin_base_url.'subcategory?cat_id='.$_GET['cat_id'].'&action=updated');
		}
	  }	
	}
	else
	{
	  redirect(admin_base_url.'subcategory?cat_id='.$_GET['cat_id'].'');
	}

	$this->load->view('management/backend_header');
	$this->load->view('management/category/subcategory/edit_subcategory',$data);
	$this->load->view('management/backend_footer');  
  }  

  function deleted()
  {
    if( !isset($_GET['cat_id']) || empty($_GET['cat_id']) || !is_numeric($_GET['cat_id']) )
	{
	  redirect(admin_base_url.'category');
	}
	
	$Id   = $this->uri->segment(4);
	if( !empty($Id) && is_numeric($Id) )
	{
	  $Del = array('display_status'=>2);
	  $this->db->where('id',$Id);
	  $this->db->update('category',$Del);	 
	  redirect(admin_base_url.'subcategory?cat_id='.$_GET['cat_id'].'&action=deleted');
	}
	else
	{
	  redirect(admin_base_url.'subcategory?cat_id='.$_GET['cat_id'].'');
	}
  }
}