<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attributes extends CI_Controller 
{
  public function __construct()
  {    
    parent::__construct();        
    $this->load->helper('url');  
	$this->load->helper('email');
	$this->load->library('session');
	$this->load->database();	
	if(empty($this->session->userdata('ID'))){
      redirect(admin_base_url.'auth/login');   
    }
	$this->WhrCnd = ' AND type=1';
	$this->cat_id = '';
	$this->dbcat_id = 0;
	if( isset($_GET['cat_id']) && !empty($_GET['cat_id']) && is_numeric($_GET['cat_id']) )
	{
	  $this->WhrCnd = ' AND type='.$_GET['cat_id'].'';
	  $this->cat_id = '?cat_id='.$_GET['cat_id'];
	  $this->dbcat_id = $_GET['cat_id'];
	}
  }
  
  function index()
  {    
	$data['category'] = $this->db->query('SELECT * FROM attributes WHERE display_status !=2 '.$this->WhrCnd.' ORDER BY id DESC')->result();

	$this->load->view('management/backend_header');
	$this->load->view('management/attributes/listing_attributes',$data);
	$this->load->view('management/backend_footer'); 
  }

  function add()
  {  
    $data = array();	
	if(isset($_POST['add_category']) && $_POST['add_category']=='add')
	{
	  $category_name     = addslashes($_POST['category_name']); 	
      $display_status    = addslashes($_POST['display_status']);

	  if(empty($category_name))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter attribute name.</strong></div>';
	  }	  
	  else
	  {
		$Add = array('type'=>$this->dbcat_id,
			         'attribute_name'=>$category_name,
			         'display_status'=>$display_status);
	    $this->db->insert('attributes',$Add);
		   
		if(!empty($this->cat_id)){
		     redirect(admin_base_url.'attributes'.$this->cat_id.'&action=added');
		}else{
		     redirect(admin_base_url.'attributes?action=added');
		}   
	  }
	}

	$this->load->view('management/backend_header');
	$this->load->view('management/attributes/add_attributes',$data);
	$this->load->view('management/backend_footer');
  }

  public function edit()
  {
    $data = array();
	$Id   = $this->uri->segment(4);
	
	if( !empty($Id) && is_numeric($Id) )
	{	  
	  $data['category'] = $this->db->query('SELECT * FROM attributes WHERE id='.$Id.'')->result();

	  if(isset($_POST['edit_category']) && $_POST['edit_category']=='edit')
	  {	    
		  $category_name     = addslashes($_POST['category_name']); 		 
		  $display_status    = addslashes($_POST['display_status']);

		  if(empty($category_name))
		  {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter attribute name.</strong></div>';
		  }
		  else
		  {
		   $upd = array('type'=>$this->dbcat_id,
						'attribute_name'=>$category_name,						
					    'display_status'=>$display_status);

		   $this->db->where('id',$Id);
		   $this->db->update('attributes',$upd);

		   if(!empty($this->cat_id)){
		     redirect(admin_base_url.'attributes'.$this->cat_id.'&action=updated');
		   }else{
		     redirect(admin_base_url.'attributes?action=updated');
		   }		  		   
		}
	  }	
	}
	else
	{
	  redirect(admin_base_url.'attributes');
	}

	$this->load->view('management/backend_header');
	$this->load->view('management/attributes/edit_attributes',$data);
	$this->load->view('management/backend_footer');  
  }  

  function deleted()
  {
    $Id   = $this->uri->segment(4);
	if( !empty($Id) && is_numeric($Id) )
	{
	  $Del = array('display_status'=>2);
	  $this->db->where('id',$Id);
	  $this->db->update('attributes',$Del);
	  if(!empty($this->cat_id)){
	   redirect(admin_base_url.'attributes'.$this->cat_id.'&action=deleted');
	  }else{
	   redirect(admin_base_url.'attributes?action=deleted');
	  }
	}
	else
	{
	  redirect(admin_base_url.'attributes');
	}
  }
}