<?php

class AUTHORIZATION {

    public static function validateTimestamp($token) {
        $token = self::validateToken($token);
        if ($token != false && (now() - $token->timestamp < (get_instance()->config->item('token_timeout') * 60))) {
            return $token;
        }
        return false;
    }

    public static function validateToken($token) {
        return JWT::decode($token, get_instance()->config->item('jwt_key'));
    }

    public static function generateToken($data) {
        return JWT::encode($data, get_instance()->config->item('jwt_key'));
    }

}
