<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_Model extends CI_Model
{  
  public function __construct()
  {
    parent::__construct();
	$this->load->database();
  }

  public function countCartItem($productId,$userId)
  {
    $query = $this->db->query('SELECT id FROM carts WHERE product_id="'.$productId.'" AND ( user_id="'.$userId.'" OR temp_user_id="'.$userId.'" )');
    return $query->num_rows();
  }
  
   public function countwishlistItem($productId,$userId)
  {
    $query = $this->db->query('SELECT id FROM product_watchlist WHERE product_id="'.$productId.'" AND ( user_id="'.$userId.'" OR temp_user_id="'.$userId.'" )');
    return $query->num_rows();
  }
  
   public function get_all_record_by_condition($table, $data) {
        $query = $this->db->get_where($table, $data);
        return $query->result();
    }
    
    public function update_record_by_condition($table, $data, $where) {
        $query = $this->db->update($table, $data, $where);
        return 1;
    }
}

?>

