<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('email');
        $this->load->library('session');
        $this->load->database();
        if (empty($this->session->userdata('ID'))) {
            redirect(admin_base_url . 'auth/login');
        }
    }

    function index() {
        $data = array();
        $data['users'] = $this->db->query('SELECT *  FROM users WHERE role="user" ORDER BY ID DESC');
        $data['categories'] = $this->db->query('SELECT *  FROM category WHERE parent_id="0" AND display_status="1" ORDER BY ID DESC');
        $data['subcategories'] = $this->db->query('SELECT *  FROM category WHERE type="1" AND display_status="1"  ORDER BY ID DESC');
        $data['product'] = $this->db->query('SELECT *  FROM products WHERE  display_status="1"  ORDER BY ID DESC');

       


        $this->load->view('management/backend_header');
        $this->load->view('management/backend_dashboard', $data);
        $this->load->view('management/backend_footer');
    }

    function websetting() {
        $data['Setting'] = $this->db->query("SELECT * FROM global_settings WHERE 1")->result();

        if (isset($_POST['save_setting']) && $_POST['save_setting'] == 'save') {
            $email_address = addslashes($_POST['email_address']);

            if (empty($email_address) || !valid_email($email_address)) {
                $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter email.</strong></div>';
            } else {
                $SaveSetting = array('email_address' => $email_address,
                    'updated_date' => date('Y-m-d H:i:s'));

                $this->db->where('ID', 1);
                $this->db->update('' . DB_PREFIX . 'global_settings', $SaveSetting);
                redirect(admin_base_url . 'dashboard/websetting?action=added');
            }
        }

        $this->load->view('management/backend_header');
        $this->load->view('management/web_setting_view', $data);
        $this->load->view('management/backend_footer');
    }

}
