<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slider extends CI_Controller 
{
  public function __construct()
  {    
    parent::__construct();        
    $this->load->helper('url');  
	$this->load->helper('email');
	$this->load->library('session');
	$this->load->database();	
	if(empty($this->session->userdata('ID'))){
      redirect(admin_base_url.'auth/login');   
    }	
  }
  
  function index()
  {    
	$data['sliders'] = $this->db->query('SELECT * FROM sliders WHERE type=0 AND display_status !=2 ORDER BY id DESC')->result();

	$this->load->view('management/backend_header');
	$this->load->view('management/slider/listing_slider',$data);
	$this->load->view('management/backend_footer'); 
  }

  function add()
  {  
    $data = array();
	if(isset($_POST['add_slider']) && $_POST['add_slider']=='add')
	{
	  $display_status = $_POST['display_status'];
	  $image_name = addslashes($_FILES["image"]["name"]);
	  $templocation = $_FILES["image"]["tmp_name"];	
	  $ext= strtolower(substr($image_name, strrpos($image_name, '.') + 1));
	  $store = "uploads/slider-imgs/";
      if(!file_exists($store)){ @mkdir($store, 0777); }

	  if(empty($templocation))
	  {
	    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please upload image.</strong></div>';
	  }
	  else
	  {
	     if($ext != "jpg" && $ext != "png" && $ext != "jpeg" && $ext != "gif" && $ext != 'svg') 
		 {
            $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Only jpg,png,jpeg,jif images are accepted for upload.</strong></div>';
         }
		 else
		 {          
		   $newfilename = file_new_name_when_exist($store,$image_name);	
		   if(move_uploaded_file($templocation, $store.$newfilename))
		   {
             $Add = array('slider_image'=>$store.$newfilename,
			              'display_status'=>$display_status);
			 $this->db->insert('sliders',$Add);		
		   }		   
	       
           redirect(admin_base_url.'slider?action=added');	       
		 }   		   
	  }
	}

	$this->load->view('management/backend_header');
	$this->load->view('management/slider/add_slider',$data);
	$this->load->view('management/backend_footer');
  }

  public function edit()
  {    
	$Id = $this->uri->segment(4);
	if( !empty($Id) && is_numeric($Id) )
	{  
	  $data['slider'] = $this->db->query('SELECT * FROM sliders WHERE id='.$Id.'')->result();

	  if(isset($_POST['edit_slider']) && $_POST['edit_slider']=='edit')
	  {
		  $display_status = $_POST['display_status'];
		  $image_name = addslashes($_FILES["image"]["name"]);
	      $templocation = $_FILES["image"]["tmp_name"];	 	    
	      $ext= strtolower(substr($image_name, strrpos($image_name, '.') + 1));
	      $store = "uploads/slider-imgs/";
          if(!file_exists($store)){ @mkdir($store, 0777); }

		  if(empty($templocation) && empty($_POST['old_img']))
		  {
			$data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter category name.</strong></div>';
		  }
		  else
		  {
		   $upd = array('display_status'=>$display_status);

		   if(!empty($templocation))
	       {
		     if($ext != "jpg" && $ext != "png" && $ext != "jpeg" && $ext != "gif" && $ext != 'svg'){}
			 else
			 {
			   $newfilename = file_new_name_when_exist($store,$image_name);	
			   if(move_uploaded_file($templocation, $store.$newfilename))
		       {
                 $upd['slider_image'] = $store.$newfilename;
				 if(!empty($_POST['old_img']) && file_exists($_POST['old_img']))
				 {
				   unlink($_POST['old_img']);
				 }
		       }
			 }
		   }
		   $this->db->where('id',$Id);
		   $this->db->update('sliders',$upd);		   
		   redirect(admin_base_url.'slider?action=updated');		   		  		   
		}
	  }	
	}
	else
	{
	  redirect(admin_base_url.'slider');
	}

	$this->load->view('management/backend_header');
	$this->load->view('management/slider/edit_slider',$data);
	$this->load->view('management/backend_footer');  
  }  

  function deleted()
  {
    $Id   = $this->uri->segment(4);
	if( !empty($Id) && is_numeric($Id) )
	{
	  $Del = array('display_status'=>2);
	  $this->db->where('id',$Id);
	  $this->db->update('sliders',$Del);
	  redirect(admin_base_url.'slider?action=deleted');	 
	}
	else
	{
	  redirect(admin_base_url.'slider');
	}
  }
}