<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Product_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    //Get Slider | Home
    function getSliderRows($id = '') {
        if (!empty($id)) {
            $Whr = array('id' => $id, 'display_status' => 1, 'type' => 0);
            $query = $this->db->select('slider_image as image')->get_where('sliders', $Whr);
            return $query->row();
        } else {
            $Whr = array('display_status' => 1, 'type' => 0);
            $this->db->limit(5);
            $query = $this->db->select('slider_image as image')->get_where('sliders', $Whr);
            return $query->result();
        }
    }

    //Get All Deals | Home
    public function getDealsRows($id = '') {
        if (!empty($id)) {
            $Whr = array('id' => $id, 'display_status' => 1);
            $query = $this->db->select('*')->get_where('deals', $Whr);
            return $query->row();
        } else {
            $Whr = array('display_status' => 1);
            //$this->db->limit(10);
            $query = $this->db->select('*')->get_where('deals', $Whr);
            return $query->result();
        }
    }

    public function getallDealsRows($id = '') {
        if (!empty($id)) {
            $Whr = array('id' => $id, 'display_status' => 1);
            $query = $this->db->select('*')->get_where('deals', $Whr);
            return $query->row();
        } else {
            $Whr = array('display_status' => 1);
            //$this->db->limit(10);
            $query = $this->db->select('*')->get_where('deals', $Whr);
            return $query->result();
        }
    }

    //Get Individual Product According to Deal Id | Home
    public function getProductOnDeals($dealId, $type = '') {
        $WhrCnd = "";
        if ($type == 'dailydeal') {
            $WhrCnd = " AND DATE(pro.deal_end_date)='" . date('Y-m-d') . "'";
        }
        $deals = $this->db->query("SELECT pro.id,pro.product_name,pro.price,pro.discount,pro.discount_type,pro.price_after_discount,pro.cover_image,pro.deal_end_date from product_deals as deal LEFT JOIN products as pro ON deal.product_id=pro.id WHERE deal.deal_id=" . $dealId . " " . $WhrCnd . " ORDER BY pro.id desc LIMIT 5")->result();
        return $deals;
    }

    //Get Individual Product & Sub Category According to Deal Id | Home
    public function getProductWithCategoryOnDeals($dealId) {
        $deals = $this->db->query("SELECT pro.id,pro.product_name,pro.price,pro.discount,pro.discount_type,pro.price_after_discount,pro.cover_image,cat.name as category_name,cat.id as category_id from product_deals as deal LEFT JOIN products as pro ON deal.product_id=pro.id LEFT JOIN category as cat ON pro.sub_category_id=cat.id WHERE deal.deal_id=" . $dealId . " AND pro.sub_category_id != '' ORDER BY pro.id desc LIMIT 5")->result();
        return $deals;
    }

    //Get Today Deals Item | Home
    public function getProductRows($Id = '') {
        if (!empty($Id)) {
            $Whr = array('id' => $Id, 'display_status' => 1);
            $query = $this->db->select('id,product_name,price,discount_type,discount,price_after_discount,cover_image,deal_end_date,sizes,color,description,specification,total_sale,subsub_category_id,sub_category_id,category_id,quantity,deal_ids,item_sold')->get_where('products', $Whr);
            return $query->row_array();
        } else {
            $Whr = array('display_status' => 1, 'is_featured' => 1);
            $this->db->limit(6);
            $this->db->order_by("id", "desc");
            $query = $this->db->select('id,product_name,price,discount_type,discount,price_after_discount,cover_image,deal_end_date,deal_ids')->get_where('products', $Whr);
            return $query->result();
        }
    }

    //Get Featured Category | Home
    public function getCategoriesHome() {
        $this->db->select('id,name,image');
        $this->db->from('category');
        $this->db->where('parent_id', 0);
        $this->db->where('is_featured', 1);
        $this->db->where('display_status', 1);
        $parent = $this->db->get();
        $categories = $parent->result();
        $i = 0;
        foreach ($categories as $p_cat) {
            $categories[$i]->sub = $this->subCategoriesHome($p_cat->id);
            $i++;
        }
        return $categories;
    }

    //fetch all categories in admin
    public function getCategoriesadmin() {
        $this->db->select('id,name,image');
        $this->db->from('category');
        $this->db->where('parent_id', 0);
        $this->db->where('display_status', 1);
        $parent = $this->db->get();
        $categories = $parent->result();
        return $categories;
    }

    //Get Featured Sub Category | Home
    public function subCategoriesHome($id) {
        $this->db->select('id,name,image');
        $this->db->from('category');
        $this->db->where('parent_id', $id);
        $this->db->where('type', 1);
        $this->db->where('display_status', 1);
        $this->db->limit(2);
        $child = $this->db->get();
        $categories = $child->result();
        return $categories;
    }

    //Get All Categories | Category ::(1)
    public function getCategoriesRows() {
        $this->db->select('id,name,image');
        $this->db->from('category');
        $this->db->where('parent_id', 0);
        $this->db->where('display_status', 1);
        $parent = $this->db->get();
        $categories = $parent->result();
        $i = 0;
        foreach ($categories as $p_cat) {
            $categories[$i]->sub = $this->getSubCategoriesRows($p_cat->id);
            $i++;
        }
        return $categories;
    }

    //Get All Sub Categories | Category ::(2)
    public function getSubCategoriesRows($id) {
        $this->db->select('id,name,image');
        $this->db->from('category');
        $this->db->where('parent_id', $id);
        $this->db->where('type', 1);
        $this->db->where('display_status', 1);
        $child = $this->db->get();
        $categories = $child->result();
        $i = 0;
        foreach ($categories as $p_cat) {
            $categories[$i]->subsub = $this->getSubSubCategoriesRows($p_cat->id);
            $i++;
        }
        return $categories;
    }

    //Get All Sub Sub Categories | Category ::(3)
    public function getSubSubCategoriesRows($id) {
        $this->db->select('id,name,image');
        $this->db->from('category');
        $this->db->where('parent_id', $id);
        $this->db->where('type', 2);
        $this->db->where('display_status', 1);
        $child = $this->db->get();
        $categories = $child->result();
        return $categories;
    }

    //category_products_get
    public function getProductByCategory($CatId, $type = '', $ProductId = 0) {
        $Whr = "";
        if ($type == 'related') {
            $Whr = " AND id != " . $ProductId . "";
        }

        $products = $this->db->query("SELECT id,product_name,price,discount,discount_type,price_after_discount,cover_image from products WHERE subsub_category_id=" . $CatId . " " . $Whr . " ORDER BY id desc Limit 6")->result();
        return $products;
    }

    public function getProductsByCategory($CatId) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('products.category_id', $CatId);
        $this->db->where('products.display_status', 1);
        $query = $this->db->get();

        $result = $query->result_array();
        return $result;
    }

    public function getProductsBySearchBySubCategory($CatId) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('products.sub_category_id', $CatId);
        $this->db->where('products.display_status', 1);
        $query = $this->db->get();

        $result = $query->result_array();
        return $result;
    }

    public function getProductsBySearchBySubSubCategory($CatId) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->where('products.subsub_category_id', $CatId);
        $this->db->where('products.display_status', 1);
        $query = $this->db->get();

        $result = $query->result_array();
        return $result;
    }

    public function getSizeInProducts($Ids) {
        // $size = $this->db->query("SELECT id,attribute_name from size WHERE  id IN(" . $Ids . ") ORDER BY id asc")->result();
        $size = $this->db->query("SELECT id,attribute_name from size WHERE  id IN(" . $Ids . ") ORDER BY id asc")->result();
        return $size;
    }

    public function getColorInProducts($Ids) {
        $color = $this->db->query("SELECT id,attribute_name from color WHERE  id IN(" . $Ids . ") ORDER BY id asc")->result();
        return $color;
    }

    public function getProductRatings($ProductId) {
        $ratings = $this->db->query("SELECT rat.rating,rat.comments,rat.added_date,usr.first_name as sender_name,usr.status from product_ratings as rat LEFT JOIN users as usr ON rat.user_id=usr.id WHERE rat.product_id=" . $ProductId . " ORDER BY rat.id desc")->result();
        return $ratings;
    }

    public function getProductSlider($ProductId) {
        $images = $this->db->query("SELECT id,image from product_images WHERE product_id=" . $ProductId . "")->result();
        return $images;
    }

    public function getCategoeiesByDealId($DealId) {
        $dealCategory = $this->db->query("SELECT DISTINCT(deal.subsub_category_id) as category_id,cat.name FROM product_deals as deal LEFT JOIN category as cat ON deal.subsub_category_id=cat.id WHERE deal.deal_id=" . $DealId . "")->result_array();
        return $dealCategory;
    }

    public function getProductsByCategoryAndDealId($DealId, $CatId) {
        $dealProducts = $this->db->query("SELECT pro.id,pro.product_name,pro.quantity,pro.price,pro.discount_type,pro.discount,pro.price_after_discount,pro.sizes,pro.color,pro.description,pro.specification,pro.cover_image,pro.deal_end_date,pro.total_sale FROM product_deals as deal LEFT JOIN products as pro ON deal.product_id=pro.id WHERE deal.deal_id=" . $DealId . " AND deal.subsub_category_id=" . $CatId . "")->result_array();
        return $dealProducts;
    }

    public function getsubCategoriesByDealId($DealId) {
        $dealCategory = $this->db->query("SELECT DISTINCT
                                 (deal.sub_category_id) AS subcategory_id,
                                      cat.name
                                       FROM
                               product_deals AS deal
                               LEFT JOIN category AS cat
                                      ON
                        deal.sub_category_id = cat.id
                               WHERE
                       deal.deal_id = " . $DealId . "")->result_array();
        return $dealCategory;
    }

    public function getProductsBysubCategoryAndDealId($DealId, $CatId) {
        $today = date('Y-m-d');

        $dealProducts = $this->db->query("SELECT
                                            pro.*,
                                            deal.*
                                        FROM
                                            product_deals AS deal
                                        LEFT JOIN products AS pro
                                        ON
                                            deal.product_id = pro.id
                                        WHERE
                                   
                                          deal.deal_id = " . $DealId . " AND pro.display_status = '1' AND deal.sub_category_id = " . $CatId . "")->result_array();
        return $dealProducts;
    }

    public function getProductsBysubCategoryAndDealIdfiftypercentoff() {
        $dealProducts = $this->db->query("SELECT
                                            pro.*
                                           
                                        FROM
                                           
                                       products AS pro
                                       
                                        WHERE
                                            pro.discount = '50'  AND pro.display_status = '1'")->result_array();
        return $dealProducts;
    }

    public function find($table, $select = '*', $where = array(), $resultType = 'array', $orderby = array()) {

        $this->db->select($select);
        $this->db->from($table);
        if (!empty($where)) {
            foreach ($where as $key => $val) {
                $this->db->where($key, $val);
            }
        }
        if (!empty($orderby)) {
            foreach ($orderby as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $query = $this->db->get();
        if ($resultType == 'objects') {
            $result = $query->first_row();
        } else {
            $result = $query->first_row('array');
        }
        return $result;
    }

    public function getCountries() {
        $this->db->select('id,name,shortname,phonecode');
        $this->db->from('countries');
        $this->db->where('status', 1);
        $this->db->order_by('id');
        $parent = $this->db->get();
        $countries = $parent->result();
        return $countries;
    }

    /*
      product search
     */

    public function getProductBySearch($srchStrng) {
        $this->db->select('*');
        $this->db->from('products');
        $this->db->like('products.product_name', $srchStrng, 'match');
        $this->db->or_like('products.description', $srchStrng, 'match');
        $this->db->where('products.display_status', 1);


        $query = $this->db->get();

        $result = $query->result_array();
        return $result;
    }

    public function getProductBySearchByCategory($srchStrng) {
        $this->db->select('*');
        $this->db->from('category');
        $this->db->like('category.name', $srchStrng);
        $this->db->where('category.display_status', 1);
        $query = $this->db->get()->row();

        return $query;
    }

    public function getProductImages($id = null) {
        $ProductImagedata = $this->db->get_where('product_images', array('product_id' => $id))->result();
        return $ProductImagedata != NULL ? $ProductImagedata : [];
    }

    public function getColor() {
        $this->db->select('*');
        $this->db->from('color');
        $this->db->where('display_status', 1);
        $this->db->order_by('id');
        $parent = $this->db->get();
        $colors = $parent->result();
        return $colors;
    }

    public function getSize() {
        $this->db->select('*');
        $this->db->from('size');
        $this->db->where('display_status', 1);
        $this->db->order_by('id');
        $parent = $this->db->get();
        $sizes = $parent->result();
        return $sizes;
    }

}
