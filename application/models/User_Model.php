<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_Model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function insert_one_row($table, $data) {
        $query = $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        if ($insert_id)
            return $insert_id;
        else
            return false;
    }

    public function find($table, $select = '*', $where = array(), $resultType = 'array', $orderby = array()) {

        $this->db->select($select);
        $this->db->from($table);
        if (!empty($where)) {
            foreach ($where as $key => $val) {
                $this->db->where($key, $val);
            }
        }
        if (!empty($orderby)) {
            foreach ($orderby as $key => $val) {
                $this->db->order_by($key, $val);
            }
        }
        $query = $this->db->get();
        if ($resultType == 'objects') {
            $result = $query->first_row();
        } else {
            $result = $query->first_row('array');
        }
        return $result;
    }

    public function update_record_by_condition($table, $data, $where) {
        $query = $this->db->update($table, $data, $where);
        return 1;
    }

    public function generateOrderId() {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 8; $i++) {
            $res .= $chars[mt_rand(0, strlen($chars) - 1)];
        }
        return $res;
    }

    public function getallIncompleteOrder($sessId) {
        $this->db->select('*');
        $this->db->from('orders');
        $this->db->where('user_id', $sessId);
        $this->db->where('order_status', 1);
        $this->db->order_by("id", "DESC");
        $query = $this->db->get();
        $result = $query->result();
        
        if ($result != NULL) {
            foreach ($result as $key => $value) {
                
                
              $value->order_items = $this->db->order_by('order_id', 'DESC')->get_where('order_items', array('order_id'=> $value->id))->result();
                if ($value->order_items != NULL) {
                    foreach ($value->order_items as $value_key => $item) {
//                              $value->Total_amount =$item->Total_amount;
                          $item->product = $this->db->get_where('products', array('id' => $item->product_id))->row();
//                        $item->rating = 3; //rating by logged in user
//                        $item->rating_comment = 'Very Good';
//                        $item->total_avg_rating = 4.5; //avg rating from all the users who has purchased this
//                        $value->order_items[$value_key] = $item;
                         $item->rating = $this->db->order_by('order_id', 'DESC')->get_where('product_ratings', array('user_id' =>$sessId, 'product_id' => $item->product_id ,'order_id'=> $value->id))->row();
                       


                        
                    }
                }
                $result[$key] = $value;
            }
        }

        return $result;
    }
    
    

}
