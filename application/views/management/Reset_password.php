<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" >
        <title>Reset password</title>


    </head>


    <body>
        <div class="container center">
            <div class="card" style="width: 30rem;margin-left: 30%;margin-top: 10%;">
                <div class="card-header text-center">
                    <?php
                    $firstname = isset($user->first_name) && $user->first_name!=null? $user->first_name : '';
                    $lastname = isset($user->last_name) && $user->last_name!=null? $user->last_name : '';
                    ?>
                    Hello <?php echo $firstname .$lastname?>, Please enter your new password here.
                </div>
                <div class="card-body">
                    <p class="card-title text-center" id ="responseDiv"></p>
                    <div class="form-group">
                        <label for="new_password">New Password</label>
                        <input type="text" class="form-control" id="new_password"  placeholder="Enter New Password">

                    </div>
                    <div class="form-group">
                        <label for="cpassword">Confirm Password</label>
                        <input type="text" class="form-control" id="cpassword" placeholder="Enter New Password">
                    </div>

                    <button type="button" onclick="submit();" id ="btn" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>



        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script>

                        function submit() {
                            $.ajax({
                                url: "<?php echo base_url() . 'ChangePassword/save' ?>",
                                type: "POST",
                                dataType: "html",
                                data: {
                                    "new_password": $("#new_password").val(),
                                    "cpassword": $("#cpassword").val(),
                                    "hash": "<?php echo $token ?>"
                                },
                                beforeSend: function () {
//                    $("#searchbtn").attr("disabled", true);
                                },
                                success: function (msg) {
                                    $("#responseDiv").html(msg);
                                    $("#new_password").val(''),
                                          
                                            $("#cpassword").val(''),
                                            console.log(msg)

                                },

                                error: function (error) {
                                    $("#responseDiv").html(error.responseText);

                                },
                                complete: function () {
//                    $("#searchbtn").removeAttr("disabled");
                                    //
                                },

                                cache: false
                            });
                        }
        </script>
    </body>
</html>

