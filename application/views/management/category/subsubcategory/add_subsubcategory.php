<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">Create New Sub Sub Category</h4>
            </div>
            <div>
               <a href="<?php echo admin_base_url.'subsubcategory?cat_id='.$_GET['cat_id']; ?>" class="btn btn-primary btn-icon-text">
               <i class="ti-angle-left btn-icon-append"></i>  
               BACK
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <?php if(!empty($errmsg)) { echo $errmsg; } ?>
               <form class="forms-sample" method="post" enctype="multipart/form-data">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="category_name"><b>Name <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter name" value="<?php if(!empty($_POST['category_name'])){ echo $_POST['category_name']; } ?>">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="phone_number"><b>Upload Image <font color="red">*</font></b></label>
                           <input type="file" class="form-control" name="image" id="userfile1" />
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                         <div class="form-group">
                           <label for="parent_id"><b>Parent Category</b></label>
                           <select class="custom-select" id="parent_id" name="parent_id">
                              <!--<option value="0">Select parent category</option>-->
							  <?php if(!empty($parentcat)){ ?>
							  <?php foreach($parentcat as $category){ ?>
							  <?php if($_GET['cat_id']==$category->id){ ?>
                               <option value="<?=$category->id?>" <?php if(isset($_GET['cat_id']) && $_GET['cat_id']==$category->id){ echo 'selected'; } ?>>
							   <?=stripslashes($category->name)?></option>
							  <?php } } } ?>
                           </select>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="display_status"><b>Status</b></label>
                           <select class="custom-select" id="display_status" name="display_status">
                              <option value="1" selected>Active</option>
							  <option value="0">Inactive</option>
                           </select>
                        </div>
                     </div>
                  </div>

				 
				  <div class="col-sm-12">
                        <button type="submit" name="add_category" value="add" class="btn btn-primary btn-lg mr-2">Save</button> 
                     </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>