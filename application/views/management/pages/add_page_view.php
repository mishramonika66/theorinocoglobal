<?php
if(isset($_GET['action']) && $_GET['action']=='added' && empty($errmsg))
{
  $errmsg = '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Added successfully.</strong></div>';
}
?>
<!-- Everything goes inside this --> 
<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">ADD PAGE</h4>
            </div>
            <div>
               <a href="<?php echo admin_base_url.'pages'; ?>" class="btn btn-primary btn-icon-text">
               <i class="ti-angle-left btn-icon-append"></i>  
                 BACK
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">	    
              <div class="card">			  
                <div class="card-body">
                  <h4 class="card-title"></h4> 
				  <?php if(!empty($errmsg)) { echo $errmsg; } ?>
                  <form class="forms-sample" method="post">
                    <div class="form-group">
                      <label for="page_title"><b>Title <font color="red">*</font></b></label>
                      <input type="text" class="form-control" id="page_title" name="page_title" placeholder="Enter page title" value="<?php if(!empty($_POST['page_title'])){ echo $_POST['page_title']; } ?>">
                    </div>
					
					<div class="form-group">
                      <label for="page_description"><b>Description <font color="red">*</font></b></label>
                      <textarea class="form-control" id="page_description" name="page_description" placeholder="Enter page description"><?php if(!empty($_POST['page_description'])){ echo $_POST['page_description']; } ?></textarea>
					  <script type="text/javascript">
                        CKEDITOR.replace('page_description');			
                     </script>
                    </div>
                    
                    <button type="submit" name="add_page" value="add" class="btn btn-primary btn-lg mr-2">Add</button>                    
                  </form>
                </div>
              </div>

	  
	  </div>
   </div>
</div>
<!-- Everything goes inside this -->