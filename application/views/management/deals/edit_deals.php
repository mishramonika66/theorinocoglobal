<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#timer").change(function () {
            if ($(this).val() == "1") {
                $("#deal_time").show();
            } else {
                $("#deal_time").hide();
            }
        });
    });
</script>
<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">Create New Deal Category</h4>
            </div>
            <div>
               <a href="<?php echo admin_base_url.'deals'; ?>" class="btn btn-primary btn-icon-text">
               <i class="ti-angle-left btn-icon-append"></i>  
               BACK
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <?php if(!empty($errmsg)) { echo $errmsg; } ?>
               <form class="forms-sample" method="post" enctype="multipart/form-data">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="category_name"><b>Name <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter name" value="<?=$category[0]->name?>">
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="phone_number"><b>Upload Image <font color="red">*</font></b></label>
                           <input type="file" class="form-control" name="image" id="userfile1" />
                        </div>
                     </div>
					 <div class="col-sm-2 image_preview_new">
					    <input type="hidden" name="old_img" value="<?=$category[0]->image?>">
                        <?php if(!empty($category[0]->image) && file_exists($category[0]->image)){ ?>
						<div class="del_unit">
						   <img src="<?php echo base_url().$category[0]->image; ?>">
						</div>
						<?php } ?>
                     </div>
                  </div>
             <div class="row">    

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="pro_qty"><b>minimum <font color="red">*</font></b></label>
                                    <input type="number" min="0" class="form-control" id="minimum" name="minimum" placeholder="Enter Quantity"  value="<?php echo isset($category[0]->minimum) && $category[0]->minimum != null ? $category[0]->minimum:''?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="pro_qty"><b>maximum <font color="red">*</font></b></label>
                                    <input type="number" min="0" class="form-control" id="maximum" name="maximum" placeholder="Enter Quantity"  value="<?php echo isset($category[0]->maximum) && $category[0]->maximum != null ? $category[0]->maximum:''?>">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="category_name"><b>Color<font color="red">*</font></b></label>
                                    <input type="color" class="form-control" id="color" name="color" placeholder="Enter color"  value="<?php echo isset($category[0]->color) && $category[0]->color != null ?$category[0]->color:''?>">
                                </div>
                            </div>

                        </div>
                    <div class="row">    

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="display_status"><b>Timer</b></label>
                                    <select class="custom-select" id="timer" name="timer">
                                        <option value="1"<?php if($category[0]->isdeal_time !=null && $category[0]->isdeal_time !==1) { echo ' selected' ;?><?php } ?> >Deal Time</option>
                                        <option value="2"<?php if($category[0]->isdeal_time !=null && $category[0]->isdeal_time !==2) { echo  'selected' ;?><?php } ?>  >For each product</option>
                                    </select>
                                </div>
                            </div>

                        </div>
                    <div class="row" id ="deal_time">    

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pro_qty"><b>Start Time <font color="red">*</font></b></label>
                                    <input type="time"  class="form-control" id="start_time" name="start_time"  value="<?php echo isset($category[0]->start_time) && $category[0]->start_time != null ?$category[0]->start_time:''?>">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pro_qty"><b>Start Date <font color="red">*</font></b></label>
                                    <input type="date"  class="form-control" id="start_date" name="start_date"  value="<?php echo isset($category[0]->start_date) && $category[0]->start_date != null ?$category[0]->start_date:''?>">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pro_qty"><b>End Time <font color="red">*</font></b></label>
                                    <input type="time"  class="form-control" id="end_time" name="end_time"  value="<?php echo isset($category[0]->end_time) && $category[0]->end_time != null ?$category[0]->end_time:''?>">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pro_qty"><b>End Date <font color="red">*</font></b></label>
                                    <input type="date"  class="form-control" id="end_date" name="end_date" value="<?php echo isset($category[0]->end_date) && $category[0]->end_date != null ?$category[0]->end_date:''?>">
                                </div>
                            </div>

                        </div>

                  <div class="row"> 
                       <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="deal_desc"><b>Description <font color="red">*</font></b></label>
                                    <textarea name="deal_desc" id="deal_desc" class="form-control" rows="4" contenteditable="true" value="<?php echo isset($category[0]->deal_desc) && $category[0]->deal_desc != null ?$category[0]->deal_desc:''?>"></textarea>
                                </div>
                            </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="display_status"><b>Status</b></label>
                           <select class="custom-select" id="display_status" name="display_status">
                              <option value="1" <?php if($category[0]->display_status==1){ echo 'selected'; } ?>>Active</option>
							  <option value="0" <?php if($category[0]->display_status==0){ echo 'selected'; } ?>>Inactive</option>
                           </select>
                        </div>
                     </div>
                  </div>				 
				  <div class="col-sm-12">
                        <button type="submit" name="edit_category" value="edit" class="btn btn-primary btn-lg mr-2">Save</button> 
                     </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>