<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        $("#timer").change(function () {
            if ($(this).val() == "1") {
                $("#deal_time").show();
            } else {
                $("#deal_time").hide();
            }
        });
    });
</script>

<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between align-items-center">
                <div>
                    <h4 class="font-weight-normal font-transform mb-0">Create New Deal Category</h4>
                </div>
                <div>
                    <a href="<?php echo admin_base_url . 'deals'; ?>" class="btn btn-primary btn-icon-text">
                        <i class="ti-angle-left btn-icon-append"></i>  
                        BACK
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <?php
                    if (!empty($errmsg)) {
                        echo $errmsg;
                    }
                    ?>
                    <form class="forms-sample" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="category_name"><b>Name <font color="red">*</font></b></label>
                                    <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter name" value="<?php
                                    if (!empty($_POST['category_name'])) {
                                        echo $_POST['category_name'];
                                    }
                                    ?>">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="phone_number"><b>Upload Image <font color="red">*</font></b></label>
                                    <input type="file" class="form-control" name="image" id="userfile1" />
                                </div>
                            </div>
                        </div>

                        <div class="row">    

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="pro_qty"><b>minimum <font color="red">*</font></b></label>
                                    <input type="number" min="0" class="form-control" id="minimum" name="minimum" placeholder="Enter Quantity" value="">
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="pro_qty"><b>maximum <font color="red">*</font></b></label>
                                    <input type="number" min="0" class="form-control" id="maximum" name="maximum" placeholder="Enter Quantity" value="">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label for="category_name"><b>Color<font color="red">*</font></b></label>
                                    <input type="color" class="form-control" id="color" name="color" placeholder="Enter color" value="">
                                </div>
                            </div>

                        </div>

                        <div class="row">    

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="display_status"><b>Timer</b></label>
                                    <select class="custom-select" id="timer" name="timer">
                                        <option value="1" selected>Deal Time</option>
                                        <option value="2">For each product</option>
                                    </select>
                                </div>
                            </div>

                        </div>


                        <div class="row" id ="deal_time">    

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pro_qty"><b>Start Time <font color="red">*</font></b></label>
                                    <input type="time"  class="form-control" id="start_time" name="start_time"  value="">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pro_qty"><b>Start Date <font color="red">*</font></b></label>
                                    <input type="date"  class="form-control" id="start_date" name="start_date"  value="">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pro_qty"><b>End Time <font color="red">*</font></b></label>
                                    <input type="time"  class="form-control" id="end_time" name="end_time" value="">
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label for="pro_qty"><b>End Date <font color="red">*</font></b></label>
                                    <input type="date"  class="form-control" id="end_date" name="end_date" value="">
                                </div>
                            </div>

                        </div>

                        <div class="row">    

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="deal_desc"><b>Description <font color="red">*</font></b></label>
                                    <textarea name="deal_desc" id="deal_desc" class="form-control" rows="4" contenteditable="true"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="display_status"><b>Status</b></label>
                                    <select class="custom-select" id="display_status" name="display_status">
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <button type="submit" name="add_category" value="add" class="btn btn-primary btn-lg mr-2">Save</button> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>