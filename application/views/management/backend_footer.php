<footer class="footer">
    <div class="d-sm-flex justify-content-center justify-content-sm-between">
        <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright &copy; 2019 <!--SensibleParenting.--> All rights reserved.</span>
        <span class="float-none float-sm-right d-block mt-1 mt-sm-0 text-center"></span>
    </div>
</footer>
<!-- partial -->
</div>
<!-- main-panel ends -->
</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="<?php echo base_url(); ?>assets/back/js/vendor.bundle.base.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!--<script src="<?php echo base_url(); ?>assets/back/js/Chart.min.js"></script>-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="<?php echo base_url(); ?>assets/back/js/off-canvas.js"></script>
<script src="<?php echo base_url(); ?>assets/back/js/hoverable-collapse.js"></script>
<script src="<?php echo base_url(); ?>assets/back/js/template.js"></script>
<script src="<?php echo base_url(); ?>assets/back/js/todolist.js"></script>
<!--<script src="<?php echo base_url(); ?>assets/back/js/chart.js"></script>-->
<!-- endinject -->
<!-- Custom js for this page-->
<script src="<?php echo base_url(); ?>assets/back/js/dashboard.js"></script>
<!-- End custom js for this page-->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"/></script>
<script src="<?php echo base_url(); ?>assets/back/js/bootstrap-datepicker.min.js"></script>
<script>
    var BaseUrl = "<?php echo admin_base_url; ?>";
    $(document).ready(function () {
        $('.dataTables').DataTable();
    });
    $(".alert-danger,.alert-success, .alert-info").fadeTo(2000, 600).slideUp(600, function () {
        $(".alert-danger,.alert-success").slideUp(600);
    });
//sidebar-icon-only
</script>
<script src="<?php echo base_url(); ?>assets/back/js/orinocoglobal.js"></script>
<script type="text/javascript">
    function ChangeCategory() {
        $("#subcat_id").prop( "disabled", false );
        $("#subsubcat_id").prop( "disabled", true );
        $("#subsubcat_id").val('');
        var cat_id = $("#pro_cat").val();
        $.ajax({
            url: "<?php echo admin_base_url . 'products/get_sub_category'; ?>",
            method: "POST",
            data: {cat_id: cat_id},
            async: true,
            dataType: 'json',
            success: function (data) {

                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    html += '<option value=' + data[i].id + '>' + data[i].name + '</option>';
                }
                $('#subcat_id').html(html);

            }
        });

    }


</script>

<script type="text/javascript">
    function ChangeSubCategory() {
       $("#subsubcat_id").prop( "disabled", false );
        var sub_cat_id = $("#subcat_id").val();
        $.ajax({
            url: "<?php echo admin_base_url . 'products/get_sub_sub_category'; ?>",
            method: "POST",
            data: {sub_cat_id: sub_cat_id},
            async: true,
            dataType: 'json',
            success: function (data) {
                console.log(data);
                var html = '';
                var i;
                for (i = 0; i < data.length; i++) {
                    html += '<option value=' + data[i].id + '>' + data[i].name + '</option>';
                }
                $('#subsubcat_id').html(html);

            }
        });

    }


</script>


<script type="text/javascript">
    $("#description").focus(function () {
        if (document.getElementById('description').value === '') {
            document.getElementById('description').value += '• ';
        }
    });

    $("#description").keyup(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13')
        {
            document.getElementById('description').value += '• ';
        }
        var txtval = document.getElementById('description').value;
        if (txtval.substr(txtval.length - 1) == '\n')
        {
            document.getElementById('description').value = txtval.substring(0, txtval.length - 1);
        }
    });
</script>

</body>
</html>