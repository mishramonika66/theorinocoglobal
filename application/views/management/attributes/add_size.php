<style>
    .errordiv {
/*        margin: 5px;*/
        font-size: 14px;
    }
</style> 
<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">Create New Attribute</h4>
            </div>
            <div>
               <a href="<?php echo admin_base_url.'Size'.$this->cat_id; ?>" class="btn btn-primary btn-icon-text">
               <i class="ti-angle-left btn-icon-append"></i>  
               BACK
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-6 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <form class="forms-sample" method="post">
                  <div class="row">
                       
                     <div class="col-sm-6">
                        <div class="form-group">
                                        

                          
                      
                     
                      <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="form-group">
                           <label for="category_name"><b>Size <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter name" value="<?php if(!empty($_POST['category_name'])){ echo $_POST['category_name']; } ?>">
                         <span id="mobile-error-label"  class="errordiv text-danger"><i class="fa fa-info-circle"></i> <?php if(!empty($errmsg)) { echo $errmsg; } ?>
</span>
                        </div>
                     </div>
                       
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="form-group">
                           <label for="display_status"><b>Status</b></label>
                           <select class="custom-select" id="display_status" name="display_status">
                              <option value="1" selected>Active</option>
							  <option value="0">Inactive</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  

				 
				  <div class="col-sm-12">
                        <button type="submit" name="add_category" value="add" class="btn btn-primary btn-lg mr-2">Save</button> 
                     </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>