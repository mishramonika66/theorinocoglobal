<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between align-items-center">
                <div>
                    <h4 class="font-weight-normal font-transform mb-0">Create New Attribute</h4>
                </div>
                <div>
                    <a href="<?php echo admin_base_url . 'color' . $this->cat_id; ?>" class="btn btn-primary btn-icon-text">
                        <i class="ti-angle-left btn-icon-append"></i>  
                        BACK
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <?php
                    if (!empty($errmsg)) {
                        echo $errmsg;
                    }
                    ?>
                    <form class="forms-sample" method="post">
                        <div class="row">
                            <?php $Id = $this->uri->segment(5);
                            ?>
                          
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <label for="category_name"><b>Color <font color="red">*</font></b></label>
                                        <input type="color" class="form-control" id="category_name" name="category_name" placeholder="Enter name" value="<?= $category[0]->attribute_name ?>">
                                    </div>
                                </div>
                           
                           
                               
                           

                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label for="display_status"><b>Status</b></label>
                                    <select class="custom-select" id="display_status" name="display_status">
                                        <option value="1" <?php
                                        if ($category[0]->display_status == 1) {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if ($category[0]->display_status == 0) {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>
                                    </select>
                                </div>
                            </div>                  

                        </div>

                        <div class="col-sm-12">
                            <button type="submit" name="edit_category" value="edit" class="btn btn-primary btn-lg mr-2">Save</button> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>