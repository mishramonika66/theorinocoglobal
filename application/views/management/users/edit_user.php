<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">Edit New Member</h4>
            </div>
            <div>
               <a href="<?php echo admin_base_url.'users'; ?>" class="btn btn-primary btn-icon-text">
               <i class="ti-angle-left btn-icon-append"></i>  
               BACK
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <?php if(!empty($errmsg)) { echo $errmsg; } ?>
               <form class="forms-sample" method="post">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="user_name"><b>Name <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter name" value="<?php echo $User[0]->username; ?>">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="phone_number"><b>Phone Number <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="phone_number" name="phone_number" placeholder="Enter phone number" value="<?php echo $User[0]->phone_number; ?>">
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="user_email"><b>Email <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="user_email" name="user_email" placeholder="Enter email" value="<?php echo $User[0]->email; ?>">
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="user_password"><b>Password </b></label>
                           <input type="password" class="form-control" id="user_password" name="user_password" placeholder="Enter password" value="">
                        </div>
                     </div>
                  </div>

				  <div class="row">
				  <?php 
				  $permission = array();
				  if(!empty($User[0]->access_permission)){
				      $permission = explode(',',$User[0]->access_permission);
				  }
				  ?>
				   <!--
				   <div class="col-sm-2">
                        <div class="form-group">                           
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="all" name="permission[]" class="form-check-input" <?php if(!empty($permission) && in_array('all',$permission) ){ echo 'checked'; } ?>>All
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>
					 -->

                     <div class="col-sm-2">
                        <div class="form-group">
                          <label for="user_password"><b>Access Permission <font color="red">*</font></b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="staff" name="permission[]" class="form-check-input" <?php if(!empty($permission) && in_array('staff',$permission) ){ echo 'checked'; } ?>>Manage Staff
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>
                     
					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="location" name="permission[]" class="form-check-input" <?php if(!empty($permission) && in_array('location',$permission) ){ echo 'checked'; } ?>>Manage Location
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>

					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="vendors" name="permission[]" class="form-check-input" <?php if(!empty($permission) && in_array('vendors',$permission) ){ echo 'checked'; } ?>>Manage Vendors
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>

					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="items" name="permission[]" class="form-check-input" <?php if(!empty($permission) && in_array('items',$permission) ){ echo 'checked'; } ?>>Manage Items
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>

					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="orders" name="permission[]" class="form-check-input" <?php if(!empty($permission) && in_array('orders',$permission) ){ echo 'checked'; } ?>>Manage Orders
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>

					 <div class="col-sm-2">
                        <div class="form-group">
                           <label for="user_password"><b>&nbsp;</b></label>
                           <div class="form-group">
						       <div class="form-check">
								 <label class="form-check-label">
								 <input type="checkbox" value="reports" name="permission[]" class="form-check-input" <?php if(!empty($permission) && in_array('reports',$permission) ){ echo 'checked'; } ?>>View Reports
								 <i class="input-helper"></i><i class="input-helper"></i></label>
							   </div>
						   </div>
                        </div>
                     </div>


                  </div>

                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="user_type"><b>Role <font color="red">*</font></b></label>
                           <select class="custom-select" id="user_type" name="user_type">
                              <option value="staff" <?php if($User[0]->role=='staff'){ echo 'selected'; } ?>>Staff members</option>
                           </select>
                        </div>
                  </div>				  
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="user_status"><b>Status <font color="red">*</font></b></label>
                           <select class="custom-select" id="user_status" name="user_status">
						      <option value="0" <?php if($User[0]->active==0){ echo 'selected'; } ?>>Suspend this account</option>
                              <option value="1" <?php if($User[0]->active==1){ echo 'selected'; } ?>>Active</option>
                           </select>
                        </div>
                  </div>
				  <div class="col-sm-12">
                        <button type="submit" name="edit_user" value="edit" class="btn btn-primary btn-lg mr-2">Save</button> 
                     </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>