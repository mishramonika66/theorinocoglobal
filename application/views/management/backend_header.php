<?php 
$controller = $this->router->fetch_class();
$Methods    = $this->router->fetch_method();
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>OrinocoGlobal | Control Panel</title>
      <!-- plugins:css -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/back/css/themify-icons.css">
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/back/css/vendor.bundle.base.css">
      <!-- endinject -->
      <!-- plugin css for this page -->
      <!-- End plugin css for this page -->
      <!-- inject:css -->
      <link rel="stylesheet" href="<?php echo base_url(); ?>assets/back/css/style.css">
	  <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet"/>
<!--      <link href="<?php echo base_url(); ?>assets/back/css/bootstrap-datepicker.min.css" rel="stylesheet">-->
      <!-- endinject -->	  
      <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/back/images/face28.png">	  
   </head>
   <body>
      <div class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
         <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center">
            <a class="navbar-brand brand-logo mr-5" href="<?php echo admin_base_url.'dashboard'; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OrinocoGlobal</a>
            <a class="navbar-brand brand-logo-mini" href="<?php echo admin_base_url.'dashboard'; ?>"><small>Orinoco</small></a>
         </div>
         <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="ti-view-list"></span>
            </button>
            <ul class="navbar-nav navbar-nav-right">              
               <li class="nav-item nav-profile dropdown">
                  <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="profileDropdown">
                    <img src="<?php echo base_url(); ?>assets/back/images/face28.png" alt="profile"/>
                  </a>
                  <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="profileDropdown">                   
					 <a href="<?php echo admin_base_url.'dashboard/websetting'; ?>" class="dropdown-item">
                       <i class="ti-settings text-primary"></i>Settings
                     </a>	
                     <a href="<?php echo admin_base_url.'auth/logout'; ?>" class="dropdown-item">
                       <i class="ti-power-off text-primary"></i>Logout
                     </a>
                  </div>
               </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="ti-view-list"></span>
            </button>
         </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">    
      <nav class="sidebar sidebar-offcanvas" id="sidebar">
         <ul class="nav">		 
			
			<li class="nav-item <?php if($controller=='dashboard' && $Methods=='index'){ echo 'active'; } ?>">
                <a class="nav-link" href="<?php echo admin_base_url.'dashboard'; ?>">
                  <i class="ti-shield menu-icon text-success"></i><span class="menu-title">Dashboard</span>
                </a>
            </li>
            
            
			<li class="nav-item <?php if($controller=='users'){ echo 'active'; } ?>">
                <a class="nav-link" href="<?php echo admin_base_url.'users'; ?>">
                  <i class="ti-user menu-icon text-danger"></i><span class="menu-title">Users</span>
                </a>
            </li>
            

			<li class="nav-item <?php if($controller=='category'){ echo 'active'; } ?>">
                <a class="nav-link" href="<?php echo admin_base_url.'category'; ?>">
                   <i class="ti-bell menu-icon text-success"></i></i><span class="menu-title">Manage Categories</span>
                </a>
            </li>

			<li class="nav-item <?php if($controller=='deals'){ echo 'active'; } ?>">
                <a class="nav-link" href="<?php echo admin_base_url.'deals'; ?>">
                  <i class="ti-paragraph menu-icon text-warning"></i><span class="menu-title">Manage Deals</span>
                </a>
            </li>


            
            
            
			<li class="nav-item <?php if($controller=='Size'){ echo 'active'; } ?>">
                <a class="nav-link" href="<?php echo admin_base_url.'size';?>">
                  <i class="ti-paragraph menu-icon text-warning"></i><span class="menu-title">Manage Size Attributes</span>
                </a>
            </li>

			<li class="nav-item <?php if($controller=='Color'){ echo 'active'; } ?>">
                <a class="nav-link" href="<?php echo admin_base_url.'Color'; ?>">
                  <i class="ti-paragraph menu-icon text-warning"></i><span class="menu-title">Manage Color Attributes</span>
                </a>
            </li>

			<li class="nav-item <?php if($controller=='products'){ echo 'active'; } ?>">
                <a class="nav-link" href="<?php echo admin_base_url.'products'; ?>">
                  <i class="ti-paragraph menu-icon text-warning"></i><span class="menu-title">Manage Products</span>
                </a>
            </li>



         </ul>
      </nav>
      <!-- partial -->
      <div class="main-panel">