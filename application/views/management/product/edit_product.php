<div class="content-wrapper">
    <div class="row">
        <div class="col-md-12 grid-margin">
            <div class="d-flex justify-content-between align-items-center">
                <div>
                    <h4 class="font-weight-normal font-transform mb-0">Create New Product</h4>
                </div>
                <div>
                    <a href="<?php echo admin_base_url . 'products'; ?>" class="btn btn-primary btn-icon-text">
                        <i class="ti-angle-left btn-icon-append"></i>  
                        BACK
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">
                    <?php
                    if (!empty($errmsg)) {
                        echo $errmsg;
                    }
                    ?>
                    <form class="forms-sample" method="post" enctype="multipart/form-data">
                        <div class="row">

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pro_name"><b>Name <font color="red">*</font></b></label>
                                    <input type="text" class="form-control" id="pro_name" name="pro_name" placeholder="Enter name" value="<?= stripslashes($product[0]->product_name) ?>">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pro_qty"><b>Quantity <font color="red">*</font></b></label>
                                    <input type="number" min="0" class="form-control" id="pro_qty" name="pro_qty" placeholder="Enter Quantity" value="<?= stripslashes($product[0]->quantity) ?>">
                                </div>
                            </div>

                            <!--All Category | Start-->
                            <div class="col-sm-6">
                                <div class="form-group">

                                    <label for="pro_cat"><b>Category <font color="red">*</font></b></label>
                                    <select class="custom-select" id="pro_cat" name="pro_cat" onchange="ChangeCategory()">
                                        <option value="0">Select parent category</option>
                                        <?php if (!empty($parentcat)) { ?>
                                            <?php foreach ($parentcat as $category) { ?>
                                                <option value="<?php echo $category->id; ?>" <?php
                                                if ($category->id == $product[0]->category_id) {
                                                    echo 'selected';
                                                }
                                                ?> ><?php echo stripslashes($category->name); ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            $Subcategories = $this->Product_Model->subCategoriesHome($product[0]->category_id);
                            ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="subcategory_id"><b>Sub Category </b></label>
                                    <select class="custom-select" id="subcat_id" name="subcat_id" onchange="ChangeSubCategory()">
                                        <?php foreach ($Subcategories as $value) { ?>
                                            <option value="<?php echo $value->id ?>" <?php echo$product[0]->sub_category_id == $value->id ? 'selected=""' : '' ?>><?php echo $value->name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            $SubSubcategories = $this->Product_Model->getSubSubCategoriesRows($product[0]->sub_category_id);
                            ?>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="subsubcategory_id"><b>Sub Sub Category </b></label>
                                    <select class="custom-select" id="subsubcat_id" name="subsubcat_id" style="min-height:46px;" >
                                        <?php foreach ($SubSubcategories as $value) { ?>
                                            <option value="<?php echo $value->id ?>" <?php echo$product[0]->subsub_category_id == $value->id ? 'selected=""' : '' ?>><?php echo $value->name ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <!--All Category | End-->

                            <!--Price With Discount | Start-->
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="pro_price"><b>Price <font color="red">*</font></b></label>
                                    <input type="number" class="form-control" id="pro_price" name="pro_price" placeholder="Enter price" value="<?= stripslashes($product[0]->price) ?>" min="0">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="category_name"><b>Discount</b></label>
                                    <input type="number" class="form-control" id="pro_discount" name="pro_discount" placeholder="Enter name" value="<?= stripslashes($product[0]->discount) ?>" min="0">
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="discount_type"><b>Discount Type <font color="red">*</font></b></label>
                                    <select class="custom-select" id="discount_type" name="discount_type" style="min-height:46px;">  
                                        <option value="0" <?php
                                        if ($product[0]->discount_type == 0) {
                                            echo 'selected';
                                        }
                                        ?>>None</option>
                                        <option value="1" <?php
                                        if ($product[0]->discount_type == 1) {
                                            echo 'selected';
                                        }
                                        ?>>In Percentage</option>
                                        <option value="2" <?php
                                        if ($product[0]->discount_type == 2) {
                                            echo 'selected';
                                        }
                                        ?>>Fixed</option>
                                    </select>
                                </div>
                            </div>

                            <!--<div class="col-sm-6">
           <div class="form-group">
              <label for="category_name"><b>Discount Price <font color="red">*</font></b></label>
              <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter name" value="<?php
                            if (!empty($_POST['category_name'])) {
                                echo $_POST['category_name'];
                            }
                            ?>">
           </div>
        </div>-->

                            <!--Price With Discount | End-->


                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="sizes"><b>Available Size <font color="red">*</font></b></label>
                                    <select class="custom-select" id="sizes" name="sizes[]" multiple> 
                                        <?php if (!empty($sizes)) { ?>
                                            <?php foreach ($sizes as $size) { ?>
                                                <option value="<?= $size->id ?>"><?= stripslashes($size->attribute_name) ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="color"><b>Available Color <font color="red">*</font></b></label>
                                    <select class="custom-select" id="color" name="color[]" multiple> 
                                        <?php if (!empty($colors)) { ?>
                                            <?php foreach ($colors as $color) { ?>
                                                <option value="<?php echo $color->id; ?>"><?php echo stripslashes($color->attribute_name); ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="pro_desc"><b>Description <font color="red">*</font></b></label>
                                    <textarea name="pro_desc" id="pro_desc" class="form-control" rows="4"><?= $product[0]->description ?></textarea>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="pro_spec"><b>Specification <font color="red">*</font></b></label>
                                    <textarea class="form-control" name="pro_spec" id="pro_spec" rows="4"><?= stripslashes($product[0]->specification) ?></textarea>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="cover_img"><b>Cover Image <font color="red">*</font></b></label>
                                    <input type="file" class="form-control" name="cover_img">
                                </div>
                            </div>

                            <div class="col-sm-6">    
                                <div class="image_preview_new">
                                    <input type="hidden" name="old_img" value="<?= $product[0]->cover_image ?>">
                                    <?php if (!empty($product[0]->cover_image) && file_exists($product[0]->cover_image)) { ?>
                                        <div class="del_unit">
                                            <img src="<?php echo base_url() . $product[0]->cover_image; ?>">
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="pro_imgs"><b>Upload Other Images </b></label>
                                    <input type="file" class="form-control" name="product_image[]" multiple>
                                    <div class="image_preview_new">
                                        <?php
                                        if (!empty($images)) {
                                            foreach ($images as $image) {
                                                if (!empty($image['image']) && file_exists($image['image'])) {
                                                    ?>
                                                    <div class="del-pro-img del-img del_unit" id="del-pro-img-<?= $image['id'] ?>">								
                                                        <img src="<?php echo base_url() . $image['image']; ?>">
                                                        <span class="del-pro-img del-pro-img-icon" data-imgnm="<?= $image['image'] ?>" data-imgid="<?= $image['id'] ?>" class="del-pro-img"><a href="javascript:void(0);"><i class="ti-trash" style="color:red;"></i></a></span>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="in_featured"><b>Is Show In Featured Product <font color="red">*</font></b></label>
                                    <select class="custom-select" id="in_featured" name="in_featured"> 
                                        <option value="0" <?php
                                        if ($product[0]->is_featured == 0) {
                                            echo 'selected';
                                        }
                                        ?>>No</option>
                                        <option value="1" <?php
                                        if ($product[0]->is_featured == 1) {
                                            echo 'selected';
                                        }
                                        ?>>Yes</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="display_status"><b>Status <font color="red">*</font></b></label>
                                    <select class="custom-select" id="display_status" name="display_status">      
                                        <option value="1" <?php
                                        if ($product[0]->display_status == 1) {
                                            echo 'selected';
                                        }
                                        ?>>Active</option>
                                        <option value="0" <?php
                                        if ($product[0]->display_status == 1) {
                                            echo 'selected';
                                        }
                                        ?>>Inactive</option>
                                    </select>
                                </div>
                            </div>                    
                        </div>

                        <!--Add In Deals | Start-->
                       <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive table-sm mt-2">
                                    <h4>Attach products with <i class="text-primary">Deals</i></h4>
                                    <table class="table">
                                        <?php if (!empty($deals)) { ?>
                                            <thead>
                                                <tr>
                                                    <th>Deal</th>
                                                    <th>Start Time</th>
                                                    <th>Start Date</th>
                                                    <th>End Time</th>
                                                    <th>End Date</th>
                                                    <th>Deal Info</th>
                                                    
                                                </tr>
                                            </thead>
                                            <?php foreach ($deals as $key => $deal) { ?>
                                                <tr>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <div class="form-check">
                                                                    <label class="form-check-label">
                                                                        <input type="checkbox" value="<?= $deal->id ?>" name="deals[<?php echo $key ?>][deal_id]" class="form-check-input"><?= stripslashes($deal->name) ?>
                                                                        <i class="input-helper"></i><i class="input-helper"></i></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td><input type="time" name="deals[<?php echo $key ?>][deal_start_time]" placeholder="Deal start Time"></td>
                                                    <td><input type="date" name="deals[<?php echo $key ?>][deal_start_date]" placeholder="Deal start Date"></td>
                                                    <td><input type="time" name="deals[<?php echo $key ?>][deal_end_time]" placeholder="Deal end Time"></td>
                                                    <td><input type="date" name="deals[<?php echo $key ?>][deal_end_date]" placeholder="Deal end date"></td>
                                                    <td> <textarea  id ="description" name="deals[<?php echo $key ?>][deal_info]" placeholder="Deal_info"></textarea></td>

                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <!--Add In Deals | End-->

                        <div class="col-sm-12">
                            <button type="submit" name="add_product" value="add" class="btn btn-primary btn-lg mr-2">Save</button> 
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

