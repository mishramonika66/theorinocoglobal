<?php

/*
 *  @Author: Sakshi
 *  @FileName: 
 *  @Date: 
 */

/**
 * Description of Searchfactory
 *
 * @author parangat-pt-09
 */
class Searchfactory {
    //put your code here
    
    public function __construct() {
        $this->_ci = &get_instance();
    }
    
    
    
    public function productSearch($post){
        $productData = $this->_ci->Product_Model->getProductBySearch($post['search_text']);
        if(!empty($productData)){
            return $productData;
        } else {
           return []; 
        }
    }
    
    public function storeSearch($post){
        $storeData = $this->_ci->StoreModel->getStoreBySearch($post['search_text'],$post['lat'],$post['lng']);
        if(!empty($storeData)){
            return $storeData;
        } else {
           return []; 
        }
    }
}
