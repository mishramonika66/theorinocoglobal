<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require(APPPATH . '/libraries/Format.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Order extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']);
        $this->load->model(array('Product_Model', 'Order_Model'));
        $this->load->helper(array('email', 'url'));
    }

    private function verify_request($token = null) {
        if (isset($token) && !empty($token)) {
            try {
                $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                    $this->response($response, $status);
                    exit();
                } else {
                    $token_db = ValidateJwtToken($token);
                    if ($token_db == false) {
                        $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                        $this->response($response, $status);
                    } else {
                        return $token_db;
                    }
                }
            } catch (Exception $e) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                $this->response($response, $status);
            }
        } else {
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
            $this->response($response, $status);
        }
    }

    public function guest_verify_request($token = null, $sessId = '') {
        try {
            $token = $sessId; // Remove this when you think you have fixed JWT
            $headers = $this->input->request_headers();
            if (isset($token)) {
                $data = AUTHORIZATION::validateToken($token);
                if ($data == false) {
                    $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                    return $this->response($response, parent::HTTP_UNAUTHORIZED);
                } else {
                    $token_db = ValidateJwtToken($token);
                    if ($token_db == false) {
                        $response = ['status' => FALSE, 'message' => 'Failed to validate the token.'];
                        return $this->response($response, parent::HTTP_UNAUTHORIZED);
                    } else {
                        return $token_db;
                    }
                }
            } else {
                if (empty($sessId) || !is_numeric($sessId)) {
                    $response = ['status' => FALSE, 'message' => 'No Token or Session Token received.'];
                    return $this->response($response, parent::HTTP_UNAUTHORIZED);
                }
                $response = ['status' => 'guest', 'id' => $sessId];
                return arrayToObject($response);
            }
        } catch (Exception $e) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response, parent::HTTP_UNAUTHORIZED);
        }
    }

    /*
     * ADD TO CART| START HERE
     */

    public function add_to_cart_post() {
        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (is_object($record)) {
            $quantity = $this->input->post('quantity');
            $product_id = $this->post('product_id');
            $color_id = $this->post('color_id');
            $size_id = $this->post('size_id');

            if (!$quantity || $quantity <= 0) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Invalid quantity.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            } else if (empty($product_id) || !is_numeric($product_id)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Invalid product.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            } else {
                $this->db->select('*');
                $this->db->from('carts');
                $this->db->where('user_id', $record->id);
                $this->db->where('temp_user_id', $sessId);
                $this->db->where('product_id', $product_id);
                $existingCartResult = $this->db->get()->row();

                $quantity = is_object($existingCartResult) ? $existingCartResult->quantity + $quantity : $quantity;

                $this->db->select('*');
                $this->db->from('products');
                $this->db->where('id', $product_id);
                $this->db->where('display_status', 1);
                $avaliabilityQtyproducts = $this->db->get()->row();
                /*
                 * check for availablity
                 */
                if (is_object($avaliabilityQtyproducts)) {
                    if ($avaliabilityQtyproducts->quantity >= $quantity) {
                        $Inputs = $this->input->post();
                        $TotalPrice = ($quantity * $avaliabilityQtyproducts->price_after_discount);
                        /*
                         * check for existance if exist update else create
                         */
//                         if (is_object($existingCartResult) && ($existingCartResult->quantity < $avaliabilityQtyproducts->quantity)) {

                        if (is_object($existingCartResult) && $quantity > 0) {
                            $Result = $this->db->update('carts', [
                                'price_after_discount' => $avaliabilityQtyproducts->price_after_discount,
                                'quantity' => $quantity,
                                'color_id' => isset($Inputs['color_id']) ? $Inputs['color_id'] : $existingCartResult->color_id,
                                'size_id' => isset($Inputs['size_id']) ? $Inputs['size_id'] : $existingCartResult->size_id,
                                'total_price' => $TotalPrice,
                                'added_date' => date('Y-m-d H:i:s'),
                                    ],
                                    [
                                        'id' => $existingCartResult->id // where
                                    ]
                            );
                        } elseif ($quantity == 0) {
                            $Result = $this->db->delete('carts', ['id' => $existingCartResult->id]);
                        } else {
                            $Result = $this->db->insert('carts', [
                                'temp_user_id' => isset($Inputs['temp_user_id']) ? $Inputs['temp_user_id'] : $sessId,
                                'user_id' => isset($record->id) ? $record->id : '',
                                'product_id' => $product_id,
                                'price_after_discount' => $avaliabilityQtyproducts->price_after_discount,
                                'quantity' => $quantity,
                                'color_id' => isset($Inputs['color_id']) ? $Inputs['color_id'] : '',
                                'size_id' => isset($Inputs['size_id']) ? $Inputs['size_id'] : '',
                                'total_price' => $TotalPrice,
                                'added_date' => date('Y-m-d H:i:s'),
                            ]);
                        }

                        if ($Result) {
                            $carts = $this->db->query("SELECT 
                                
                                col.attribute_name as selectedcolor,
                                siz.attribute_name as selectedsize,
                                cart.id as id,
                                cart.shipping_method,
                                cart.shipping_price ,
                                cart.chenname ,
                                cart.channelcode,
                                cart.reftime ,
                                cart.stylename ,
                                cart.styleremark ,
                               
                                wishlist.id as wishlist_id,
                                pro.id as product_id,
                                pro.product_name,
                                pro.price,
                                pro.price_after_discount,
                                pro.discount_type,
                                pro.cover_image,
                                cart.quantity,
                                (
                                    cart.quantity * pro.price_after_discount
                                ) AS total_price
                            FROM
                                carts AS cart
                            LEFT JOIN products AS pro
                            ON
                                cart.product_id = pro.id
                            left join product_watchlist as wishlist
                            ON cart.product_id = wishlist.product_id
                            AND cart.user_id = wishlist.user_id
                            
                       LEFT JOIN color AS col
                            ON
                                cart.color_id = col.id
                                left join size as siz
                                ON cart.size_id = siz.id
                            WHERE
                                (
                                    cart.user_id = '" . $record->id . "' OR cart.temp_user_id = '" . $sessId . "'
                                ) AND pro.display_status = 1 AND pro.quantity > 0")->result();
                               $country_id = 0;
                            $country_name = '';
                            $country = $this->db->get_where('users', array('id' => $record->id))->row();
                            $country_code = $this->db->get_where('countries', array('phonecode' => $country->country_code))->row();
                            $country_id = isset($country_code->base_placeId) && $country_code->base_placeId !=null ? $country_code->base_placeId: 165;
                           $country_name = isset($country_code->name) &&  $country_code->name != null ?  $country_code->name : 'Nigeria';
//
//            var_dump($carts);die;
//           
                            if (!empty($carts)) {


                                foreach ($carts as $key => $value) {


                                    $shippingMethods = $country_id == 0 ? [] : $this->getshippingmethods($country_id);

                                    $value->shippingcountry_code = $country_id;
                                    $value->country_name = $country_name;
                                    $value->shippingMethods = $shippingMethods;
//                  $carts[$key] = $value;



                                    $color = $this->getColorByProductId($value->product_id);
                                    $value->colors = $color;
//                    $carts[$key] = $value;

                                    $size = $this->getSizeByProductId($value->product_id);
                                    $value->sizes = $size;
                                    $carts[$key] = $value;
                                }
                            }

                            $Response['result']['status'] = TRUE;
                            $Response['result']['message'] = count($carts) ? 'Product is added to cart successfully.' : 'Cart is empty.';
                            $Response['result']['carts'] = $carts;
                            return $this->response($Response, REST_Controller::HTTP_OK);
                        } else {
                            $Response['result']['status'] = FALSE;
                            $Response['result']['message'] = 'Something went wrong,Please rise a request to our support team.';
                            return $this->response($Response, REST_Controller::HTTP_OK);
                        }
                    } else {
                        $Response['result']['status'] = FALSE;
                        $Response['result']['message'] = 'Product is out of stock or available less than the quantity you have mentioned.';
                        return $this->response($Response, REST_Controller::HTTP_OK);
                    }
                } else {
                    $Response['result']['status'] = FALSE;
                    $Response['result']['message'] = 'No such product is available.';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
            }
        }
    }

    public function add_to_cart_post_old() {
        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (!empty($record) && is_object($record)) {
            $quantity = $this->post('quantity');
            $product_id = $this->post('product_id');

            $color_id = $this->post('color_id');
            $size_id = $this->post('size_id');

            if (empty($quantity)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Invalid quantity.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            } else if (empty($product_id) || !is_numeric($product_id)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Invalid product.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            } else {
                $inStock = getProductRow($product_id, $quantity);
                if (!empty($inStock)) {
                    $AddCart = array('product_id' => $product_id,
                        'price_after_discount' => $inStock->price_after_discount,
                        'quantity' => $quantity,
                        'color_id' => $color_id,
                        'size_id' => $size_id,
                        'total_price' => ($quantity * $inStock->price_after_discount),
                        'added_date' => date('Y-m-d H:i:s'));

                    if (isset($record->status) && $record->status == 'guest') {
                        $AddCart['temp_user_id'] = $sessId;
                    } else {
                        $AddCart['user_id'] = $record->id;
                        $sessId = $record->id;
                    }

                    $cartItem = $this->Order_Model->countCartItem($product_id, $sessId);

                    if ($cartItem > 0) {
                        $or_where = array('user_id', $sessId, 'temp_user_id' => $sessId);
                        $this->db->where('product_id', $product_id);
                        $this->db->or_where($or_where);
                        $this->db->update('carts', $AddCart);
                    } else {
                        $this->db->insert('carts', $AddCart);
                    }
                    $Response['result']['status'] = TRUE;
                    $Response['result']['message'] = 'Add to cart successfully.';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Out of stock or quantity is more then availability.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function update_to_cart_post() {
        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);

        if (is_object($record)) {
            $quantity = $this->post('quantity');
            $product_id = $this->post('product_id');
            $color_id = $this->post('color_id');
            $size_id = $this->post('size_id');
            $cart_id = $this->post('cart_id');
            $data = [];

            /*
             * check if product is already in cart, if so how many qty user already has
             * if the product is already added in the cart then calc.total qty
             * now check in products for the calculate qty for its availability
             */
            if (empty($cart_id)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Cart id is required.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            } else {
                $this->db->select('*');
                $this->db->from('carts');
                $this->db->where('user_id', $record->id);
                $this->db->or_where('temp_user_id', $sessId);
                $this->db->where('product_id', $product_id);
                $existingCartResult = $this->db->get()->row();



                $quantity = $this->post('quantity');
                $this->db->select('*');
                $this->db->from('products');
                $this->db->where('id', $existingCartResult->product_id);
                $this->db->where('display_status', 1);
                $avaliabilityQtyproducts = $this->db->get()->row();

                /*
                 * check for availablity
                 */
                if (is_object($avaliabilityQtyproducts)) {
                    if ($avaliabilityQtyproducts->quantity >= $quantity) {
                        $Inputs = $this->input->post();
                        $TotalPrice = ($quantity * $avaliabilityQtyproducts->price_after_discount);
                        /*
                         * check for existance if exist update else create
                         */
                        if (is_object($existingCartResult) && $quantity > 0) {

                            $Result = $this->db->update('carts', [
                                'price_after_discount' => $avaliabilityQtyproducts->price_after_discount,
                                'quantity' => $quantity,
                                'color_id' => isset($Inputs['color_id']) ? $Inputs['color_id'] : $existingCartResult->color_id,
                                'size_id' => isset($Inputs['size_id']) ? $Inputs['size_id'] : $existingCartResult->size_id,
                                'total_price' => $TotalPrice,
                                'added_date' => date('Y-m-d H:i:s'),
                                    ],
                                    [
                                        'id' => $cart_id // where
                                    ]
                            );
                        } elseif ($quantity == 0) {

                            $Result = $this->db->delete('carts', ['id' => $cart_id]);
                        }


//                        } else {
//                          
//                            $Result = $this->db->insert('carts', [
//                                'temp_user_id' => isset($Inputs['temp_user_id']) ? $Inputs['temp_user_id'] : $sessId,
//                                'user_id' => isset($record->id) ? $record->id : '',
//                                'product_id' => $product_id,
//                                'price_after_discount' => $avaliabilityQtyproducts->price_after_discount,
//                                'quantity' => $quantity,
//                                'color_id' => isset($Inputs['color_id']) ? $Inputs['color_id'] : '',
//                                'size_id' => isset($Inputs['size_id']) ? $Inputs['size_id'] : '',
//                                'total_price' => $TotalPrice,
//                                'added_date' => date('Y-m-d H:i:s'),
//                            ]);
//                        }

                        if ($Result) {
                            $carts = $this->db->query("SELECT 
                                
                                col.attribute_name as selectedcolor,
                                siz.attribute_name as selectedsize,
                                cart.id as id,
                                wishlist.id as wishlist_id,
                                cart.shipping_method,
                                cart.shipping_price ,
                                cart.chenname ,
                                cart.channelcode,
                                cart.reftime ,
                                cart.stylename ,
                                cart.styleremark ,
                                pro.id as product_id,
                                pro.product_name,
                                pro.price,
                                pro.price_after_discount,
                                pro.discount_type,
                                pro.cover_image,
                                cart.quantity,
                                (
                                    cart.quantity * pro.price_after_discount
                                ) AS total_price
                            FROM
                                carts AS cart
                            LEFT JOIN products AS pro
                            ON
                                cart.product_id = pro.id
                                left join product_watchlist as wishlist
                                ON cart.product_id = wishlist.product_id
                                AND cart.user_id = wishlist.user_id
                                

                          LEFT JOIN color AS col
                            ON
                                cart.color_id = col.id
                                left join size as siz
                                ON cart.size_id = siz.id

                            WHERE
                                (
                                    cart.user_id = '" . $record->id . "' OR cart.temp_user_id = '" . $sessId . "'
                                ) AND pro.display_status = 1 AND pro.quantity > 0")->result();

                            $country_id = 0;
                            $country_name = '';
                            $country = $this->db->get_where('users', array('id' => $record->id))->row();
                            $country_code = $this->db->get_where('countries', array('phonecode' => $country->country_code))->row();
                            $country_id = isset($country_code->base_placeId) && $country_code->base_placeId !=null ? $country_code->base_placeId: 165;
                            $country_name = isset($country_code->name) &&  $country_code->name != null ?  $country_code->name : 'Nigeria';
//
//            var_dump($carts);die;
//           
                            if (!empty($carts)) {


                                foreach ($carts as $key => $value) {


                                    $shippingMethods = $country_id == 0 ? [] : $this->getshippingmethods($country_id);

                                    $value->shippingcountry_code = $country_id;
                                    $value->country_name = $country_name;
                                    $value->shippingMethods = $shippingMethods;
//                  $carts[$key] = $value;



                                    $color = $this->getColorByProductId($value->product_id);
                                    $value->colors = $color;
//                    $carts[$key] = $value;

                                    $size = $this->getSizeByProductId($value->product_id);
                                    $value->sizes = $size;
                                    $carts[$key] = $value;
                                }
                            }
                            $Response['result']['status'] = TRUE;
                            $Response['result']['message'] = count($carts) ? 'Product is updated in cart successfully.' : 'Cart is empty.';
                            $Response['result']['cart_total'] = getCartTotal($record->id);
                            $Response['result']['count_carts'] = count($carts) ? count($carts) : "";
                            $Response['result']['carts'] = $carts;
                            return $this->response($Response, REST_Controller::HTTP_OK);
                        } else {
                            $Response['result']['status'] = FALSE;
                            $Response['result']['message'] = 'Something went wrong,Please rise a request to our support team.';
                            return $this->response($Response, REST_Controller::HTTP_OK);
                        }
                    } else {
                        $Response['result']['status'] = FALSE;
                        $Response['result']['message'] = 'Product is out of stock or available less than the quantity you have mentioned.';
                        return $this->response($Response, REST_Controller::HTTP_OK);
                    }
                } else {
                    $Response['result']['status'] = FALSE;
                    $Response['result']['message'] = 'No such product is available.';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
            }
        } else {
            $Response['result']['status'] = FALSE;
            $Response['result']['message'] = 'Authentication failed.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    /*
     * REMOVE FROM CART| START HERE
     */

    public function remove_from_cart_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);

        if (!empty($record) && is_object($record)) {

            $cartId = $this->post('cart_id');
            if (!empty($cartId) && is_numeric($cartId)) {
                $this->db->where('id', $cartId);
                $this->db->delete('carts');
                if ($this->db->affected_rows()) {

                    $Response['result']['status'] = TRUE;
                    $Response['result']['message'] = 'Cart Removed successfully';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
            }
            $Response['result']['status'] = FALSE;
            $Response['result']['message'] = 'Invalid Product';
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    /*
     * VIEW CART ITEMS | START HERE
     */

    public function carts_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);

        if (!empty($record) && is_object($record)) {

            if (isset($record->status) && $record->status == 'guest') {
                
            } else {
                $sessId = $record->id;
            }



            $carts = $this->db->query("SELECT 
                
                                col.attribute_name as selectedcolor,
                                siz.attribute_name as selectedsize,
                                 cart.id as id,
                                cart.shipping_method,
                                cart.shipping_price ,
                               cart.chenname ,
                              cart.channelcode,
                              cart.reftime ,
                               cart.stylename ,
                               cart.styleremark ,
                               
                                wishlist.id as wishlist_id,
                                pro.id as product_id,
                                pro.product_name,
                                pro.price as price,
                                pro.price_after_discount,
                                pro.discount_type, 
                                pro.cover_image,
                             
                                cart.quantity,
                                (
                                   cart.quantity * pro.price_after_discount 
                                ) AS total_price
                            FROM
                                carts AS cart
                            LEFT JOIN products AS pro
                            ON
                                cart.product_id = pro.id
                                left join product_watchlist as wishlist
                                ON cart.product_id = wishlist.product_id
                                AND cart.user_id = wishlist.user_id
                                
                         LEFT JOIN color AS col
                            ON
                                cart.color_id = col.id
                                left join size as siz
                                ON cart.size_id = siz.id
                                
                          WHERE
                                (
                                    cart.user_id = '" . $record->id . "' OR cart.temp_user_id = '" . $sessId . "'
                                ) AND pro.display_status = 1 AND pro.quantity > 0")->result();
            $country_id = 0;
            $country_name = '';
            $country = $this->db->get_where('users', array('id' => $record->id))->row();
            $country_code = $this->db->get_where('countries', array('phonecode' => $country->country_code))->row();
            $country_id = isset($country_code->base_placeId) && $country_code->base_placeId !=null ? $country_code->base_placeId: 165;
            $country_name = isset($country_code->name) &&  $country_code->name != null ?  $country_code->name : 'Nigeria';
//            var_dump($carts);die;
//           
            if (!empty($carts)) {


                foreach ($carts as $key => $value) {


                    $shippingMethods = $country_id == 0 ? [] : $this->getshippingmethods($country_id);

                    $value->shippingcountry_code = $country_id;
                    $value->country_name = $country_name;
                    $value->shippingMethods = $shippingMethods;
//                  $carts[$key] = $value;



                    $color = $this->getColorByProductId($value->product_id);
                    $value->colors = $color;
//                    $carts[$key] = $value;

                    $size = $this->getSizeByProductId($value->product_id);
                    $value->sizes = $size;
                    $carts[$key] = $value;
                }


//              
                $Response['result']['status'] = TRUE;
                $Response['result']['cart_total'] = getCartTotal($sessId);
                $Response['result']['count_carts'] = count($carts) ? count($carts) : "";
                $Response['result']['carts'] = $carts;
//                $Response['result']['colors'] = $allcolorsofproduct;
//                $Response['result']['sizes'] =  $allsizesofproduct;
//                $Response['result']['allcolors'] = $allcolors1;

                return $this->response($Response, REST_Controller::HTTP_OK);
            }

            $Response['result']['status'] = FALSE;
            $Response['result']['message'] = 'Cart is empty';
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    /*
     * Saved Items /Recently Viewed /Recently Searched | START HERE
     * 1=Saved, 2=Recently Viewed, 3=Recently Search Item
     */

    public function saved_history_post() {
        $Authorization = $this->get('auth') == NULL ? $this->input->post('sess_id') : $this->input->get('auth');
        $sessId = $this->input->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);

        $type = $this->input->post('type') != '' ? $this->input->post('type') : 0;
        if (!empty($record) && is_object($record)) {
            $savedItems = $this->db->query("SELECT
                                                fav.id AS wishlist_id,
                                                pro.id as product_id,
                                                pro.product_name,
                                                pro.price,
                                                pro.discount_type,
                                                pro.discount,
                                                pro.cover_image,
                                                pro.price_after_discount
                                            FROM
                                                product_watchlist AS fav
                                            LEFT JOIN products AS pro
                                            ON
                                                fav.product_id = pro.id
                                            WHERE
                                                (
                                                    fav.user_id = '" . $record->id . "' OR fav.temp_user_id = '" . $sessId . "'
                                                ) AND pro.display_status = 1 AND pro.quantity > 0 AND fav.type = " . $type . "
                                            ORDER BY
                                                fav.id
                                            DESC
                                            LIMIT 10")->result();

            if (!empty($savedItems)) {
                $Response['result']['status'] = TRUE;
                $Response['result']['items'] = $savedItems;
            } else {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'did not find any products.';
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    function add_to_wishlist_post() {
        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (!empty($record) && is_object($record)) {
            $product_id = $this->post('product_id');
            if (empty($product_id)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Product id is required.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            } else {

                $AddtowishlistCart = array('product_id' => $product_id,
                    'type' => 1,
                    'added_date' => date('Y-m-d H:i:s'));

                if (isset($record->status) && $record->status == 'guest') {
                    $AddtowishlistCart['temp_user_id'] = $sessId;
                } else {
                    $AddtowishlistCart['user_id'] = $record->id;
                    $sessId = $record->id;
                }
                $checkWishList = $this->Order_Model->countwishlistItem($product_id, $sessId);
                if ($checkWishList > 0) {
                    $Response['result']['status'] = FALSE;
                    $Response['result']['message'] = 'Product is already in wishlist.';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                } else {
                    $this->db->insert('product_watchlist', $AddtowishlistCart);
                    $wishlist_id = $this->db->insert_id();
                    $Response['result']['status'] = TRUE;
                    $Response['result']['wishlist_id'] = $wishlist_id;
                    $Response['result']['message'] = 'Product added to saved items successfully.';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
            }
        }
    }

    function remove_from_wishlist_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);

        if (!empty($record) && is_object($record)) {

            $wishlistid = $this->post('id');
            if (!empty($wishlistid) && is_numeric($wishlistid)) {
                $this->db->where('id', $wishlistid);
                $this->db->delete('product_watchlist');
                if ($this->db->affected_rows()) {

                    $Response['result']['status'] = TRUE;
                    $Response['result']['message'] = 'Product removed from saved items successfully';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
            } else {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Wishlist id is required';
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function getCountriescode_post() {
        try {
            $countries = $this->Product_Model->getCountries();
            if (isset($countries) && $countries != null) {
                $Response['result']['status'] = TRUE;
                $Response['result']['countries'] = $countries;
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
            $Response['result']['status'] = FALSE;
            $Response['result']['message'] = 'countries not found.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } catch (Exception $ex) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response);
        }
    }

    public function getshippingmethods($country_id) {

       try{
        $input = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soap:Body>\n<getFeeByCWV xmlns=\"http://tempuri.org/\">\n<country>$country_id</country>\n<weight>0.1</weight>\n<volume>10</volume>\n<customerid>100589</customerid>\n<secretkey>5417aabe-181b-48c6-9eb0-eada6e3c6323100589</secretkey>\n</getFeeByCWV>\n</soap:Body>\n</soap:Envelope>";


        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://www.pfcexpress.com/webservice/apiwebservice.asmx",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $input,
            CURLOPT_HTTPHEADER => array(
                "Content-Type: text/xml;charset=utf-8"
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
//        echo $response;


        $doc = new DOMDocument('1.0', 'utf-8');
        $doc->loadXML($response);

        return json_decode($doc->textContent, TRUE);
       }
       catch (Exception $ex) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response);
        }
    }

    public function getColorByProductId($product_id) {




        $product = $this->Product_Model->getProductRows($product_id);

        $color = array();
        $color = $this->Product_Model->getColorInProducts($product['color']);


        return $color;
//        }
    }

    public function getSizeByProductId($product_id) {


        $product = $this->Product_Model->getProductRows($product_id);
        $sizes = array();
        $sizes = $this->Product_Model->getSizeInProducts($product['sizes']);
        return $sizes;



//        }
    }

}
