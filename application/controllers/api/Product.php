<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require(APPPATH . '/libraries/Format.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Product extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']);
        $this->load->model('Product_Model');
        $this->load->helper(array('email', 'url'));
    }

    /*
     * HOME API| START HERE
     */

    private function verify_request($token = null) {
        if (isset($token) && !empty($token)) {
            try {
                $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                    $this->response($response, $status);
                    exit();
                } else {
                    $token_db = ValidateJwtToken($token);
                    if ($token_db == false) {
                        $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                        $this->response($response, $status);
                    } else {
                        return $token_db;
                    }
                }
            } catch (Exception $e) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                $this->response($response, $status);
            }
        } else {
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
            $this->response($response, $status);
        }
    }

    public function guest_verify_request($token = null, $sessId = '') {
        try {
            $token = $sessId; // Remove this when you think you have fixed JWT
            $headers = $this->input->request_headers();
            if (isset($token)) {
                $data = AUTHORIZATION::validateToken($token);
                if ($data == false) {
                    $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                    return $this->response($response, parent::HTTP_UNAUTHORIZED);
                } else {
                    $token_db = ValidateJwtToken($token);
                    if ($token_db == false) {
                        $response = ['status' => FALSE, 'message' => 'Failed to validate the token.'];
                        return $this->response($response, parent::HTTP_UNAUTHORIZED);
                    } else {
                        return $token_db;
                    }
                }
            } else {
                if (empty($sessId) || !is_numeric($sessId)) {
                    $response = ['status' => FALSE, 'message' => 'No Token or Session Token received.'];
                    return $this->response($response, parent::HTTP_UNAUTHORIZED);
                }
                $response = ['status' => 'guest', 'id' => $sessId];
                return arrayToObject($response);
            }
        } catch (Exception $e) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response, parent::HTTP_UNAUTHORIZED);
        }
    }

    public function home_post() {
        $dealsItems = array();
        $user_details = array();
        $sliders = $this->Product_Model->getSliderRows();
        $deals = $this->Product_Model->getDealsRows();
        $dailyDealItems = $this->Product_Model->getProductOnDeals(15, 'dailydeal');
        $superDealItems = $this->Product_Model->getProductWithCategoryOnDeals(14);
        $moreProducts = $this->Product_Model->getProductRows();
        $FeaturedCategories = $this->Product_Model->getCategoriesHome();
        $user_id = $this->post('user_id') != null ? $this->post('user_id') : 0;
        if (!empty($deals) && !empty($deals[0])) {
            //$dealsItems = $this->Product_Model->getProductOnDeals($deals[0]->id);		
        }
        if (isset($user_id) && !empty($user_id) && is_numeric($user_id)) {
            $user_details = $this->UserExample->getRows($user_id);


            if ($user_details) {
                $this->db->select('*');
                $this->db->from('carts');
                $this->db->where('user_id', $user_id);
                $carts = $this->db->get()->result();
                $user_details ['carts_total'] = (count($carts) != null) ? count($carts) : null;

                $this->db->select('*');
                $this->db->from('user_address');
                $this->db->where('user_id', $user_id);
                $this->db->where('type', 1);
                $address = $this->db->get()->row();
                if ($address) {
                    $user_details ['address_id'] = ($address != null) ? $address : null;
                } else {
                    $user_details ['address_id'] = null;
                }
            }
        } else {
            $user_details = null;
        }

        $Response['result']['status'] = true;
        $Response['result']['slider'] = $sliders;
        $Response['result']['orinico_deals'] = $deals;
        //$Response['result']['orinico_deals_item'] = $dealsItems;
        $Response['result']['daily_deals_item'] = $dailyDealItems;
        $Response['result']['deals_by_category'] = $superDealItems;
        $Response['result']['featured_categories'] = $FeaturedCategories;
        $Response['result']['more_products_for_you'] = $moreProducts;
        $Response['result']['users_details'] = $user_details;

        $this->response($Response, REST_Controller::HTTP_OK);
    }

    /*
     * CATEGORIES API| START HERE
     */

    public function categories_get() {
        $categories = $this->Product_Model->getCategoriesRows();
        if (!empty($categories)) {
            $Response['result']['status'] = true;
            $Response['result']['categoriesnew'] = $categories;
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
        $Response['status'] = false;
        $Response['    
                    
              message'] = 'categories not found.';
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    /*
     * DEALS API| START HERE
     */

    public function deals_get() {
        $deals = $this->Product_Model->getDealsRows();
        if (!empty($deals)) {
            $Response['result']['status'] = true;
            $Response['result']['deals'] = $deals;
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
        $Response['status'] = false;
        $Response['message'] = 'deals not found.';
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    /*
     * PRODUCT LIST BY CATEGORY PAGE API| START HERE
     */

    public function category_products_get() {
        if (isset($_GET['category_id']) && is_numeric($_GET['category_id'])) {
            $products = $this->db->get_where('products', array('category_id' => $_GET['category_id']))->result();
            if (!empty($products)) {
                $Response['result']['status'] = true;
                $Response['result']['products'] = $products;
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
        $Response['status'] = false;
        $Response['message'] = 'Product not found.';
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function subcategory_products_get() {
        if (isset($_GET['sub_category_id']) && is_numeric($_GET['sub_category_id'])) {
            $products = $this->db->get_where('products', array('sub_category_id' => $_GET['sub_category_id'], 'display_status' => 1))->result();
            if (!empty($products)) {
                $Response['result']['status'] = true;
                $Response['result']['products'] = $products;
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
        $Response['status'] = false;
        $Response['message'] = 'Product not found.';
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    /*
     * SINGLE PRODUCT DETAIL BY PRODUCT ID API| START HERE
     */

    public function product_detail_post() {
        if (isset($_POST['product_id']) && !empty($_POST['product_id']) && is_numeric($_POST['product_id'])) {
            $product = $this->Product_Model->getProductRows($_POST['product_id']);
            $deals_info = NULL;


            if (!empty($product)) {
                $sizes = array();
                $colors = array();
                $relatedProducts = array();

                $ratings = $this->Product_Model->getProductRatings($_POST['product_id']);
                $sliders = $this->Product_Model->getProductSlider($_POST['product_id']);

                if (!empty($product['sizes'])) {
                    $sizes = $this->Product_Model->getSizeInProducts($product['sizes']);
                }
                if (!empty($product['color'])) {
                    $colors = $this->Product_Model->getColorInProducts($product['color']);
                }
                $user_id = $this->post('user_id') != NULL ? $this->post('user_id') : '';

                if (isset($user_id) && !empty($user_id) && is_numeric($user_id)) {
                    $this->db->select('*');
                    $this->db->from('product_watchlist');
                    $this->db->where('user_id', $user_id);
                    $this->db->where('product_id', $_POST['product_id']);
                    $isInwishlist = $this->db->get()->row();
                    if (is_object($isInwishlist)) {
                        $wishlist_id = $isInwishlist->id;
                    }
                }

                if (!empty($product['subsub_category_id'])) {
                    $CatId = $product['subsub_category_id'];
                } else if (!empty($product['sub_category_id'])) {
                    $CatId = $product['sub_category_id'];
                } else if (!empty($product['category_id'])) {
                    $CatId = $product['category_id'];
                }

                if (!empty($CatId)) {
                    $relatedProducts = $this->Product_Model->getProductByCategory($CatId, 'related', $_POST['product_id']);
                }

                $Inputs = [
                    'user_id' => isset($user_id) ? $user_id : NULL,
                    'product_id' => isset($_POST['product_id']) ? $_POST['product_id'] : NULL
                ];
                $this->db->insert('recently_viewed', $Inputs);


                $this->db->select('*');
                $this->db->from('deals');
                $this->db->where('id', isset($_POST['deal_id']) ? $_POST['deal_id'] : '');
                $this->db->where('isdeal_time', 1);
                $Isindeal_time = $this->db->get()->row();
                if ($Isindeal_time != null) {
                    $deals_info = $Isindeal_time;
                    $deals_info->total_stock = isset($product['quantity']) ? $product['quantity'] : 0;
                } else {
                    $this->db->select('*');
                    $this->db->from('product_deals');
                    $this->db->where('product_id', $_POST['product_id']);
                    $this->db->where('deal_id', isset($_POST['deal_id']) ? $_POST['deal_id'] : '');
                    $Isindeal = $this->db->get()->row();
                    $dealdata = $this->db->get_where('deals', array('id' => isset($_POST['deal_id']) ? $_POST['deal_id'] : ''))->row();




                    if ($Isindeal != null) {

                        $deals_info = $Isindeal;
                        $deals_info->isdeal_time = $dealdata->isdeal_time;
                        $deals_info->maximum = $dealdata->maximum;
                        $deals_info->minimum = $dealdata->minimum;
                        $deals_info->total_stock = isset($product['quantity']) ? $product['quantity'] : 0;
                        $deals_info->color = $dealdata->color;
                    }
                }

                $Response['result']['status'] = true;
                $Response['result']['wishlist_id'] = isset($wishlist_id) ? $wishlist_id : null;
                $Response['result']['product'] = $product;
                $Response['result']['product']['return_policy'] = 'Free return With in 15 days for orinoco items and 7 days for other eligible items.';
                $Response['result']['product']['avg_rating'] = 3;
                $Response['result']['product']['pro_sizes'] = $sizes;

                $Response['result']['product']['pro_colors'] = $colors;
                $Response['result']['product']['pro_ratings'] = $ratings;
                $Response['result']['product']['pro_related'] = $relatedProducts;
                $Response['result']['product']['pro_sliders'] = $sliders;
                $Response['result']['product']['deal_info'] = $deals_info;


                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }

        $Response['status'] = false;
        $Response['message'] = 'Product not found.';
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    /*
     * DEALS PRODUCT PAGE BY DEAL ID API| START HERE
     */

    public function product_deals_get() {
        if (isset($_GET['deal_id']) && !empty($_GET['deal_id']) && is_numeric($_GET['deal_id'])) {
            $dealCategoeies = $this->Product_Model->getCategoeiesByDealId($_GET['deal_id']);
            if (!empty($dealCategoeies)) {
                $catId = $dealCategoeies[0]['category_id'];
                if (isset($_GET['category_id']) && !empty($_GET['category_id']) && is_numeric($_GET['category_id'])) {
                    $catId = $_GET['category_id'];
                }

                $dealProducts = $this->Product_Model->getProductsByCategoryAndDealId($_GET['deal_id'], $catId);
                $Response['result']['status'] = true;
                $Response['result']['banner'] = 'uploads/website-imgs/download (1).jpg';
                $Response['result']['categoeies'] = $dealCategoeies;
                $Response['result']['products'] = $dealProducts;
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }

        $Response['status'] = false;
        $Response['message'] = 'deal not found.';
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function allproducts_deals_post() {
        if (isset($_POST['deal_id']) && !empty($_POST['deal_id']) && is_numeric($_POST['deal_id'])) {
            $dealDetails = $this->Product_Model->getallDealsRows($_POST['deal_id']);
//            var_dump($dealDetails->start_time);die;
            $dealsubCategories = $this->Product_Model->getsubCategoriesByDealId($_POST['deal_id']);





            if ($dealsubCategories) {
                $Response['result']['status'] = TRUE;
                $Response['result']['subcategories'] = $dealsubCategories;
                $Response['result']['deal_start_date'] = isset($dealDetails->start_date) ? $dealDetails->start_date : NULL;
                $Response['result']['deal_start_time'] = isset($dealDetails->start_time) ? $dealDetails->start_time : NULL;
                $Response['result']['deal_end_date'] = isset($dealDetails->end_date) ? $dealDetails->end_date : NULL;
                $Response['result']['deal_end_time'] = isset($dealDetails->end_time) ? $dealDetails->end_time : NULL;
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
        $Response['result']['status'] = false;
        $Response['result']['message'] = 'deal not found.';
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function allproducts_deals_old_post() {
        if (isset($_POST['deal_id']) && !empty($_POST['deal_id']) && is_numeric($_POST['deal_id'])) {
            $dealDetails = $this->Product_Model->getallDealsRows($_POST['deal_id']);
            $dealsubCategories = $this->Product_Model->getsubCategoriesByDealId($_POST['deal_id']);

            if (isset($dealDetails->has_everything_50_percent_off) && $dealDetails->has_everything_50_percent_off == 1) {
                $Result = [];
                if (count($dealsubCategories) > 0) {
                    $Result[0] = $dealsubCategories[0];
                    unset($dealsubCategories[0]);
                }
                $Result[] = [
                    "subcategory_id" => 0,
                    "name" => "Everything 50% Off"
                ];
                foreach ($dealsubCategories as $value) {
                    $Result[] = $value;
                }
                $Response['result']['status'] = TRUE;
                $Response['result']['subcategories'] = $Result;
            } else {
                $Response['result']['status'] = TRUE;
                $Response['result']['subcategories'] = $dealsubCategories;
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
        $Response['result']['status'] = false;
        $Response['result']['message'] = 'deal not found.';
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function allproductsbysubcat_deals_post() {

        $this->form_validation->set_rules('deal_id', 'deal_id', 'required');
        $this->form_validation->set_rules('subcat_id', 'subcat_id', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == TRUE) {

            if (isset($_POST['deal_id']) && !empty($_POST['deal_id']) && is_numeric($_POST['deal_id'])) {
                $dealProducts = $this->Product_Model->getProductsBysubCategoryAndDealId($_POST['deal_id'], $_POST['subcat_id']);

                if ($dealProducts != NULL) {
                    foreach ($dealProducts as $products) {
                        $dealProducts[0]['minimum'] = $products['quantity'];
                        $dealProducts[0]['maximum'] = $products['quantity'];
                        $dealProducts[0]['deal_sold'] = $products['item_sold'];
                    }
                }

                $Response['result']['status'] = TRUE;
                $Response['result']['product_deals'] = $dealProducts;
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        } else {
            $Response['result']['message'] = validation_errors();
            $Response['result']['code'] = '202';
            $Response['result']['status'] = FALSE;
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    public function productSearch_post() {
        $this->form_validation->set_rules('search_text', 'Search Text', 'required');
        $this->form_validation->set_error_delimiters('', '');

        $temp_categories = [];
        $temp_deal_ids = [];
        $categories = [];
        $deals = [];
        if ($this->form_validation->run() == TRUE) {
            $post = $this->input->post();
            $user_id = isset($post['user_id']) ? $post['user_id'] : '';

            $productIsincategory = $this->Product_Model->getProductBySearchByCategory($post['search_text']);


            if (is_object($productIsincategory)) {

                if ($productIsincategory->type == 0) {

                    $productData = $this->Product_Model->getProductsByCategory($productIsincategory->id);
                } else if ($productIsincategory->type == 1) {

                    $productData = $this->Product_Model->getProductsBySearchBySubCategory($productIsincategory->id);
                } else {
                    $productData = $this->Product_Model->getProductsBySearchBySubSubCategory($productIsincategory->id);
                }
            } else {
                $productData = $this->Product_Model->getProductBySearch($post['search_text']);
            }





            if ($productData != []) {


                foreach ($productData as $key => $value) {
                    $temp_categories[] = $value['category_id'];
                    $temp_categories[] = $value['sub_category_id'];



                    $deal_ids = $this->db->get_where('product_deals', ['product_id' => $value['id']])->result();
                    foreach ($deal_ids as $id) {
                        $temp_deal_ids[] = $id->deal_id;
                    }


                    $ProductImages = $this->Product_Model->getProductImages($value['id']);
                    $ProductImg = [];

                    foreach ($ProductImages as $ikey => $ivalue) {
                        $ProductImg[] = $ivalue->image;
                    }
                    $productData[$key]['productImages'] = $ProductImg;
                }
                foreach ($productData as $data) {
                    $id = $data['id'];
                }
                $temp_categories = array_unique($temp_categories);
                $categories_count = $this->db->get_where('products', array('category_id' => $value['category_id'], 'product_name' => $post['search_text']))->result();
                $categoriesproductcount = $categories_count!=null?count($categories_count):0;

                foreach ($temp_categories as $category_id) {


                    $categories[] = $this->db->get_where('category', array('id' => $category_id))->row();
//                    var_dump($categories);die;
                    foreach ($categories as $ca) {
                        $ca->productcountresults = $categoriesproductcount;
                    }
                }
                $temp_deal_ids = array_unique($temp_deal_ids);
              


                foreach ($temp_deal_ids as $ID) {
               $deals_count = $this->db->get_where('products', array('deal_ids' => $value['deal_ids'], 'product_name' => $post['search_text']))->result();
               $dealsproductcount = $deals_count!=null? count($deals_count):0;
                    $deals[] = $this->db->get_where('deals', ['id' => $ID])->row();
                    foreach ($deals as $de) {
                        $de->productcountresults = $dealsproductcount;
                    }
                    
                }

                $Inputs = [
                    'user_id' => isset($user_id) ? $user_id : NULL,
                    'product_id' => isset($id) ? $id : NULL
                ];

                $this->db->insert('recently_searched', $Inputs);
                $Response['result']['message'] = 'Product list fetch successfully.';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['products'] = $productData;
                $Response['result']['category'] = $categories;
                $Response['result']['deals'] = $deals;
            } else {
                $Response['result']['message'] = 'No product found.';
                $Response['result']['code'] = '202';
                $Response['result']['status'] = FALSE;
                $Response['result']['result'] = $categories;
                $Response['result']['deals'] = $deals;
            }
        } else {
            $Response['result']['message'] = validation_errors();
            $Response['result']['code'] = '202';
            $Response['result']['status'] = FALSE;
        }
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function price_lowtohigh_post() {


        $this->form_validation->set_rules('sub_category_id', 'sub_category_id', 'required');
        $this->form_validation->set_error_delimiters('', '');


        if ($this->form_validation->run() == TRUE) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where("display_status", 1);
            $this->db->where("sub_category_id", $this->input->post('sub_category_id'));
            $this->db->order_by("price", "ASC");


            $productData = $this->db->get()->result();
            if ($productData) {

                $Response['result']['message'] = 'Product list fetch successfully.';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['products'] = $productData;
            } else {
                $Response['result']['message'] = 'No product found.';
                $Response['result']['code'] = '202';
                $Response['result']['status'] = FALSE;
                $Response['result']['result'] = [];
            }
        } else {
            $Response['result']['message'] = validation_errors();
            $Response['result']['code'] = '202';
            $Response['result']['status'] = FALSE;
        }
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function price_hightolow_post() {

        $this->form_validation->set_rules('sub_category_id', 'sub_category_id', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == TRUE) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where("display_status", 1);
            $this->db->where("sub_category_id", $this->input->post('sub_category_id'));
            $this->db->order_by("price", "DESC");


            $productData = $this->db->get()->result();
            if ($productData) {

                $Response['result']['message'] = 'Product list fetch successfully.';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['products'] = $productData;
            } else {
                $Response['result']['message'] = 'No product found.';
                $Response['result']['code'] = '202';
                $Response['result']['status'] = FALSE;
                $Response['result']['result'] = [];
            }
        } else {
            $Response['result']['message'] = validation_errors();
            $Response['result']['code'] = '202';
            $Response['result']['status'] = FALSE;
        }

        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function price_range_post() {

        $this->form_validation->set_rules('min_price', 'min_price', 'required');
        $this->form_validation->set_rules('max_price', 'max_price', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == TRUE) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where('price >=', $this->input->post('min_price'));
            $this->db->where('price  <=', $this->input->post('max_price'));
            $products = $this->db->get()->result();

            if ($products) {
                $Response['result']['message'] = 'Product list fetch successfully.';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['products'] = $products;
            } else {
                $Response['result']['message'] = 'No product found.';
                $Response['result']['code'] = '202';
                $Response['result']['status'] = FALSE;
                $Response['result']['result'] = [];
            }
        } else {
            $Response['result']['message'] = validation_errors();
            $Response['result']['code'] = '202';
            $Response['result']['status'] = FALSE;
        }
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function recently_viewed_post() {

        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == TRUE) {

            $user_id = $this->post('user_id') != null ? $this->post('user_id') : 0;

            $this->db->select('*');
            $this->db->from('recently_viewed');
            $this->db->join('products', 'products.id = recently_viewed.product_id');
            $this->db->where('user_id', $user_id);
            $this->db->order_by('recently_viewed.id', 'DESC');
            $this->db->limit(15);
            $recentviewed = $this->db->get()->result();

            if ($recentviewed != NULL) {

                $Response['result']['message'] = 'recent viewed products are fetch successfully.';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['products'] = $recentviewed;
            } else {
                $Response['result']['message'] = 'No more Products are viewed';
                $Response['result']['code'] = '203';
                $Response['result']['status'] = FALSE;
            }
        } else {
            $Response['message'] = validation_errors();
            $Response['status'] = FALSE;
            $Response['code'] = '202';
        }
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    function remove_from_recentlyviewed_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);

        if (!empty($record) && is_object($record)) {

            $recent_id = $this->post('recent_id');
            if (!empty($recent_id) && is_numeric($recent_id)) {
                $this->db->where('id', $recent_id);
                $this->db->delete('recently_viewed');
                if ($this->db->affected_rows()) {

                    $Response['result']['status'] = TRUE;
                    $Response['result']['message'] = 'Product removed from recent items successfully';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
            } else {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'recent_id is required';
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function recently_searched_post() {

        $this->form_validation->set_rules('user_id', 'user_id', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == TRUE) {

            $temp_categories = [];
            $user_id = $this->post('user_id') != null ? $this->post('user_id') : 0;
            $this->db->select('*');
            $this->db->from('recently_searched');
            $this->db->join('products', 'products.id = recently_searched.product_id');
            $this->db->where('user_id', $user_id);
            $this->db->order_by('recently_searched.id', 'DESC');
            $this->db->limit(15);
            $recentsearched = $this->db->get()->result();



            if ($recentsearched != NULL) {

                foreach ($recentsearched as $value) {

                    $temp_categories = $value->category_id;

                    $categories = $this->db->get_where('category', array('id' => $temp_categories))->row();
                    $value->category = $categories;
                }


                $Response['result']['message'] = 'recent searched products are fetch successfully.';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['products'] = $recentsearched;
            } else {
                $Response['result']['message'] = 'No more Products are viewed';
                $Response['result']['code'] = '203';
                $Response['result']['status'] = FALSE;
            }
        } else {
            $Response['message'] = validation_errors();
            $Response['status'] = FALSE;
            $Response['code'] = '202';
        }
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function getSize_post() {
        try {
            $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
            $sessId = $this->post('sess_id');
            $record = $this->guest_verify_request($Authorization, $sessId);
            if (is_object($record)) {
                $sizes = $this->Product_Model->getSize();
                if (isset($sizes) && $sizes != null) {
                    $Response['result']['status'] = TRUE;
                    $Response['result']['sizes'] = $sizes;
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'sizes not found.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        } catch (Exception $ex) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response);
        }
    }

    public function getColor_post() {
        try {
            $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
            $sessId = $this->post('sess_id');
            $record = $this->guest_verify_request($Authorization, $sessId);
            if (is_object($record)) {
                $colors = $this->Product_Model->getColor();
                if (isset($colors) && $colors != null) {
                    $Response['result']['status'] = TRUE;
                    $Response['result']['colors'] = $colors;
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'colors not found.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        } catch (Exception $ex) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response);
        }
    }

    public function getColorByProductId_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (is_object($record)) {

            $product_id = $this->post('product_id');


            if (empty($product_id)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'product_id is required.';
            } else {
                $product = $this->Product_Model->getProductRows($_POST['product_id']);
                $colors = array();
                $colors = $this->Product_Model->getColorInProducts($product['color']);

                if ($colors != NULL) {

                    $Response['result']['message'] = 'Colors are fetch successfully.';
                    $Response['result']['code'] = '200';
                    $Response['result']['status'] = TRUE;
                    $Response['result']['colors'] = $colors;
                } else {
                    $Response['result']['message'] = 'No Colors are Found';
                    $Response['result']['code'] = '203';
                    $Response['result']['status'] = FALSE;
                }
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    public function getSizeByProductId_post($product_id) {

//        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
//        $sessId = $this->post('sess_id');
//        $record = $this->guest_verify_request($Authorization, $sessId);
//        if (is_object($record)) {

        $product_id = $this->post('product_id');


        if (empty($product_id)) {
            $Response['result']['status'] = FALSE;
            $Response['result']['message'] = 'product_id is required.';
        } else {
            $product = $this->Product_Model->getProductRows($_POST['product_id']);
            $sizes = array();
            $sizes = $this->Product_Model->getSizeInProducts($product['sizes']);

            if ($sizes != NULL) {

                $Response['result']['message'] = 'Sizes are fetch successfully.';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['sizes'] = $sizes;
            } else {
                $Response['result']['message'] = 'No Sizes are Found';
                $Response['result']['code'] = '203';
                $Response['result']['status'] = FALSE;
            }
        }
        return $this->response($Response, REST_Controller::HTTP_OK);
//        }
    }

    public function updateSizeByProductId_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (is_object($record)) {

            $size_id = $this->post('size_id');
            $cart_id = $this->post('cart_id');
            $Inputs = [
                'size_id' => isset($size_id) ? $size_id : 0
            ];


            if (empty($size_id)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = ' size_id is required.';
            } else if (empty($cart_id)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'cart_id is required.';
            } else {
                $sizes = $this->db->update('carts', $Inputs, ['id' => $cart_id]);

                if ($sizes != NULL) {

                    $Response['result']['message'] = 'Sizes updated successfully.';
                    $Response['result']['code'] = '200';
                    $Response['result']['status'] = TRUE;
                    $Response['result']['sizes'] = $sizes;
                } else {
                    $Response['result']['message'] = 'No Sizes are Found';
                    $Response['result']['code'] = '203';
                    $Response['result']['status'] = FALSE;
                }
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    public function updateColorByProductId_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (is_object($record)) {

            $color_id = $this->post('color_id');
            $cart_id = $this->post('cart_id');
            $Inputs = [
                'color_id' => isset($color_id) ? $color_id : 0
            ];


            if (empty($color_id)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = ' color_id is required.';
            } else if (empty($cart_id)) {
                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'cart_id is required.';
            } else {
                $colors = $this->db->update('carts', $Inputs, ['id' => $cart_id]);

                if ($colors != NULL) {

                    $Response['result']['message'] = 'Color updated successfully.';
                    $Response['result']['code'] = '200';
                    $Response['result']['status'] = TRUE;
                    $Response['result']['sizes'] = $colors;
                } else {
                    $Response['result']['message'] = 'No Color are Found';
                    $Response['result']['code'] = '203';
                    $Response['result']['status'] = FALSE;
                }
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    public function bestMatching_post() {

        $this->form_validation->set_rules('sub_category_id', 'sub_category_id', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == TRUE) {
            $this->db->select('*');
            $this->db->from('products');
            $this->db->where("display_status", 1);
            $this->db->where("sub_category_id", $this->input->post('sub_category_id'));
            $this->db->order_by("id", "DESC");
            $this->db->limit(100);


            $productData = $this->db->get()->result();
            if ($productData) {

                $Response['result']['message'] = 'Best Mactch products successfully.';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['products'] = $productData;
            } else {
                $Response['result']['message'] = 'No product found.';
                $Response['result']['code'] = '202';
                $Response['result']['status'] = FALSE;
                $Response['result']['result'] = [];
            }
        } else {
            $Response['result']['message'] = validation_errors();
            $Response['result']['code'] = '202';
            $Response['result']['status'] = FALSE;
        }

        return $this->response($Response, REST_Controller::HTTP_OK);
    }

}
