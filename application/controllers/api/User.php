<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require(APPPATH . '/libraries/Format.php');

require 'vendor/autoload.php';

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class User extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']);
        $this->load->model('User_Model');
        $this->load->helper(array('email', 'url'));
    }

    private function verify_request($token = null) {
        if (isset($token) && !empty($token)) {
            try {
                $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                    $this->response($response, $status);
                    exit();
                } else {
                    $token_db = ValidateJwtToken($token);
                    if ($token_db == false) {
                        $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                        $this->response($response, $status);
                    } else {
                        return $token_db;
                    }
                }
            } catch (Exception $e) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                $this->response($response, $status);
            }
        } else {
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
            $this->response($response, $status);
        }
    }

    public function guest_verify_request($token = null, $sessId = '') {
        try {
            $token = $sessId; // Remove this when you think you have fixed JWT
            $headers = $this->input->request_headers();
            if (isset($token)) {
                $data = AUTHORIZATION::validateToken($token);
                if ($data == false) {
                    $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                    return $this->response($response, parent::HTTP_UNAUTHORIZED);
                } else {
                    $token_db = ValidateJwtToken($token);
                    if ($token_db == false) {
                        $response = ['status' => FALSE, 'message' => 'Failed to validate the token.'];
                        return $this->response($response, parent::HTTP_UNAUTHORIZED);
                    } else {
                        return $token_db;
                    }
                }
            } else {
                if (empty($sessId) || !is_numeric($sessId)) {
                    $response = ['status' => FALSE, 'message' => 'No Token or Session Token received.'];
                    return $this->response($response, parent::HTTP_UNAUTHORIZED);
                }
                $response = ['status' => 'guest', 'id' => $sessId];
                return arrayToObject($response);
            }
        } catch (Exception $e) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response, parent::HTTP_UNAUTHORIZED);
        }
    }

    public function add_address_post() {
        try {
            $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
            $sessId = $this->post('sess_id');
            $record = $this->guest_verify_request($Authorization, $sessId);

            if (!empty($record) && is_object($record)) {
                $this->form_validation->set_rules('contact_name', 'Contact Name', 'required');
                $this->form_validation->set_rules('countrycode_id', 'Country code', 'required');
                $this->form_validation->set_rules('contact_no', 'Mobile Number', 'required');
                $this->form_validation->set_rules('address', 'Address', 'required');
                $this->form_validation->set_rules('country_name', 'country_name', 'required');
                $this->form_validation->set_rules('state', 'State', 'required');
                $this->form_validation->set_rules('city', 'City', 'required');
                $this->form_validation->set_rules('zipcode', 'Zip/Postcode', 'required');
                $this->form_validation->set_rules('type', 'type', 'required');
                $this->form_validation->set_error_delimiters('', '');
                if ($this->form_validation->run() == TRUE) {
                    $post = $this->input->post();


                    $this->db->from('user_address');
                    $this->db->where('user_id', $record->id);
                    $this->db->or_where('temp_user_id', $sessId);
                    $existingAddressResult = $this->db->get()->result();


                    if ($existingAddressResult) {
                        $Inputs = [
                            'type' => 2
                        ];

                        foreach ($existingAddressResult as $value) {

                            $aid = $value->id;
                            $this->User_Model->update_record_by_condition('user_address', $Inputs, array('user_id' => $record->id, 'id' => $aid));
                        }
                    }


                    $tbl = 'user_address';
                    $data = array(
                        'user_id' => $record->id,
                        'temp_user_id' => isset($sessId) ? $sessId : 'null',
                        'contact_name' => $post['contact_name'],
                        'countrycode_id' => $post['countrycode_id'],
                        'contact_no' => $post['contact_no'],
                        'address' => $post['address'],
                        'country_name' => $post['country_name'],
                        'state' => $post['state'],
                        'city' => $post['city'],
                        'zipcode' => $post['zipcode'],
                        'type' => (int) $post['type'],
                        'status' => 1,
                        'created_at' => date('Y-m-d H:i:s')
                    );






                    $insert_id = $this->User_Model->insert_one_row($tbl, $data);


                    if (!empty($insert_id)) {
                        $id = $this->db->insert_id();
                        $users_address = $this->db->get_where('user_address', array('id' => $id, 'user_id' => $record->id))->row();
                        $Response['result']['message'] = 'Address added successfully.';
                        $Response['result']['users_address'] = $users_address != null ? $users_address : null;
                        $Response['result']['code'] = '200';
                        $Response['result']['status'] = TRUE;
                    } else {
                        $Response['result']['message'] = 'something went wrong';
                        $Response['result']['code'] = '203';
                        $Response['result']['status'] = FALSE;
                    }
                } else {
                    $Response['result']['message'] = validation_errors();
                    $Response['result']['code'] = '202';
                    $Response['result']['status'] = FALSE;
                }
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        } catch (Exception $ex) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response, parent::HTTP_UNAUTHORIZED);
        }
    }

    public function edit_address_post() {
        try {
            $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
            $sessId = $this->post('sess_id');
            $record = $this->guest_verify_request($Authorization, $sessId);


            if (!empty($record) && is_object($record)) {
                $post = $this->input->post();
                if ($this->post('id') == null) {
                    $Response['result']['status'] = FALSE;
                    $Response['result']['message'] = 'Address id is required.';
                } else {

                    $this->db->from('user_address');
                    $this->db->where('user_id', $record->id);
                    $this->db->or_where('temp_user_id', $sessId);
                    $existingAddressResult = $this->db->get()->result();


                    if ($existingAddressResult) {
                        $Inputs = [
                            'type' => 2
                        ];

                        foreach ($existingAddressResult as $value) {

                            $aid = $value->id;
                            $this->User_Model->update_record_by_condition('user_address', $Inputs, array('user_id' => $record->id, 'id' => $aid));
                        }
                    }

                    $tbl = 'user_address';
                    $post = $this->input->post();
                    $check = $this->User_Model->find($tbl, '*', array('id' => $post['id']));
                    if (!empty($check)) {
                        $data = array(
                            'contact_name' => isset($post['contact_name']) ? $post['contact_name'] : "",
                            'countrycode_id' => isset($post['countrycode_id']) ? $post['countrycode_id'] : "",
                            'contact_no' => isset($post['contact_no']) ? $post['contact_no'] : "",
                            'address' => isset($post['address']) ? $post['address'] : "",
                            'country_name' => isset($post['country_name']) ? $post['country_name'] : "",
                            'state' => isset($post['state']) ? $post['state'] : "",
                            'city' => isset($post['city']) ? $post['city'] : "",
                            'type' => isset($post['type']) ? (int) $post['type'] : "",
                            'zipcode' => isset($post['zipcode']) ? $post['zipcode'] : "",
                            'status' => 1,
                            'updated_at' => date('Y-m-d H:i:s')
                        );

                        $this->db->from('user_address');
                        $this->db->where('user_id', $record->id);
                        $this->db->or_where('temp_user_id', $sessId);
                        $existingAddressResult = $this->db->get()->result();
//                        $Inputs = [
//                            'type' => 2
//                        ];
//
//                        if ($existingAddressResult) {
//
//                            foreach ($existingAddressResult as $value) {
//
//                                $aid = $value->id;
//                                $this->db->update('user_address', $Inputs, ['user_id' => $record->id]);
//                            }
//                        }
                        $this->User_Model->update_record_by_condition($tbl, $data, array('id' => $check['id']));
                        $id = $this->db->insert_id();
                        $users_address = $this->db->get_where('user_address', array('id' => $post['id']))->row();
//                        
                        $Response['result']['message'] = 'Address updated successfully';
                        $Response['result']['users_address'] = $users_address != null ? $users_address : null;
                        ;
                        $Response['result']['code'] = '200';
                        $Response['result']['status'] = TRUE;
                    } else {
                        $Response['result']['message'] = 'Address not found';
                        $Response['result']['code'] = '203';
                        $Response['result']['status'] = FALSE;
                    }
                }
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        } catch (Exception $ex) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response, parent::HTTP_UNAUTHORIZED);
        }
    }

    public function addresslist_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);

        if (!empty($record) && is_object($record)) {

            if (isset($record->status) && $record->status == 'guest') {
                
            } else {
                $sessId = $record->id;
            }
            $this->db->select('*');
//           $this->db->select('user_address.id as address_id,user_address.contact_name,user_address.contact_no,user_address.address,user_address.state,user_address.city,user_address.type,user_address.zipcode,countries.name as country,countries.phonecode');
            $this->db->from('user_address');
//            $this->db->join('countries', 'user_address.countrycode_id = countries.id', 'right outer');
            $this->db->where('user_id', $record->id);
            $this->db->or_where('temp_user_id', $sessId);
            $usersAddresslist = $this->db->get()->result();

            if (!empty($usersAddresslist)) {
                $Response['result']['status'] = TRUE;
                $Response['result']['users_address'] = $usersAddresslist;
            } else {

                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Address doesnot exist';
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    public function addresslistByAddress_id_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        $address_id = $this->post('id');


        if (!empty($record) && is_object($record)) {

            if (isset($record->status) && $record->status == 'guest') {
                
            } else {
                $sessId = $record->id;
            }
            if (empty($address_id)) {

                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Address id is required';
            }

            $this->db->select('*');

//            $this->db->select('user_address.id as address_id,user_address.contact_name,user_address.contact_no,user_address.address,user_address.state,user_address.city,user_address.zipcode,user_address.type');
            $this->db->from('user_address');
//            $this->db->join('countries', 'user_address.countrycode_id = countries.id', 'right outer');
            $this->db->where('id', $address_id);
            $this->db->where('user_id', $record->id);
            $this->db->or_where('temp_user_id', $sessId);
            $usersAddresslist = $this->db->get()->row();

            if (is_object($usersAddresslist)) {
                $Response['result']['status'] = TRUE;
                $Response['result']['users_address'] = $usersAddresslist;
            } else {

                $Response['result']['status'] = FALSE;
                $Response['result']['message'] = 'Address doesnot exist';
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    public function forgot_password_post() {
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_error_delimiters('', '');
        if ($this->form_validation->run() == TRUE) {
            $postData = $this->input->post();
            $validateUser = $this->User_Model->find('users', '*', array('email' => $postData['email']));

            if (!empty($validateUser)) {
                $Token = md5($validateUser['id']);

                if ($Token != null) {
                    $thisUpdate = $this->User_Model->update_record_by_condition('users', array('reset_password_token' => $Token), array('email' => $postData['email']));
                }
                /*
                 * Save this token into DB users table as reset_password_token
                 */


                $link = "https://webmobril.org/dev/orinocoglobal/changePassword/doChange/" . $Token;
                $title = "Click here the link to reset your password";
                $from = "orinicoglobal@orinico.com";
//                $to =  $postData['email'];
                $to = $postData['email'];
                $headers = "MIME-Version$from: 1.0" . "\r\n";
                $headers .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
                $headers .= 'From: ' . $from . "\r\n";
                $headers .= 'Reply-To: ' . $from . "\r\n";
                $headers .= 'X-Mailer: PHP/' . phpversion();
                $headers .= "X-Priority: 1 (Highest)\n";
                $headers .= "X-MSMail-Priority: High\n";
                $headers .= "Importance: High\n";
                $message = '<p>Hi ' . $validateUser['first_name'] . ' ' . $validateUser['last_name'] . ',</p>
                            <p><br></p>
                            <p>Click on the below link to reset your password.</p>
                            <p> Please do not share this link with anyone for security reasons.&nbsp;</p>
                            <p>For any assistance contact our technical support.&nbsp;</p>
                            <p><b><i><a href="' . $link . '" target="_blank">RESET PASSWORD</a></i></b></p>
                            <p><b><i>Thank You</i></b></p>
                            <p><b><i>OrinicoGlobal</i></b></p>';
                $subject = "Orinico Global: Reset password link";

                mail($to, $subject, $message, $headers);
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['message'] = "A link has been sent in your email id for reset password";
                $Response['result']['profile'] = $validateUser;
            } else {
                $Response['result']['message'] = 'Email does not exist';
                $Response['result']['status'] = FALSE;
                $Response['result']['code'] = '203';
            }
        } else {
            $Response['result']['message'] = validation_errors();
            $Response['result']['code'] = '202';
            $Response['result']['status'] = FALSE;
        }
        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function resetPassword_post() {
        $this->form_validation->set_rules('user_id', 'user id', 'required');
        $this->form_validation->set_rules('new_password', 'new password', 'required');
        $this->form_validation->set_rules('confirm_password', 'confirm password', 'required|matches[new_password]');
        if ($this->form_validation->run() == TRUE) {
            $postData = $this->input->post();
            $tbl = 'users';
            if ($postData['new_password'] == $postData['confirm_password']) {
                $data = array('password' => $this->bcrypt->hash_password($postData['new_password']));
                $updated_id = $this->Base_model->update_record_by_condition($tbl, $data, array('id' => $postData['user_id']));
                $getProfile = $this->Base_model->get_record_by_id($tbl, array('id' => $postData['user_id']));
                if ($updated_id > 0) {
                    $response['message'] = 'password reset successfully';
                    $response['code'] = '200';
                    $response['status'] = TRUE;
                    $response['profile'] = $getProfile;
                } else {
                    $response['message'] = 'nothing to change.';
                    $response['code'] = '204';
                    $response['status'] = FALSE;
                    $response['profile'] = $getProfile;
                }
            } else {
                $response['message'] = 'new password and confirm password should be same';
                $response['status'] = FALSE;
                $response['code'] = '204';
            }
        } else {
            $response['message'] = validation_errors();
            $response['code'] = '202';
            $response['status'] = FALSE;
        }
        echo json_encode($response);
        die();
    }

    public function user_rating_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (is_object($record)) {

            $this->form_validation->set_rules('rating', 'rating', 'required');
            $this->form_validation->set_rules('comments', 'comments');
            $this->form_validation->set_rules('order_id', 'order_id', 'required');
            $this->form_validation->set_rules('product_id', 'product_id', 'required');
            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() == TRUE) {
                $post = $this->input->post();
                $tbl = 'product_ratings';
                $data = array(
                    'user_id' => $record->id,
                    'temp_user_id' => isset($sessId) ? $sessId : null,
                    'rating' => $post['rating'],
                    'order_id' => $post['order_id'],
                    'product_id' => $post['product_id'],
                    'comments' => ($post['comments']) ? $post['comments'] : '',
                    'added_date' => date('Y-m-d H:i:s')
                );
                $insert_id = $this->User_Model->insert_one_row($tbl, $data);


                if (!empty($insert_id)) {

                    $id = $this->db->insert_id();
                    $rating = $this->db->get_where('product_ratings', array('id' => $id))->row();
                    $Response['result']['message'] = 'rating given successfully.';
                    $Response['result']['code'] = '200';
                    $Response['result']['status'] = TRUE;
                    $Response['result']['ratings'] = $rating;
                } else {
                    $Response['result']['message'] = 'something went wrong';
                    $Response['result']['code'] = '203';
                    $Response['result']['status'] = FALSE;
                }
            } else {
                $Response['message'] = validation_errors();
                $Response['status'] = FALSE;
                $Response['code'] = '202';
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    public function user_order_post() {
        $Response = [];
        $Insertable = [];
        $Inputs = [];
        $dataInfo = json_decode(file_get_contents('php://input'), TRUE);


        if ($dataInfo != NULL) {
//            var_dump($dataInfo);die;
                $Inputs = [
                    'user_id' => $dataInfo['user_id'],
                    'temp_user_id' => isset($dataInfo['sess_id']) ? $dataInfo['sess_id'] : '',
                    'order_number' => $this->User_Model->generateOrderId(),
                    'order_date' => date('Y-m-d'),
                    'order_time' => date('Y-m-d H:i:s'),
                    'order_status' => $dataInfo['order_status'],
                    'user_address_id' => $dataInfo['address_id']
                ];



                $this->db->insert('orders', $Inputs);
                $orderid = $this->db->insert_id();
                /* 
                 * Change here
                 */
                if (is_int($orderid) && $orderid) {
                    $a = [];
                    foreach ($dataInfo['carts'] as $cart) {
                        $cart_data = $this->db->get_where('carts', ['id' => $cart['id'], 'user_id' => $dataInfo['user_id']])->result();

                        foreach ($cart_data as $value) {
                            $Insertable = [
                                'order_id' => $orderid,
                                'product_id' => isset($value->product_id) ? $value->product_id : 0,
                                'total_price_after_discount' => isset($value->price_after_discount) ? $value->price_after_discount : 0,
                                'quantity' => isset($value->quantity) ? $value->quantity : 0,
                                'color_id' => isset($value->color_id) ? $value->color_id : 0,
                                'size_id' => isset($value->size_id) ? $value->size_id : 0,
                                'total_price' => isset($value->total_price) ? $value->total_price + $cart['shipping_price'] : 0 + $cart['shipping_price'],
                                'shipping_method' => $cart['CHCnName'],
                                'shipping_price' => $cart['shipping_price'],
                                'chenname' => $cart['CHEnName'],
                                'channelcode' => $cart['ChannelCode'],
                                'reftime' => $cart['RefTime'],
                                'stylename' => $cart['StyleName'],
                                'styleremark' => $cart['StyleRemark']
                            ];
                            $this->db->insert('order_items', $Insertable);
                            $a[] = $this->db->insert_id();
                        }
                    }
                    /*
                     * 
                     */
                    $Response['result']['message'] = 'Order Placed Sucessfully.';
                    $Response['result']['order_id'] = $orderid;
                    $Response['result']['code'] = '200';
                    $Response['result']['status'] = TRUE;
                    $Response['result']['item_insert_status'] = $a;
                } else {
                    $Response['result']['message'] = 'Failed to place the order, please contact support.';
                    $Response['result']['order_id'] = $orderid;
                    $Response['result']['code'] = '200';
                    $Response['result']['status'] = FALSE;
                }
        } else {
            $Response['result']['message'] = 'user_id,sess_id,address_id,cart_id are required';
            $Response['result']['code'] = '203';
            $Response['result']['status'] = FALSE;
        }

        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function user_orderlist_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (!empty($record) && is_object($record)) {

            if (isset($record->status) && $record->status == 'guest') {
                
            } else {
                $sessId = $record->id;
            }


            $allIncompleteOrders = $this->User_Model->getallIncompleteOrder($sessId);




            if ($allIncompleteOrders != NULL) {

                $Response['result']['message'] = 'Orders List Fetch Sucessfully';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['orders'] = $allIncompleteOrders;
//                $Response['result']['total_amount'] = isset($total_amount->Total_amount) ? $total_amount->Total_amount : 0;
            } else {
                $Response['result']['message'] = 'Orders Not Found';
                $Response['result']['code'] = '203';

                $Response['result']['status'] = FALSE;
            }

            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    public function generate_token_get() {


        $gateway = new Braintree\Gateway([
            'accessToken' => 'access_token$sandbox$3mt8kw74bgsrwgtk$3339408ae42838878d4af24e38d3f081',
        ]);

//        $gateway = new Braintree\Gateway([
//            'environment' => 'sandbox',
//            'merchantId' => '564rrvzxmv29vwfk',
//            'publicKey' => 'dr7bs94xmn9mzm7x',
//            'privateKey' => 'a2f57eb0fc6518e5ede19558b3e3a241'
//        ]);

        $clientToken = $gateway->clientToken()->generate();

        if ($clientToken != NULL) {

            $Response['result']['message'] = 'Client Token ';
            $Response['result']['code'] = '200';
            $Response['result']['status'] = TRUE;
            $Response['result']['clientToken'] = $clientToken;
        } else {
            $Response['result']['message'] = 'Client Token Not Found';
            $Response['result']['code'] = '203';
            $Response['result']['status'] = FALSE;
        }

        return $this->response($Response, REST_Controller::HTTP_OK);
    }

    public function transaction_post() {


        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (is_object($record)) {

            $this->form_validation->set_rules('order_id', 'order_id', 'required');
            $this->form_validation->set_rules('paymentMethodNonce', 'paymentMethodNonce', 'required');

            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() == TRUE) {

                $gateway = new Braintree\Gateway([
                    'accessToken' => 'access_token$sandbox$3mt8kw74bgsrwgtk$3339408ae42838878d4af24e38d3f081',
                ]);

//                $gateway = new Braintree\Gateway([
//                    'environment' => 'sandbox',
//                    'merchantId' => '564rrvzxmv29vwfk',
//                    'publicKey' => 'dr7bs94xmn9mzm7x',
//                    'privateKey' => 'a2f57eb0fc6518e5ede19558b3e3a241'
//                ]);

                $this->db->select('SUM(total_price) AS Total_amount');
                $this->db->from('order_items');
                $this->db->where('order_id', $this->input->post('order_id'));
                $total_amount = $this->db->get()->row();

                $result = $gateway->transaction()->sale([
                    'orderId' => $this->input->post('order_id'),
                    'amount' => $total_amount->Total_amount,
                    'paymentMethodNonce' => $this->input->post('paymentMethodNonce'),
                    'deviceData' => 0,
                    'options' => [
                        'submitForSettlement' => True
                    ]
                ]);

                if ($result != NULL) {

                    $jsonresult = json_encode($result);
                    $Inputs = [
                        'transaction_meta' => $jsonresult,
                        'transaction_id' => isset($result->transaction->id) ? $result->transaction->id : '',
                    ];
                    $this->db->update('orders', $Inputs, ['id' => $this->input->post('order_id')]);
                    $this->db->delete('carts', array('user_id' => $record->id));
                }




                $Response['result']['message'] = 'Transactions placed successfully';
                $Response['result']['code'] = '200';
                $Response['result']['status'] = TRUE;
                $Response['result']['orders'] = $result;
            } else {
                $Response['message'] = validation_errors();
                $Response['status'] = FALSE;
                $Response['code'] = '202';
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        }
    }

    public function FlutterTransaction_post() {

        $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
        $sessId = $this->post('sess_id');
        $record = $this->guest_verify_request($Authorization, $sessId);
        if (is_object($record)) {

            $this->form_validation->set_rules('txref', 'txref', 'required');
            $this->form_validation->set_rules('order_id', 'order_id', 'required');
            $this->form_validation->set_rules('total_amount', 'total_amount', 'required');

            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() == TRUE) {
                $txref = $this->post('txref');
                $order_id = $this->post('order_id');
                $total_amount = $this->post('total_amount');

                $curl = curl_init();
                $data = array(
                    'txref' => $txref,
                    'SECKEY' => 'FLWSECK_TEST-c871440a815b15d2b60f5f9675bfac63-X'
                );
                curl_setopt_array($curl, array(
                    CURLOPT_URL => "https://api.ravepay.co/flwv3-pug/getpaidx/api/v2/verify",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => json_encode($data),
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json"
                    ),
                ));

                $response = curl_exec($curl);
                curl_close($curl);
                if ($response != NULL) {
//                    var_dump($response);die;
                    $jsonresult = json_decode($response, true);
                    $Inputs = [
                        'transaction_meta' => $response,
                        'transaction_id' => $jsonresult['data']['txid'],
                        'total_amount' => $jsonresult['data']['amount'],
                        'payment_type' => $jsonresult['data']['paymenttype']
                    ];
                    $this->db->update('orders', $Inputs, ['id' => $order_id]);

                    $this->db->select('*');
                    $this->db->from('orders');
                    $this->db->where('id', $order_id);
                    $transaction = $this->db->get()->result();
                    $this->db->delete('carts', array('user_id' => $record->id));


                    $Response['result']['message'] = 'Transaction Placed Sucessfully';
                    $Response['result']['code'] = '200';
                    $Response['result']['status'] = TRUE;
                    $Response['result']['transaction'] = $transaction;
                    return $this->response($Response, REST_Controller::HTTP_OK);
                } else {
                    $Response['result']['message'] = 'Transaction Not Found';
                    $Response['result']['202'] = '200';
                    $Response['result']['status'] = FALSE;
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
            } else {
                $Response['message'] = validation_errors();
                $Response['status'] = FALSE;
                $Response['code'] = '202';
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function shipping_post() {
        try {
            $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
            $sessId = $this->post('sess_id');
            $record = $this->guest_verify_request($Authorization, $sessId);
            if (is_object($record)) {


                $country_id = 0;
                $country_name = '';
                $country = $this->db->get_where('users', array('id' => $record->id))->row();
                $country_code = $this->db->get_where('countries', array('phonecode' => $country->country_code))->row();
               $country_id = isset($country_code->base_placeId) && $country_code->base_placeId !=null ? $country_code->base_placeId: 165;
               $country_name = isset($country_code->name) &&  $country_code->name != null ?  $country_code->name : 'Nigeria';
//


                $input = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n<soap:Body>\n<getFeeByCWV xmlns=\"http://tempuri.org/\">\n<country>$country_id</country>\n<weight>0.1</weight>\n<volume>10</volume>\n<customerid>100589</customerid>\n<secretkey>5417aabe-181b-48c6-9eb0-eada6e3c6323100589</secretkey>\n</getFeeByCWV>\n</soap:Body>\n</soap:Envelope>";


                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => "http://www.pfcexpress.com/webservice/apiwebservice.asmx",
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => $input,
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: text/xml;charset=utf-8"
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
//        echo $response;


                $doc = new DOMDocument('1.0', 'utf-8');
                $doc->loadXML($response);
                $Response['message'] = "Shipping Methods";
                $Response['country_name'] = isset($country_name) ? $country_name : '';
                $Response['status'] = TRUE;
                $Response['code'] = '200';
                $Response['data'] = json_decode($doc->textContent, TRUE);
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        } catch (Exception $ex) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response);
        }
    }

    public function updateShipping_post() {
        try {
            $Authorization = $this->get('auth') == NULL ? 'basic' : $this->get('auth');
            $sessId = $this->post('sess_id');
            $record = $this->guest_verify_request($Authorization, $sessId);
            if (is_object($record)) {
                $cart_id = $this->post('cart_id') != null ? $this->post('cart_id') : 0;
                $shipping_price = $this->post('shipping_price');
                $shipping_method = $this->post('CHCnName');
                $chenname = $this->post('CHEnName');
                $channelcode = $this->post('ChannelCode');
                $reftime = $this->post('RefTime');
                $stylename = $this->post('StyleName');
                $styleremark = $this->post('StyleRemark');





                $Inputs = [
                    'shipping_method' => isset($shipping_method) ? $shipping_method : 0,
                    'shipping_price' => isset($shipping_price) ? $shipping_price : 0,
                    'chenname' => isset($chenname) ? $chenname : 0,
                    'channelcode' => isset($channelcode) ? $channelcode : 0,
                    'reftime' => isset($reftime) ? $reftime : 0,
                    'stylename' => isset($stylename) ? $stylename : 0,
                    'styleremark' => isset($styleremark) ? $styleremark : 0,
                ];



                $shippingmethod = $this->db->update('carts', $Inputs, ['id' => $cart_id, 'user_id' => $record->id]);

                if ($shippingmethod != NULL) {

                    $Response['result']['message'] = 'Shipping updated successfully.';
                    $Response['result']['code'] = '200';
                    $Response['result']['status'] = TRUE;
                    $Response['result']['sizes'] = $shippingmethod;
                } else {
                    $Response['result']['message'] = 'No Shipping Methods are Found';
                    $Response['result']['code'] = '203';
                    $Response['result']['status'] = FALSE;
                }

                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        } catch (Exception $ex) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response);
        }
    }

//    }
}
