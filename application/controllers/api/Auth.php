<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
require(APPPATH . '/libraries/Format.php');

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

class Auth extends REST_Controller {

    private $SocialProvider = [
        'google' => 'google_id',
        'facebook' => 'facebook_id'
    ];

    public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']);
        $this->load->model('Auth_Model');
        $this->load->helper(array('email', 'url'));
    }

    private function verify_request($token = null) {
        $headers = $this->input->get_request_header('Authorization');
        if (isset($token) && !empty($token)) {
            try {
                $data = AUTHORIZATION::validateToken($token);
                if ($data === false) {
                    $status = parent::HTTP_UNAUTHORIZED;
                    $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                    $this->response($response, $status);
                    exit();
                } else {
                    $token_db = ValidateJwtToken($token);
                    if ($token_db == false) {
                        $status = parent::HTTP_UNAUTHORIZED;
                        $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                        $this->response($response, $status);
                    } else {
                        return $token_db;
                    }
                }
            } catch (Exception $e) {
                $status = parent::HTTP_UNAUTHORIZED;
                $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
                $this->response($response, $status);
            }
        } else {
            $status = parent::HTTP_UNAUTHORIZED;
            $response = ['status' => FALSE, 'message' => 'Unauthorized Access.'];
            $this->response($response, $status);
        }
    }

    public function splashes_get($id = 0) {
        $splashes = $this->Auth_Model->getSplashRows($id);
        if (!empty($splashes)) {
            $Response['status'] = true;
            $Response['result'] = $splashes;
            $this->response($Response, REST_Controller::HTTP_OK);
        } else {
            $this->response(['status' => FALSE, 'message' => 'screen not found.'], REST_Controller::HTTP_OK);
        }
    }

    public function register_post() {
        $first_name = $this->post('first_name');
        $last_name = $this->post('last_name');
        $email = $this->post('email');
        $password = $this->post('password');
        $country_code = $this->post('country_code');
        $mobile = $this->post('mobile');
        $gender = $this->post('gender');
        $device_type = $this->post('device_type');
        $device_token = $this->post('device_token');

        if (empty($first_name)) {
            $Response['status'] = false;
            $Response['message'] = 'Please enter name.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($email)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Please enter email.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (!valid_email($email)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Invalid email.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($password)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Please enter password.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($country_code)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Please enter country code.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($mobile)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Please enter contact number.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($gender)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Please select gender.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($device_type)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Invalid device.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($device_token)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Invalid device token.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else {
            $CntQry = mysqli_query($this->db->conn_id, "SELECT id FROM users WHERE email='" . $email . "'");
            $Existence = mysqli_num_rows($CntQry);


            if ($Existence > 0) {
                $Response['status'] = FALSE;
                $Response['message'] = 'Email already exist,please try with differnt email.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            } else {
                $AddUser = array('first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'password' => md5($password),
                    'country_code' => $country_code,
                    'mobile' => $mobile,
                    'status' => 1,
                    'gender' => $gender,
                    'device_type' => $device_type,
                    'device_token' => $device_token,
                    'created_date' => date('Y-m-d H:i:s'));
                $this->db->insert('users', $AddUser);
                $insertId = $this->db->insert_id();
                if ($insertId) {
                    $token = AUTHORIZATION::generateToken(['id' => $insertId]);
                    $UpdToken = array('jwt_token' => $token);
                    $this->db->where('id', $insertId);
                    $this->db->update('' . DB_PREFIX . 'users', $UpdToken);
                }

                $UserArr = array('id' => $insertId,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'email' => $email,
                    'country_code' => $country_code,
                    'mobile' => $mobile,
                    'status' => 1,
                    'gender' => $gender,
                    'jwt_token' => $token,
                    'profile_pic' => 'uploads/profile/profile_1.png',
                    'device_type' => $device_type,
                    'device_token' => $device_token);

                $UserArr['carts_total'] = "0";
                $UserArr['address_id'] = null;
                $Response['status'] = true;
                $Response['result'] = $UserArr;
                $Response['message'] = 'Register successfully.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function login_post() {
        $email = $this->post('email');
        $password = $this->post('password');
        $device_type = $this->post('device_type');
        $device_token = $this->post('device_token');
        $sessId = $this->post('sess_id');

        if (empty($email)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Please enter email.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (!valid_email($email)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Invalid email.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($password)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Please enter password.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($device_type)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Invalid device.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else if (empty($device_token)) {
            $Response['status'] = FALSE;
            $Response['message'] = 'Invalid device token.';
            return $this->response($Response, REST_Controller::HTTP_OK);
        } else {
            $CntQry = mysqli_query($this->db->conn_id, "SELECT id,first_name,last_name,email,country_code,mobile,gender,device_type,device_token,jwt_token FROM users WHERE email='" . $email . "' AND password='" . md5($password) . "' AND status=1");
            $Existence = mysqli_num_rows($CntQry);
            if ($Existence > 0) {
                $UserRec = mysqli_fetch_object($CntQry);


                if (!empty($UserRec->id)) {

                    /*
                      $token = AUTHORIZATION::generateToken(['id'=>$UserRec->id]);
                      $UpdToken = array('jwt_token'=>$token,
                      'device_type'=>$device_type,
                      'device_token'=>$device_token);

                      $this->db->where('id',$UserRec->id);
                      $this->db->update('users',$UpdToken);
                     */



                    if (!empty($sessId) && is_numeric($sessId)) {
                        $cartInfo = array('temp_user_id' => NULL, 'user_id' => $UserRec->id);
                        $this->db->where('temp_user_id', $sessId);
                        $this->db->update('carts', $cartInfo);
                    }

                    if ($UserRec) {
                        $this->db->select('*');
                        $this->db->from('carts');
                        $this->db->where('user_id', $UserRec->id);
                        $carts = $this->db->get()->result();
                        $UserRec->carts_total = (count($carts) != null) ? count($carts) : "0";



                        $this->db->select('*');
                        $this->db->from('user_address');
                        $this->db->where('user_id', $UserRec->id);
                        $this->db->where('type', 1);
                        $address = $this->db->get()->row();

                        if ($address) {

                            $UserRec->address_id = ($address != null) ? $address : null;
                        } else {
                            $UserRec->address_id = null;
                        }
                    }


                    $token = AUTHORIZATION::generateToken(['id' => $UserRec->id]);
                    $UpdToken = array('jwt_token' => $token, 'device_type' => $device_type, 'device_token' => $device_token);
                    $this->db->where('id', $UserRec->id);
                    $this->db->update('' . DB_PREFIX . 'users', $UpdToken);

                    $Response['status'] = TRUE;
                    $Response['result'] = $UserRec;
                    $Response['message'] = 'Login successfully.';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                } else {
                    $Response['status'] = FALSE;
                    $Response['message'] = 'Invalid email or password.';
                    return $this->response($Response, REST_Controller::HTTP_OK);
                }
            } else {
                $Response['status'] = FALSE;
                $Response['message'] = 'Invalid email or password.';
                return $this->response($Response, REST_Controller::HTTP_OK);
            }
        }
    }

    public function social_login_post() {
        try {
            $this->form_validation->set_rules('provider', 'Provider', 'required');
            $this->form_validation->set_rules('social_id', 'Social_id', 'required');

            $this->form_validation->set_error_delimiters('', '');
            if ($this->form_validation->run() == TRUE) {
                $post = $this->input->post();
                $Provider = isset($this->SocialProvider[$post['provider']]) ? $this->SocialProvider[$post['provider']] : 'google_id';
                $Inputs = [
                    'first_name' => isset($post['first_name']) ? $post['first_name'] : '',
                    'last_name' => isset($post['last_name']) ? $post['last_name'] : '',
                    'email' => isset($post['email']) ? $post['email'] : '',
                    'country_code' => isset($post['country_code']) ? $post['country_code'] : '',
                    'mobile' => isset($post['mobile']) ? $post['mobile'] : '',
                    'status' => 1,
                    'gender' => isset($post['gender']) ? $post['gender'] : '',
                    'device_type' => isset($post['device_type']) ? $post['device_type'] : '',
                    'device_token' => isset($post['device_token']) ? $post['device_token'] : '',
                    'created_date' => date('Y-m-d H:i:s')
                ];
                $Inputs[$Provider] = isset($post['social_id']) ? $post['social_id'] : '';
                $isUserExist = $this->db->get_where('users', [$Provider => $this->input->post('social_id')])->row();
                if ($isUserExist != NULL) {
                    $this->db->update('users', $Inputs, [$Provider => $this->input->post('social_id')]);
                    $Inputs['id'] = $isUserExist->id;
                    $user_data = $this->db->get_where('users', ['id' => $Inputs['id']])->row();
                    $user_data->provider = $post['provider'];
                    if ($user_data) {
                        $this->db->select('*');
                        $this->db->from('carts');
                        $this->db->where('user_id', $user_data->id);
                        $carts = $this->db->get()->result();
                        $user_data->carts_total = (count($carts) != null) ? count($carts) : "0";
                    }
                } else {
                    $this->db->insert('users', $Inputs);
                    $Inputs['id'] = $this->db->insert_id();
                    $user_data = $this->db->get_where('users', ['id' => $Inputs['id']])->row();
                    $user_data->provider = $post['provider'];
                    if ($user_data) {
                        $this->db->select('*');
                        $this->db->from('carts');
                        $this->db->where('user_id', $user_data->id);
                        $carts = $this->db->get()->result();
                        $user_data->carts_total = (count($carts) != null) ? count($carts) : "0";
                    }
                }
                if ($user_data) {
                    $this->db->select('*');
                    $this->db->from('user_address');
                    $this->db->where('user_id', $user_data->id);
                    $this->db->where('type', 1);
                    $address = $this->db->get()->row();

                    if ($address) {

                        $user_data->address_id = ($address != null) ? $address : null;
                    } else {
                        $user_data->address_id = null;
                    }
                }
                if (isset($Inputs['id'])) {

                    $token = AUTHORIZATION::generateToken(['id' => $Inputs['id']]);
                    $UpdToken = array('jwt_token' => $token);
                    $this->db->where('id', $Inputs['id']);
                    $this->db->update('' . DB_PREFIX . 'users', $UpdToken);
                    //
                    $Response['result']['message'] = 'LoggedIn successfully.';
                    $Response['result']['code'] = '200';
                    $Response['result']['user_data'] = $user_data;
                    $Response['result']['provider'] = $post['provider'];
                    $Response['result']['status'] = TRUE;
                } else {

                    $Response['result']['message'] = 'Something went wrong, Please try again';
                    $Response['result']['code'] = '203';
                    $Response['result']['status'] = FALSE;
                }
            } else {
                $Response['result']['message'] = validation_errors();
                $Response['result']['code'] = '202';
                $Response['result']['status'] = FALSE;
            }
            return $this->response($Response, REST_Controller::HTTP_OK);
        } catch (Exception $ex) {
            $response = [
                'status' => FALSE,
                'message' => $e->getMessage(),
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTrace()
            ];
            return $this->response($response, parent::HTTP_UNAUTHORIZED);
        }
    }

}
