<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->database();
    }

    public function index() {
        redirect(admin_base_url . 'auth/login');
        exit();
    }

    public function login() {
        if ($this->session->userdata('ID') != '' and $this->session->userdata('ip') == $_SERVER['REMOTE_ADDR']) {
            redirect(admin_base_url . 'dashboard');
        } else {
            $this->load->view('management/login');
        }
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(admin_base_url . 'auth/login?action=logout');
    }

    public function verify() {
        foreach ($_POST as $key => $value) {
            $this->$key = $value;
        }
        if (empty($this->username) or empty($this->loginpassword)) {
            redirect(admin_base_url . 'auth/login?error=required');
            exit();
        } else {
            $this->load->model('management/auth_model', 'login_model_obj');
            $it_will_be_row_if_valid = $this->login_model_obj->login($this->username, $this->loginpassword);

            if (!empty($it_will_be_row_if_valid)) {
                if ("admin" != $it_will_be_row_if_valid[0]->role) {
                    redirect(admin_base_url . 'auth/login?error=not_allowed');
                    exit;
                } elseif (1 != $it_will_be_row_if_valid[0]->status) {
                    redirect(admin_base_url . 'auth/login?error=not_allowed');
                    exit;
                } else {
                    $this->session->set_userdata(array(
                        'session_id' => session_id(),
                        'ID' => $it_will_be_row_if_valid[0]->id,
                        'login_time' => date("Y-m-d H:i:s"),
                        'role' => $it_will_be_row_if_valid[0]->role,
                        'ip' => $_SERVER['REMOTE_ADDR']));
                    redirect(admin_base_url . 'dashboard');
                }
            } else {
                redirect(admin_base_url . 'auth/login?error=failed');
                exit;
            }
        }
    }

    public function getdata() {
        $serverurl = "http://www.pfcexpress.com/";
        $acition = "webservice/v2/CreateShipment.aspx";
        $apikey = "75273d06-3371-4cfb-9fa3-4621644d53b180000";
        $params = array(
            'Type' => 2,
            'CustomerId' => '80000',
            'ChannelId' => 'DPD',
            'Sender' => 'Ken',
            'SendPhone' => '73948563',
            'SendAddress' => '20 Orley Ave',
            'SendCompany' => 'CS',
            'SendEmail' => 'fjyer@huangjia.com',
            'ShipToName' => 'Eric',
            'ShipToPhoneNumber' => '2025551212',
            'ShipToCountry' => 'US',
            'ShipToState' => 'DC',
            'ShipToCity' => 'Washington',
            'ShipToAdress1' => '475 L Enfant Plaza SW',
            'ShipToAdress2' => '',
            'ShipToZipCode' => '20260',
            'ShipToCompanyName' => 'Express',
            'OrderStatus' => 1,
            'TrackingNo' => '',
            'Remark' => '',
            'BatteryFlag' => 0,
            'INorOut' => 0,
            'CODFee' => 0,
            'IDCardNo' => '',
            'CsRefNo' => 'JH008',
            'WarehouseId' => '',
            'Products' => array(
                array(
                    'CnName' => '手机',
                    'EnName' => 'Mobile',
                    'Weight' => 0.1,
                    'Price' => 5800,
                    'ProducingArea' => 'America',
                    'HSCode' => '65421',
                    'SKU' => 'SK001',
                    'Length' => 0.1,
                    'Width' => 0.2,
                    'High' => 0.3,
                    'MaterialQuantity' => 1
                ),
                array(
                    'CnName' => '鞋子',
                    'EnName' => 'shoes',
                    'Weight' => 0.2,
                    'Price' => 120,
                    'ProducingArea' => 'France',
                    'HSCode' => '65421',
                    'SKU' => 'SK002',
                    'Length' => 0.1,
                    'Width' => 0.2,
                    'High' => 0.3,
                    'MaterialQuantity' => 1
                )
            )
        );
        $jsonparams = json_encode($params);
        $headers = array('Authorization: ' . 'Bearer ' . $apikey, 'Content-type: application/json');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $serverurl . $acition);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonparams);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $json = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($json, true);
        print_r($result);
    }

}

?>