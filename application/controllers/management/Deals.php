<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Deals extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('email');
        $this->load->library('session');
        $this->load->database();
        if (empty($this->session->userdata('ID'))) {
            redirect(admin_base_url . 'auth/login');
        }
    }

    function index() {
        $data['category'] = $this->db->query('SELECT * FROM deals WHERE display_status !=2 ORDER BY id DESC')->result();

        $this->load->view('management/backend_header');
        $this->load->view('management/deals/listing_deals', $data);
        $this->load->view('management/backend_footer');
    }

    function add() {
        $data = array();
        if (isset($_POST['add_category']) && $_POST['add_category'] == 'add') {
            $category_name = addslashes(trim($_POST['category_name']));
            $deal_info = addslashes(trim($_POST['deal_desc']));
            $display_status = addslashes($_POST['display_status']);
            $start_date = isset($_POST['start_date']) ? $_POST['start_date'] : '';
            $start_time = isset($_POST['start_time']) ? $_POST['start_time'] : '';
            $end_date = isset($_POST['end_date']) ? $_POST['end_date'] : '';
            $end_time = isset($_POST['end_time']) ? $_POST['end_time'] : '';
            $minimum = isset($_POST['minimum']) ? $_POST['minimum'] : '';
            $maximum = isset($_POST['maximum']) ? $_POST['maximum'] : '';
            $deal_time = isset($_POST['timer']) ? $_POST['timer'] : '';
            $color = isset($_POST['color']) ? $_POST['color'] : '';

            $image_name = addslashes($_FILES["image"]["name"]);
            $templocation = $_FILES["image"]["tmp_name"];
//	  $target_file = $target_dir.basename($image_name);
            $ext = strtolower(substr($image_name, strrpos($image_name, '.') + 1));
            $store = "uploads/website-imgs/";
            if (!file_exists($store)) {
                @mkdir($store, 0777);
            }

            if (empty($category_name)) {
                $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter deal name.</strong></div>';
            } else if (empty($deal_info)) {
                $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter deal Info</strong></div>';
            } else if (empty($templocation)) {
                $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please upload image.</strong></div>';
            } else {
                if ($ext != "jpg" && $ext != "png" && $ext != "jpeg" && $ext != "gif" && $ext != 'svg') {
                    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Only jpg,png,jpeg,jif images are accepted for upload.</strong></div>';
                } else {
                    $Add = array('parent_id' => 0,
                        'name' => $category_name,
                        'start_date' => $start_date,
                        'start_time' => $start_time,
                        'end_date' => $end_date,
                        'end_time' => $end_time,
                        'isdeal_time' => $deal_time,
                        'minimum' => $minimum,
                        'maximum' => $maximum,
                        'color' => $color,
                        'display_status' => $display_status);

                    $newfilename = file_new_name_when_exist($store, $image_name);
                    if (move_uploaded_file($templocation, $store . $newfilename)) {
                        $Add['image'] = $store . $newfilename;
                    }
                    $this->db->insert('deals', $Add);

                    if (!empty($this->cat_id)) {
                        redirect(admin_base_url . 'deals' . $this->cat_id . '&action=added');
                    } else {
                        redirect(admin_base_url . 'deals?action=added');
                    }
                }
            }
        }

        $this->load->view('management/backend_header');
        $this->load->view('management/deals/add_deals', $data);
        $this->load->view('management/backend_footer');
    }

    public function edit() {
        $Id = $this->uri->segment(4);

        if (!empty($Id) && is_numeric($Id)) {
            $data['category'] = $this->db->query('SELECT * FROM deals WHERE id=' . $Id . '')->result();

            if (isset($_POST['edit_category']) && $_POST['edit_category'] == 'edit') {
                $category_name = addslashes(trim($_POST['category_name']));
                $display_status = addslashes($_POST['display_status']);
                $start_date = isset($_POST['start_date']) ? $_POST['start_date'] : '';
                $start_time = isset($_POST['start_time']) ? $_POST['start_time'] : '';
                $end_date = isset($_POST['end_date']) ? $_POST['end_date'] : '';
                $end_time = isset($_POST['end_time']) ? $_POST['end_time'] : '';
                $minimum = isset($_POST['minimum']) ? $_POST['minimum'] : '';
                $maximum = isset($_POST['maximum']) ? $_POST['maximum'] : '';
                $deal_time = isset($_POST['timer']) ? $_POST['timer'] : '';
                $color = isset($_POST['color']) ? $_POST['color'] : '';

                $image_name = addslashes($_FILES["image"]["name"]);
                $templocation = $_FILES["image"]["tmp_name"];
                $target_file = $target_dir . basename($image_name);
                $ext = strtolower(substr($image_name, strrpos($image_name, '.') + 1));
                $store = "uploads/website-imgs/";
                if (!file_exists($store)) {
                    @mkdir($store, 0777);
                }

                if (empty($category_name)) {
                    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter category name.</strong></div>';
                } else {
                    $upd = array('parent_id' => 0,
                        'name' => $category_name,
                        'start_date' => $start_date,
                        'start_time' => $start_time,
                        'end_date' => $end_date,
                        'end_time' => $end_time,
                        'isdeal_time' => $deal_time,
                        'minimum' => $minimum,
                        'maximum' => $maximum,
                        'color' => $color,
                        'display_status' => $display_status);

                    if (!empty($templocation)) {
                        if ($ext != "jpg" && $ext != "png" && $ext != "jpeg" && $ext != "gif" && $ext != 'svg') {
                            
                        } else {
                            $newfilename = file_new_name_when_exist($store, $image_name);
                            if (move_uploaded_file($templocation, $store . $newfilename)) {
                                $upd['image'] = $store . $newfilename;
                                if (!empty($_POST['old_img']) && file_exists($_POST['old_img'])) {
                                    unlink($_POST['old_img']);
                                }
                            }
                        }
                    }
                    $this->db->where('id', $Id);
                    $this->db->update('deals', $upd);

                    if (!empty($this->cat_id)) {
                        redirect(admin_base_url . 'deals' . $this->cat_id . '&action=updated');
                    } else {
                        redirect(admin_base_url . 'deals?action=updated');
                    }
                }
            }
        } else {
            redirect(admin_base_url . 'deals');
        }

        $this->load->view('management/backend_header');
        $this->load->view('management/deals/edit_deals', $data);
        $this->load->view('management/backend_footer');
    }

    function deleted() {
        $Id = $this->uri->segment(4);
        if (!empty($Id) && is_numeric($Id)) {
            $Del = array('display_status' => 2);
            $this->db->where('id', $Id);
            $this->db->update('deals', $Del);
            if (!empty($this->cat_id)) {
                redirect(admin_base_url . 'deals' . $this->cat_id . '&action=deleted');
            } else {
                redirect(admin_base_url . 'deals?action=deleted');
            }
        } else {
            redirect(admin_base_url . 'deals');
        }
    }

}
