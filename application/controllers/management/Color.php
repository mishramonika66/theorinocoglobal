<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Color extends CI_Controller {
    
    

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('email');
        $this->load->library('session');
        $this->load->database();
        if (empty($this->session->userdata('ID'))) {
            redirect(admin_base_url . 'auth/login');
        }
        $this->WhrCnd = ' AND type=1';
        $this->cat_id = '';
        $this->dbcat_id = 0;
        if (isset($_GET['cat_id']) && !empty($_GET['cat_id']) && is_numeric($_GET['cat_id'])) {
            $this->WhrCnd = ' AND type=' . $_GET['cat_id'] . '';
            $this->cat_id = '?cat_id=' . $_GET['cat_id'];
            $this->dbcat_id = $_GET['cat_id'];
        }
    }

    function index() {
        $data['category'] = $this->db->query('SELECT * FROM color WHERE display_status !=2  ORDER BY id DESC')->result();

        $this->load->view('management/backend_header');
        $this->load->view('management/attributes/listing_color', $data);
        $this->load->view('management/backend_footer');
    }

    function add() {
        

        $data = array();
        if (isset($_POST['add_category']) && $_POST['add_category'] == 'add') {
            $category_name = addslashes(trim($_POST['category_name']));
            $display_status = addslashes($_POST['display_status']);
            
            if (empty($category_name)) {
                $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter attribute name.</strong></div>';
            } else {
                $Add = array(
                    'attribute_name' => $category_name,
                    'display_status' => $display_status);
                $this->db->insert('color', $Add);

               
                    redirect(admin_base_url . 'color?action=added');
               
            }
        }

        $this->load->view('management/backend_header');
        $this->load->view('management/attributes/add_color', $data);
        $this->load->view('management/backend_footer');
    }

    public function edit() {
        $data = array();
        $Id = $this->uri->segment(4);
      

        if (!empty($Id) && is_numeric($Id)) {
            $data['category'] = $this->db->query('SELECT * FROM color WHERE id=' . $Id . '')->result();

            if (isset($_POST['edit_category']) && $_POST['edit_category'] == 'edit') {
                $category_name = addslashes(trim($_POST['category_name']));
                $display_status = addslashes($_POST['display_status']);

                if (empty($category_name)) {
                    $data['errmsg'] = '<div class="alert alert-danger alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Please enter attribute name.</strong></div>';
                } else {
                    $upd = array(
                        'attribute_name' => $category_name,
                        'display_status' => $display_status);

                    $this->db->where('id', $Id);
                    $this->db->update('color', $upd);
                   
                        redirect(admin_base_url . 'color?id=' . $Id.'&action=updated');

//                        redirect(admin_base_url . 'attributes' . $this->cat_id . '&action=updated');
                  
                }
            }
        } else {
            redirect(admin_base_url . 'color');
        }

        $this->load->view('management/backend_header');
        $this->load->view('management/attributes/edit_color', $data);
        $this->load->view('management/backend_footer');
    }

    function deleted() {
        $Id = $this->uri->segment(4);
        $catid = $this->uri->segment(5);
        if (!empty($Id) && is_numeric($Id)) {
            $Del = array('display_status' => 2);
            $this->db->where('id', $Id);
            $this->db->update('color', $Del);
            
                redirect(admin_base_url . 'color?id='.  $Id . '&action=deleted');
           
        } else {
            redirect(admin_base_url . 'color');
        }
    }

}
