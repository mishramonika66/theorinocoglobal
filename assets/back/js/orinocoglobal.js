function ChangeCategory(id,level)
{
  if(id != '')
  {
    $.ajax({
		type:"POST",
		url: BaseUrl+"products/subcategories",
		dataType: 'JSON',
		data: { id : id },	    		
		success: function(data){
		  if(data.html != '' && data.status==true)
		  {
		    $("#"+level).html(data.html);
		  }
		  else
		  {
		    $("#"+level).html('<option value="">Category Not Found.</option>');
		  }
		}
      });
  
  }
}

//Remove Product images
$(document).ready(function(){
	$('.del-pro-img').click(function(){
	  var Imgid = $(this).data('imgid');
	  var Imgnm = $(this).data('imgnm');

	  if(Imgid != '' && Imgid > 0 && isNaN(Imgid)==false)
	  {
	    jQuery.ajax({
				type: 'POST',						
				url: BaseUrl+'products/deleteImg/',						
				data: {'Imgid' : Imgid, 'Imgnm':Imgnm },
				success: function(response){
					if(response=='success'){
					   $('#del-pro-img-'+Imgid).hide();
					}				
				}						
	     });  
	  }	
	});
});