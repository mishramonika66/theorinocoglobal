<?php
$Message='<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Email Template</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
</head>

<body style="font-family:Roboto, sans-serif;">
Hello, <b>'.$user_name.',</b><br><br>

Account created a successfully.<br><br>

 Please find the details bellow:<br><br>

<b>Registered email address:</b> '.$user_email.'<br>
<b>Password:</b>'.$user_password.'<br>

Please click below link to login.<br><br>
<a href="'.admin_base_url.'auth/login/">'.admin_base_url.'auth/login</a><br><br>

Best wishes,<br><br>
<b>'.$best_regards.'<br>
</body>
</html>';
$this->email->from($Noreply, $no_reply_name);
$this->email->to($user_email);                                       
$this->email->subject('New Account Created.');
$this->email->message($Message);  					
$this->email->send();
?>