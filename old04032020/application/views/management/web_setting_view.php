<?php
if(isset($_GET['action']) && $_GET['action']=='added' && empty($errmsg))
{
  $errmsg = '<div class="alert alert-success alert-dismissable"> <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Saved successfully.</strong></div>';
}
?>
<!-- Everything goes inside this --> 
<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">	    
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">WEBSITE GLOBAL SETTINGS</h4>
            </div>
         </div>
	  </div>
   </div>
   <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">               
               <?php if(!empty($errmsg)) { echo $errmsg; } ?>
			   <h4 class="card-title"></h4>
               <form class="forms-sample" method="post" enctype="multipart/form-data">
                  <div class="row">				    
                     <div class="col-sm-12">
                        <div class="form-group">
                           <label for="email_address"><b>Email <font color="red">*</font></b></label>
                           <input type="email" class="form-control" id="email_address" name="email_address" placeholder="Enter email here." value="<?=stripslashes($Setting[0]->email_address)?>">
                        </div>
                     </div>
				  </div>                  
                  
                  <p>&nbsp;</p>
                  <button type="submit" name="save_setting" value="save" class="btn btn-primary btn-lg mr-2">Save</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Everything goes inside this -->