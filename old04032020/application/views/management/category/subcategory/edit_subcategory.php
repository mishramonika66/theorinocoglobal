<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-normal font-transform mb-0">Create New Category</h4>
            </div>
            <div>
               <a href="<?php echo admin_base_url.'subcategory?cat_id='.$_GET['cat_id']; ?>" class="btn btn-primary btn-icon-text">
               <i class="ti-angle-left btn-icon-append"></i>  
               BACK
               </a>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-12 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <?php if(!empty($errmsg)) { echo $errmsg; } ?>
               <form class="forms-sample" method="post" enctype="multipart/form-data">
                  <div class="row">
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="category_name"><b>Name <font color="red">*</font></b></label>
                           <input type="text" class="form-control" id="category_name" name="category_name" placeholder="Enter name" value="<?=$category[0]->name?>">
                        </div>
                     </div>
                     <div class="col-sm-4">
                        <div class="form-group">
                           <label for="phone_number"><b>Upload Image <font color="red">*</font></b></label>
                           <input type="file" class="form-control" name="image" id="userfile1" />
                        </div>
                     </div>
					 <div class="col-sm-2 image_preview_new">
					    <input type="hidden" name="old_img" value="<?=$category[0]->image?>">
                        <?php if(!empty($category[0]->image) && file_exists($category[0]->image)){ ?>
						<div class="del_unit">
						   <img src="<?php echo base_url().$category[0]->image; ?>">
						</div>
						<?php } ?>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-sm-6">
                         <div class="form-group">
                           <label for="parent_id"><b>Parent Category</b></label>
                           <select class="custom-select" id="parent_id" name="parent_id">
                              <!--<option value="0">Select parent category</option>-->
							  <?php if(!empty($parentcat)){ ?>
							  <?php foreach($parentcat as $cat){ ?>
                                <?php if($_GET['cat_id']==$cat->id){ ?>
                               <option value="<?=$cat->id?>" <?php if($category[0]->parent_id==$cat->id){ echo 'selected'; } ?>><?=stripslashes($cat->name)?></option>     
							  <?php } } } ?>
                           </select>
                        </div>
                     </div>
                     <div class="col-sm-6">
                        <div class="form-group">
                           <label for="display_status"><b>Status</b></label>
                           <select class="custom-select" id="display_status" name="display_status">
                              <option value="1" <?php if($category[0]->display_status==1){ echo 'selected'; } ?>>Active</option>
							  <option value="0" <?php if($category[0]->display_status==0){ echo 'selected'; } ?>>Inactive</option>
                           </select>
                        </div>
                     </div>
                  </div>				 
				  <div class="col-sm-12">
                        <button type="submit" name="edit_category" value="edit" class="btn btn-primary btn-lg mr-2">Save</button> 
                     </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>