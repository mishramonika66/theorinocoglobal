<!-- Everything goes inside this --> 
<div class="content-wrapper">
   <div class="row">
      <div class="col-md-12 grid-margin">
         <div class="d-flex justify-content-between align-items-center">
            <div>
               <h4 class="font-weight-bold mb-0">Dashboard</h4>
            </div>            
         </div>
      </div>
   </div>
   
   <div class="row">
      <div class="col-md-3 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <p class="card-title text-md-center text-xl-left">Total Location</p>
               <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">12333</h3>
                  <i class="ti-calendar icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
               </div>
               
            </div>
         </div>
      </div>
      <div class="col-md-3 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <p class="card-title text-md-center text-xl-left">Total Staff</p>
               <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">113131</h3>
                  <i class="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
               </div>               
            </div>
         </div>
      </div>    
      <div class="col-md-3 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <p class="card-title text-md-center text-xl-left">Total Vendors</p>
               <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">1313</h3>
                  <i class="ti-layers-alt icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
               </div>               
            </div>
         </div>
      </div>
	  <div class="col-md-3 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <p class="card-title text-md-center text-xl-left">Total Items</p>
               <div class="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 class="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">113</h3>
                  <i class="ti-calendar icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
               </div>
               
            </div>
         </div>
      </div>
   </div>
  

   <div class="row">
      <div class="col-md-6 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <p class="card-title mb-0">Latest locations</p>
               <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>                           
                           <th>Name</th>
						   <th>Email</th>
                           <th>Phone</th>                        
                        </tr>
                     </thead>
                     <tbody>
					   
                        <tr>
                           <td>asas</td>
						   <td>as
                           <td>as</td>
                        </tr>
					   									
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <p class="card-title mb-0">latest Staff</p>
               <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>                           
                           <th>Name</th>
						   <th>Email</th>
                           <th>Phone</th>                        
                        </tr>
                     </thead>
                     <tbody>
					   
                        <tr>
                           <td>as</td>
						   <td>as</td>
                           <td>as</td>
                        </tr>
														
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>

   <div class="row">
      <div class="col-md-6 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <p class="card-title mb-0">Latest Vendors</p>
               <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>                           
                           <th>Name</th>
						   <th>Point of contact</th>
                           <th>Phone</th>                        
                        </tr>
                     </thead>
                     <tbody>
					  
                        <tr>
                           <td>as</td>
						   <td>as</td>
                           <td>s</td>
                        </tr>
					 										
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <div class="col-md-6 grid-margin stretch-card">
         <div class="card">
            <div class="card-body">
               <p class="card-title mb-0">latest Items</p>
               <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>                           
                           <th>Name</th>						  
                           <th>Added Date</th>                        
                        </tr>
                     </thead>
                     <tbody>
					  
                        <tr>
                           <td>as</td>						   
                           <td>a</td>
                        </tr>
					  									
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
		
      </div>	
   </div>
   
</div>
<!-- Everything goes inside this -->