<?php
if(isset($_GET['error']) and $_GET['error']=='failed')
{
  $class   = "alert alert-danger";
  $message = "Login failed! either Email or Password is incorrect."; 				     
}
if(isset($_GET['error']) and $_GET['error']=='required')
{
  $class   = "alert alert-danger";
  $message = "Please enter Email and Password and try again."; 
}
if(isset($_GET['error']) and $_GET['error']=='not_allowed')
{
  $class   = "alert alert-warning";
  $message = "You are not allowed to visit dashboard.";
}
if(isset($_GET['action']) and $_GET['action']=='logout')
{
  $class   = "alert alert-success";
  $message = "You are logged out now.";
}
if( isset( $_GET['error']) and $_GET['error']=='blocked')
{
  $class   = "alert alert-warning";
  $message = "You are not active right now, try to contact the admin."; 
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Control Panel | Login</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/back/css/themify-icons.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/back/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/back/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/back/images/favicon.png" />
<style>
.alert-danger {
    color: #1f1d1d !important;    
    font-weight:500 !important;
}
</style>
</head>
<body>
    <div class="container-scroller">
        <div class="container-fluid page-body-wrapper full-page-wrapper">
          <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
            <div class="row flex-grow">
              <div class="col-lg-6 d-flex align-items-center justify-content-center">
                <div class="auth-form-transparent text-left p-3">
                  <div class="brand-logo">
				     <h3>OrinocoGlobal</h3>                   
                  </div>
                  <h4>Please sign in</h4>                  				  
                  <form class="pt-3" name="signin_form" id="signin_form" action="<?php echo admin_base_url; ?>auth/verify" method="post">
                    <div class="form-group">
                      <label for="exampleInputEmail">Email address</label>
                      <div class="input-group">
                        <div class="input-group-prepend bg-transparent">
                          <span class="input-group-text bg-transparent border-right-0">
                            <i class="ti-user text-primary"></i>
                          </span>
                        </div>
                        <input type="text" class="form-control form-control-lg border-left-0" name="username" id="username" placeholder="Email address">
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword">Password</label>
                      <div class="input-group">
                        <div class="input-group-prepend bg-transparent">
                          <span class="input-group-text bg-transparent border-right-0">
                            <i class="ti-lock text-primary"></i>
                          </span>
                        </div>
                        <input type="password" class="form-control form-control-lg border-left-0" name="loginpassword" id="loginpassword" placeholder="Password">                        
                      </div>
                    </div>
                    <div class="my-2 d-flex justify-content-between align-items-center">
                     <?php 
					  if(!empty($class) and !empty($message))
					  {
						echo sprintf('<div class="%s">%s</div>' , $class , $message);		   
					  }			   
					  ?>                      
                    </div>
                    <div class="my-3">
                      <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">LOGIN</button>
                    </div>
                  </form>
                </div>
              </div>
              <div class="col-lg-6 login-half-bg d-flex flex-row">
                <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2019 All rights reserved.</p>
              </div>
            </div>
          </div>
          <!-- content-wrapper ends -->
        </div>
        <!-- page-body-wrapper ends -->
      </div>  
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script>
	$(".alert-danger,.alert-success").fadeTo(2000, 600).slideUp(600, function(){
		$(".alert-danger,.alert-success").slideUp(600);
	});
	</script>
</body>
</html>