<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_Model extends CI_Model
{  
  public function __construct()
  {
    parent::__construct();
	$this->load->database();
  }
  
  //Get Slider | Home
  function getSliderRows($id='')
  {
    if(!empty($id))
	{
	  $Whr =array('id'=>$id,'display_status'=>1,'type'=>0);
	  $query = $this->db->select('slider_image')->get_where('sliders',$Whr);
	  return $query->row();
	}
	else
	{	  
	  $Whr = array('display_status'=>1,'type'=>0);
	  $this->db->limit(5);
	  $query = $this->db->select('slider_image')->get_where('sliders',$Whr);	  
	  return $query->result();
	}  
  }
  
  //Get All Deals | Home
  public function getDealsRows($id='')
  {
    if(!empty($id))
	{
	  $Whr =array('id'=>$id,'display_status'=>1);
	  $query = $this->db->select('id,name')->get_where('deals',$Whr);
	  return $query->row();
	}
	else
	{
	  $Whr = array('display_status'=>1);
	  //$this->db->limit(10);
	  $query = $this->db->select('id,name,image')->get_where('deals',$Whr);
	  return $query->result();
	}	  
  }
 
  //Get Individual Product According to Deal Id | Home
  public function getProductOnDeals($dealId,$type='')
  {   
	$WhrCnd = "";
	if($type=='dailydeal')
	{
	  $WhrCnd = " AND DATE(pro.deal_end_date)='".date('Y-m-d')."'";
	}
	$deals =$this->db->query("SELECT pro.id,pro.product_name,pro.price,pro.discount,pro.discount_type,pro.price_after_discount,pro.cover_image,pro.deal_end_date from product_deals as deal LEFT JOIN products as pro ON deal.product_id=pro.id WHERE deal.deal_id=".$dealId." ".$WhrCnd." ORDER BY pro.id desc LIMIT 5")->result();	
	return $deals;
  }
 
  //Get Individual Product & Sub Category According to Deal Id | Home
  public function getProductWithCategoryOnDeals($dealId)
  { 
	$deals =$this->db->query("SELECT pro.id,pro.product_name,pro.price,pro.discount,pro.discount_type,pro.price_after_discount,pro.cover_image,cat.name as category_name,cat.id as category_id from product_deals as deal LEFT JOIN products as pro ON deal.product_id=pro.id LEFT JOIN category as cat ON pro.sub_category_id=cat.id WHERE deal.deal_id=".$dealId." AND pro.sub_category_id != '' ORDER BY pro.id desc LIMIT 5")->result();	
	return $deals;
  }
  
  //Get Today Deals Item | Home
  public function getProductRows($Id='')
  {
    if(!empty($Id))
	{
	  $Whr =array('id'=>$Id,'display_status'=>1);
	  $query = $this->db->select('id,product_name,price,discount_type,discount,price_after_discount,cover_image,deal_end_date,sizes,color,description,specification,total_sale,subsub_category_id,sub_category_id,category_id,quantity')->get_where('products',$Whr);
	  return $query->row_array();
	}
	else
	{
	  $Whr = array('display_status'=>1,'is_featured'=>1);
	  $this->db->limit(6);
	  $this->db->order_by("id", "desc");
	  $query = $this->db->select('id,product_name,price,discount_type,discount,price_after_discount,cover_image,deal_end_date')->get_where('products',$Whr);
	  return $query->result();
	}
  }

  //Get Featured Category | Home
  public function getCategoriesHome()
  {
    $this->db->select('id,name,image');
    $this->db->from('category');
    $this->db->where('parent_id',0);
	$this->db->where('is_featured',1);
	$this->db->where('display_status',1);
    $parent = $this->db->get();
    $categories = $parent->result();
    $i=0;
    foreach($categories as $p_cat)
	{
	  $categories[$i]->sub = $this->subCategoriesHome($p_cat->id);
      $i++;
	}
    return $categories;
  }

  //Get Featured Sub Category | Home
  public function subCategoriesHome($id)
  {
    $this->db->select('id,name,image');
    $this->db->from('category');
    $this->db->where('parent_id', $id);
    $this->db->where('type',1);
	$this->db->where('display_status',1);
	$this->db->limit(2);
    $child = $this->db->get();
    $categories = $child->result();    
    return $categories;
  }

  //Get All Categories | Category ::(1)
  public function getCategoriesRows()
  {
    $this->db->select('id,name,image');
    $this->db->from('category');
    $this->db->where('parent_id',0);	
	$this->db->where('display_status',1);
    $parent = $this->db->get();
    $categories = $parent->result();
    $i=0;
    foreach($categories as $p_cat)
	{
	  $categories[$i]->sub = $this->getSubCategoriesRows($p_cat->id);
      $i++;
	}
    return $categories;
  }

  //Get All Sub Categories | Category ::(2)
  public function getSubCategoriesRows($id)
  {
    $this->db->select('id,name,image');
    $this->db->from('category');
    $this->db->where('parent_id', $id);
    $this->db->where('type',1);
	$this->db->where('display_status',1);
    $child = $this->db->get();
    $categories = $child->result();
    $i=0;
    foreach($categories as $p_cat)
	{
	  $categories[$i]->subsub = $this->getSubSubCategoriesRows($p_cat->id);
      $i++;
	}
    return $categories; 
  }
  
  //Get All Sub Sub Categories | Category ::(3)
  public function getSubSubCategoriesRows($id)
  {
    $this->db->select('id,name,image');
    $this->db->from('category');
    $this->db->where('parent_id', $id);
    $this->db->where('type',2);
	$this->db->where('display_status',1);	
    $child = $this->db->get();
    $categories = $child->result();    
    return $categories;
  }

  //category_products_get
  public function getProductByCategory($CatId,$type='',$ProductId=0)
  {
    $Whr = "";
	if($type=='related')
	{
	  $Whr = " AND id != ".$ProductId."";
	}

	$products =$this->db->query("SELECT id,product_name,price,discount,discount_type,price_after_discount,cover_image from products WHERE subsub_category_id=".$CatId." ".$Whr." ORDER BY id desc Limit 6")->result();
	return $products;
  }

  public function getAttributeInProducts($Ids,$Type=0)
  {    
	$attributes =$this->db->query("SELECT id,attribute_name from attributes WHERE type=".$Type." AND id IN(".$Ids.") ORDER BY id asc")->result();
	return $attributes;  
  }

  public function getProductRatings($ProductId)
  {
	$ratings =$this->db->query("SELECT rat.rating,rat.comments,rat.added_date,usr.first_name as sender_name,usr.status from product_ratings as rat LEFT JOIN users as usr ON rat.sender_id=usr.id WHERE rat.product_id=".$ProductId." ORDER BY rat.id desc")->result();
	return $ratings;  
  }

  public function getProductSlider($ProductId)
  {
    $images = $this->db->query("SELECT id,image from product_images WHERE product_id=".$ProductId."")->result();
	return $images;
  }

  public function getCategoeiesByDealId($DealId)
  {
    
  }

}

?>

