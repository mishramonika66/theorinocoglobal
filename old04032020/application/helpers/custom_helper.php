<?php
if(!function_exists('UniqueSlug'))
{
  function UniqueSlug($string,$table,$field,$key=NULL,$value=NULL)
  {    
    $t =& get_instance();
    $slug = url_title($string);
    $slug = strtolower($slug);
    $i = 0;
    $params = array ();
    $params[$field] = $slug;
 
    if($key)$params["$key !="] = $value;
 
    while ($t->db->where($params)->get($table)->num_rows())
    {  
        if (!preg_match ('/-{1}[0-9]+$/', $slug ))
            $slug .= '-' . ++$i;
        else
            $slug = preg_replace ('/[0-9]+$/', ++$i, $slug );
         
        $params [$field] = $slug;
    }  
    return $slug;
  }
}

if(!function_exists('file_newname_when_exist'))
{
  function file_new_name_when_exist($path, $filename)
  {
    if ($pos = strrpos($filename, '.')){
           $name = substr($filename, 0, $pos);
           $ext = substr($filename, $pos);
    } else{
           $name = $filename;
    }

    $newpath = $path.'/'.$filename;
    $newname = $filename;
    $counter = 0;
    while (file_exists($newpath)){
           $newname = $name .'_'. $counter . $ext;
           $newpath = $path.'/'.$newname;
           $counter++;
     }
    return $newname;
  }
}

if(!function_exists('UsersInformation'))
{
  function UsersInformation($ID)
  {
	 $sqlConn = &get_instance();
     $RunQry = "select * from users where id='".$ID."'";	 
     $ExecQry = mysqli_query($sqlConn->db->conn_id,$RunQry);
     $FetchQry = mysqli_fetch_object($ExecQry);
	 return $FetchQry;	
   }
}

if(!function_exists('get_site_option'))
{
  function get_site_option()
  {  
	$sqlConn = & get_instance();
    $RunQry = "select * from global_settings where ID=1";	 
    $ExecQry = mysqli_query($sqlConn->db->conn_id,$RunQry);
    $FetchQry = mysqli_fetch_object($ExecQry);
	return $FetchQry;		
  }
}

if(!function_exists('ValidateJwtToken'))
{
  function ValidateJwtToken($Token='')
  {
    if(!empty($Token))
	{
	  $sqlConn = & get_instance();
	  $RunQry = "select id,first_name,last_name,email,country_code,mobile,gender from users where jwt_token='".$Token."' AND status=1";
	  $ExecQry = mysqli_query($sqlConn->db->conn_id,$RunQry);
	  $Record = mysqli_fetch_object($ExecQry);
	  if(!empty($Record))
	  {
	    return $Record;
	  }
	  else
	  {
	    return false;
	  }
	}
	else
	{
	  return false;
	}
  }
}

if(!function_exists('CategoryInfo'))
{
  function CategoryInfo($Id)
  {  
	$sqlConn = & get_instance();
    $RunQry = "select id,name,parent_id from category where id=".$Id."";	 
    $ExecQry = mysqli_query($sqlConn->db->conn_id,$RunQry);
    $FetchQry = mysqli_fetch_object($ExecQry);
	return $FetchQry;		
  }
}

if(!function_exists('fill_size_select_box'))
{
  function fill_size_select_box()
  {  
    $output= '';
	$Conn  = & get_instance();
	$Qry   = "SELECT id,attribute_name FROM attributes WHERE type=2 AND display_status=1 ORDER BY id DESC";	 
	$QryRun= mysqli_query($Conn->db->conn_id,$Qry);	
	$Count = mysqli_num_rows($QryRun);
	if($Count > 0)
	{
      while($Data  = mysqli_fetch_object($QryRun))
	  {
		$output .= '<option value="'.$Data->id.'">'.$Data->attribute_name.'</option>';
	   }
	}
	return $output;
  }
}

if(!function_exists('fill_color_select_box'))
{
  function fill_color_select_box()
  {
    $output= '';
	$Conn  = & get_instance();
	$Qry   = "SELECT id,attribute_name FROM attributes WHERE type=1 AND display_status=1 ORDER BY id DESC";	 
	$QryRun= mysqli_query($Conn->db->conn_id,$Qry);	
	$Count = mysqli_num_rows($QryRun);
	if($Count > 0)
	{
      while($Data  = mysqli_fetch_object($QryRun))
	  {
		$output .= '<option value="'.$Data->id.'">'.$Data->attribute_name.'</option>';
	   }
	}
	return $output;  
  }
}

if(!function_exists('SubCategories'))
{
  function SubCategories($id,$selVal='')
  {
      $html = '<option value="">Select category</option>';	 
	  if(!empty($id) && is_numeric($id))
	  {	    
		$Conn  = & get_instance();
		$Qry='SELECT * FROM category WHERE type=1 AND parent_id='.$id.' AND display_status=1 ORDER BY id DESC';
		$QryRun= mysqli_query($Conn->db->conn_id,$Qry);	
	    $Count = mysqli_num_rows($QryRun);
		if($Count > 0)
		{		 
		  while($Data  = mysqli_fetch_object($QryRun))
		  {			
			$selected = '';
			if($Data->id==$selVal){
			 $selected = 'selected';
			}

			$html .='<option value="'.$Data->id.'" '.$selected.'>'.$Data->name.'</option>';
		  }
		}
	  }
	  echo $html;	 
  }
}

